#include "headers/Date.hpp"
#include <iostream>
#include <string>
#include <iomanip>
#include <cassert>

Date::Date()
    : day_(1)
    , month_(1)
    , year_(2000)
{
}

Date::Date(const int day, const int year)
    : day_(day)
    , month_(1)
    , year_(year) 
{
}

Date::Date(const int month, const int day, const int year)
    : day_(day)
    , month_(month)
    , year_(year)
{
}

void
Date::nextDayDY()
{
    day_ = (day_ % 365) + 1;
    if (1 == day_) {
        ++year_;
    }
}

void
Date::nextDay()
{
    day_ = (day_ % daysMonth(month_, year_)) + 1;
    if (1 == day_) {
        month_ = (month_ % 12) + 1;
        if (1 == month_) {
            ++year_;
        }
    }
}

void
Date::print() const
{
    std::cout << std::setfill('0') << std::setw(2) << month_ << '/' 
              << std::setw(2) << day_ << '/' << year_ << std::endl;
}

void
Date::printStandard() const
{
    std::string monthName[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun"
                               "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
                              };
    std::cout << monthName[month_ - 1] << ' ' << day_ << ' ' << year_ << std::endl;
}

void
Date::printDY() const
{
    std::cout << std::setfill('0') << std::setw(3) << day_ << '/' << year_ << std::endl;
}

int
daysMonth(const int month, const int year)
{
    assert(month > 0 && month <= 12);
    if (2 == month) {
        if (0 == year % 400 || (year % 100 != 0 && 0 == year % 4)) {
            return 29;
        }
        return 28;
    }
    if (4 == month || 6 == month || 9 == month || 11 == month) {
        return 30;
    }
    return 31;
}


