#include "headers/Date.hpp"
#include <iostream>
#include <unistd.h>
#include <cstdlib>
#include <cassert>

void
inputDate(int& day, int& month, int& year)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input month, day and year:";
    }
    std::cin >> month >> day >> year;
    if (day <= 0 || month > 12 || month <= 0 || year < 1900 || year > 2100 || day > daysMonth(month, year)) {
        std::cerr << "Error 1: Invalid date." << std::endl;
        ::exit(1);
    }
}

void
inputDateDY(int& day, int& year)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input day and year:";
    }
    std::cin >> day >> year;
    if (day > 365 || day <= 0 || year < 1900 || year > 2100) {
        std::cerr << "Error 2: Invalid date." << std::endl;
        ::exit(2);
    }
}

int
inputFormat()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Date formats:"
                  << "\t1 - DD/MM/YY"
                  << "\t2 - DDD/YYYY"
                  << "> Choose: ";
    }
    int format;
    std::cin >> format;
    if (format != 1 && format != 2) {
        std::cerr << "Error 3: Invalid date format." << std::endl;
        ::exit(3);
    }
    return format;
}

int 
main()
{
    const int format = inputFormat();
    if (1 == format) {
        int day1, month1, year1;
        inputDate(day1, month1, year1);
        Date date1(day1, month1, year1);
        date1.nextDay();
        date1.print();
        return 0;
    }
    
    assert(2 == format);
    int day2, year2;
    inputDateDY(day2, year2);
    Date date2(day2, year2);
    date2.nextDayDY();
    date2.printDY();
    return 0;
}

