#ifndef __DATE_HPP__
#define __DATE_HPP__

int daysMonth(const int month, const int year);

class Date
{
    friend int daysMonths(const int month, const int year);
public:
    Date();
    Date(const int day, const int year);
    Date(const int month, const int day, const int year);
    void nextDayDY();
    void nextDay(); 
    void print() const;
    void printStandard() const;
    void printDY() const;


private:
    int day_;
    int month_;
    int year_;

};

#endif /// __DATE_HPP__

