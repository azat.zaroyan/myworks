#ifndef __INTEGER_SET_HPP__
#define __INTEGER_SET_HPP__
static const int MAX_SIZE = 101;

class IntegerSet;

IntegerSet unionOfSets(const IntegerSet& rhv, const IntegerSet& lhv);
IntegerSet intersectionOfSets(const IntegerSet& rhv, const IntegerSet& lhv);
bool isEqualTo(const IntegerSet& rhv, const IntegerSet& lhv);

class IntegerSet
{
    friend IntegerSet unionOfSets(const IntegerSet& rhv, const IntegerSet& lhv);
    friend IntegerSet intersectionOfSets(const IntegerSet& rhv, const IntegerSet& lhv);
    friend bool isEqualTo(const IntegerSet& rhv, const IntegerSet& lhv);
public:
    IntegerSet();
    IntegerSet(const int* array, const int size);
    ~IntegerSet();
    void insertElement(const int element);
    void deleteElement(const int element);
    void printSet() const;
private:
    bool* set_;

};

#endif /// __INTEGER_SET_HPP__

