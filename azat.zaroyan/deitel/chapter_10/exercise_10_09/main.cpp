#include "headers/IntegerSet.hpp"
#include <iostream>
#include <unistd.h>
#include <cstdlib>

void 
inputIntegerSet(int*& array, int& size)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input size:";
    }
    std::cin >> size;
    if (size < 0) {
        std::cerr << "Error 1: Invlaid size!" << std::endl;
        ::exit(1);
    }
    array = new int[size];
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input array:";
    }
    for (int i = 0; i < size; ++i) {
        std::cin >> array[i];
        if (array[i] >= MAX_SIZE) {
            std::cerr << "Error 2: Invlaid array!" << std::endl;
            ::exit(2);
        }
    }
}

int 
main()
{
    int size1, size2;
    int* array1, *array2;
    inputIntegerSet(array1, size1);
    const IntegerSet integerSet1(array1, size1);
    integerSet1.printSet();
    inputIntegerSet(array2, size2);
    const IntegerSet integerSet2(array2, size2);
    integerSet2.printSet();
    IntegerSet integerSet3 = intersectionOfSets(integerSet1, integerSet2);
    integerSet3.printSet();
    IntegerSet integerSet4 = unionOfSets(integerSet1, integerSet2);
    integerSet4.printSet();
    delete [] array1;
    delete [] array2;
    return 0;
}

