#include "headers/IntegerSet.hpp"
#include <iostream>
#include <cassert>

IntegerSet::IntegerSet()
{
    set_ = new bool[MAX_SIZE];
    for (int i = 0; i < MAX_SIZE; ++i) {
        set_[i] = false;
    }
}

IntegerSet::IntegerSet(const int* array, const int size)
{
    set_ = new bool[MAX_SIZE]; 
    for (int i = 0; i < MAX_SIZE; ++i) {
        set_[i] = false;
    }
    for (int i  = 0; i < size; ++i) {
            set_[array[i]] = true;
        }
}

IntegerSet::~IntegerSet()
{
    if (set_ != NULL) {
        delete [] set_;
        set_ = NULL;
    }
}

void
IntegerSet::insertElement(const int index) 
{
    set_[index] = true;
}

void 
IntegerSet::deleteElement(const int index)
{
    set_[index] = false;
}

void
IntegerSet::printSet() const
{
    for (int i = 0; i < MAX_SIZE; ++i) {
        if (set_[i]) { 
            std::cout << i << "  ";
        } else {
            std::cout << "--- ";
        }
    }
    std::cout << std::endl;
}

IntegerSet unionOfSets(const IntegerSet& rhv, const IntegerSet& lhv)
{
    IntegerSet temp;
    for (int i = 0; i < MAX_SIZE; ++i) {
        temp.set_[i] = rhv.set_[i] || lhv.set_[i];
    }
    return temp;
}

IntegerSet intersectionOfSets(const IntegerSet& rhv, const IntegerSet& lhv) 
{
    IntegerSet temp;
    for (int i = 0; i < MAX_SIZE; ++i) {
        temp.set_[i] = rhv.set_[i] && lhv.set_[i];
    }
    return temp;
}

bool isEqualTo(const IntegerSet& rhv, const IntegerSet& lhv)
{
    for (int i = 0; i < MAX_SIZE; ++i) {
        if (rhv.set_[i] != lhv.set_[i]) {
            return false;
        }
    }
    return true;
}

