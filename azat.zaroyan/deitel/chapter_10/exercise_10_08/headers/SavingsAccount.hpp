#ifndef __SAVINGS_ACCOUNT_HPP__
#define __SAVINGS_ACCOUNT_HPP__

class SavingsAccount
{
    static int annualInterestRate;
public:
    static void modifyInterestRate(const int rate);
public:
    SavingsAccount();
    SavingsAccount(const int balance);
    int getBalance() const;
    void setBalance(const int balance);
    int calculateMonthlyInterest() const;
    void print() const;
private:
    int savingsBalance_;

};

#endif //__SAVINGS_ACCOUNT_HPP__

