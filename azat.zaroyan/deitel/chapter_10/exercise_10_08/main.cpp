#include "headers/SavingsAccount.hpp"
#include <iostream>
#include <unistd.h>

void 
inputSavingsAccount(int& balance) 
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input balance: " << std::endl;
    }
    std::cin >> balance;
}

int 
main()
{
    int balance1;
    inputSavingsAccount(balance1);
    SavingsAccount saver1(balance1);
    saver1.print();
    SavingsAccount::modifyInterestRate(4);
    saver1.print();

    return 0;
}

