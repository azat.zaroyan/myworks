#include "headers/SavingsAccount.hpp"
#include <iostream>

int SavingsAccount::annualInterestRate = 3;

SavingsAccount::SavingsAccount()
    : savingsBalance_(0)
{
}

SavingsAccount::SavingsAccount(const int balance)
    : savingsBalance_(balance)
{
}

int
SavingsAccount::getBalance() const 
{
    return savingsBalance_;
}

void
SavingsAccount::setBalance(const int balance)
{
    savingsBalance_ = balance;
}

int
SavingsAccount::calculateMonthlyInterest() const
{
    return ((savingsBalance_ * annualInterestRate) / 100) / 12;
}

void
SavingsAccount::print() const
{
    std::cout << "Savings balance - " << savingsBalance_ << '$' << std::endl;
    std::cout << "Interest rate - " << annualInterestRate << '%' << std::endl;
    std::cout << "Monthly interest - " << calculateMonthlyInterest() << '$' << std::endl;
}

void
SavingsAccount::modifyInterestRate(const int rate) 
{
    annualInterestRate = rate;
}

