#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <typeinfo>

template <typename T>
bool isEqualTo(const T& lhv, const T& rhv);

template <typename T>
T input();


void
print(const bool areEqual);

struct Circle
{
    friend std::istream& operator>>(std::istream& input, Circle& circle)
    {
        input >> circle.radius_;
        if (!(circle.radius_ > 0)) { 
            std::cerr << "Error 1:Invalid radius! " << std::endl;
            ::exit(1);
        }
        return input;
    }
public:
    double getRadius() const { return radius_; }
    bool operator==(const Circle& rhv) const { return rhv.radius_ == radius_; } 
private:
    double radius_;
};

struct Dot
{
    friend std::istream& operator>>(std::istream& input, Dot& dot)
    {
        input >> dot.x_ >> dot.y_;
        return input;
    }

private:
    double x_;
    double y_;
};

int
main()
{
    const int num1 = input<int>();
    const int num2 = input<int>();
    print(isEqualTo(num1, num2));
    const double num3 = input<double>();
    const double num4 = input<double>();
    print(isEqualTo(num3, num4));
    const Circle circle1 = input<Circle>();
    const Circle circle2 = input<Circle>();
    print(isEqualTo(circle1, circle2));
    const Dot dot1 = input<Dot>();
    const Dot dot2 = input<Dot>();
    print(isEqualTo(dot1, dot2)); //error: ‘const struct Dot’ has no member named ‘operator==’
    return 0;
}

template <typename T>
bool
isEqualTo(const T& lhv,const T& rhv)
{
    return lhv == rhv;
}

template <typename T>
T
input()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input " << typeid(T).name() << ": " << std::endl;
    }
    T obj;
    std::cin >> obj;
    return obj;
}

void
print(const bool areEqual)
{
    if (areEqual) {
        std::cout << "Yes, is equal. " << std::endl;
    } else {
        std::cout << "No, is not equal. " << std::endl;
    }
}

