#include <iostream>
#include <unistd.h>
#include <stdlib.h>
#include <cassert>

template <typename T>
void printArray(const T* array, const int size, const int lowSubscript, const int higeSubscript);

template <typename T>
T* inputArray(int& size); 

void
inputRange(int& lowSubscript, int& higeSubscript);

int
main()
{
    int size1, size2, size3;
    int* array1 = inputArray<int>(size1);
    double* array2 = inputArray<double>(size2);
    char* array3 = inputArray<char>(size3);
    int lowSubscript1, higeSubscript1;
    inputRange(lowSubscript1, higeSubscript1);
    printArray(array1, size1, lowSubscript1, higeSubscript1);
    printArray(array2, size2, lowSubscript1, higeSubscript1);
    printArray(array3, size3, lowSubscript1, higeSubscript1);

    return 0;
}

template <typename T>
void
printArray(const T* array, const int size, const int lowSubscript, const int higeSubscript)
{
    assert(size > 0);
    if (!(higeSubscript > lowSubscript) || lowSubscript < 0 || higeSubscript > size) {
        std::cout << '0' << std::endl;
    } else {
        for (int i = lowSubscript; i < higeSubscript; ++i) {
            std::cout << array[i] << "  ";
        }
        std::cout << std::endl;
    }
}

template <typename T>
T*
inputArray(int& size)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input array size: ";
    }
    std::cin >> size;
    if (!(size> 0)) {
        std::cerr << "Error 1:Negative size! " << std::endl;
        exit(1);
    }
    T* array = new T[size];
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input array: ";
    }
    for (int i = 0; i < size; ++i) {
        std::cin >> array[i];
    }
    return array;
}

void
inputRange(int& lowSubscript, int& higeSubscript)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input low subscript and hige subscript: " << std::endl;
    }
    std::cin >> lowSubscript >> higeSubscript;
}

