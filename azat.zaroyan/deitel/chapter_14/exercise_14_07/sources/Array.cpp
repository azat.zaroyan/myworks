#include <iostream>
#include "headers/Array.hpp"
#include <cassert>

template <typename elementType, int numberOfElements>
Array<elementType, numberOfElements>::Array()
    : array_(new elementType[numberOfElements])
{
}

template <typename elementType, int numberOfElements>
Array<elementType, numberOfElements>::Array(const elementType* array)
    : array_(new elementType[numberOfElements])
{
    for (int i = 0; i < numberOfElements; ++i) {
        array_[i] = array[i];
    }
}

template <typename elementType, int numberOfElements>
Array<elementType, numberOfElements>::Array(const Array<elementType, numberOfElements>& rhv)
    : array_(new elementType[numberOfElements])
{
    for (int i = 0; i < numberOfElements; ++i) {
        array_[i] = rhv.array_[i];
    }
}

template <typename elementType, int numberOfElements>
Array<elementType, numberOfElements>::~Array()
{
    if (array_ != NULL) {
        delete [] array_;
        array_ = NULL;
    }
}

template <typename elementType, int numberOfElements>
inline int
Array<elementType, numberOfElements>::getSize() const
{ 
    return numberOfElements;
}

template <typename elementType, int numberOfElements>
const Array<elementType, numberOfElements>&
Array<elementType, numberOfElements>::operator=(const Array<elementType, numberOfElements>& rhv)
{
    if (&rhv != this) {
        for (int i  = 0; i < numberOfElements; ++i) {
            array_[i] = rhv.array_[i];
        }
    }
    return *this;
}

template <typename elementType, int numberOfElements>
bool
Array<elementType, numberOfElements>::operator==(const Array<elementType, numberOfElements>& rhv) const
{
    if (&rhv == this) {
        return true;
    }
    for (int i  = 0; i < numberOfElements; ++i) {
        if (array_[i] != rhv.array_[i]) {
            return false;
        }
    }
    return true;
}

template <typename elementType, int numberOfElements>
bool
Array<elementType, numberOfElements>::operator!=(const Array<elementType, numberOfElements>& rhv) const
{
    return !(*this == rhv);
}

template <typename elementType, int numberOfElements>
elementType&
Array<elementType, numberOfElements>::operator[](const int index)
{
    assert(index < 0 && index < numberOfElements);
    return array_[index];
}

template <typename elementType, int numberOfElements>
elementType
Array<elementType, numberOfElements>::operator[](const int index) const
{
    assert(index < 0 && index < numberOfElements);
    return array_[index];
}

template <typename elementType, int numberOfElements>
void
Array<elementType, numberOfElements>::print() const
{
    for (int i = 0; i < numberOfElements; ++i) {
        std::cout << array_[i] << "  ";
    }
    std::cout << std::endl;
}

