#ifndef __ARRAY_HPP__
#define __ARRAY_HPP__
#include <iostream>

template <typename elementType, int numberOfElements>
class Array
{
public:
    Array();
    Array(const Array<elementType, numberOfElements>& array);
    Array(const elementType* array);
    ~Array();
    int getSize() const;
    const Array<elementType, numberOfElements>& operator=(const Array<elementType, numberOfElements>& rhv);
    bool operator==(const Array<elementType, numberOfElements>& rhv) const;
    bool operator!=(const Array<elementType, numberOfElements>& rhv) const;
    elementType& operator[](const int index);
    elementType operator[](const int index) const;
    void print() const;
private:
    elementType* array_;
};

#include "sources/Array.cpp"

#endif ///__ARRAY_HPP__

