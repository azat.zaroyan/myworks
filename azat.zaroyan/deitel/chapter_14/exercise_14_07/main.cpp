#include "headers/Array.hpp"
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <stdlib.h>

template <typename T>
T* inputArray(const int size);

int
main()
{
    const int size = 8;
    int* array = inputArray<int>(size);
    Array<int, size> array1(array);
    array1.print();
    delete [] array;
    return 0;
}

template <typename T>
T*
inputArray(const int size)
{
    T* array = new T[size];
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input array: ";
    }
    for (int i = 0; i < size; ++i) {
        std::cin >> array[i];
    }
    return array;
}

