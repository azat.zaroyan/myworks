#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <stdlib.h>

template <typename T>
void 
selectionSort(T* array, const int size);

template <typename T>
void 
swap(T* ptr1, T* ptr2);

template <typename T>
T*
inputArray(int& size);

template <typename T>
void
print(const T* array, const int size);

int 
main()
{
    int size1, size2;
    int* array1 = inputArray<int>(size1);
    double* array2 = inputArray<double>(size2);
    print(array1, size1);
    selectionSort(array1, size1);
    print(array1, size1);
    print(array2, size2);
    selectionSort(array2, size2);
    print(array2, size2);
    delete [] array1;
    delete [] array2;
    return 0;
    
}

template <typename T>
void
selectionSort(T* array, const int size)
{
    for (int i = 0; i < size - 1; ++i) {
        int smallest = i;
        for (int index = i + 1; index < size; ++index) {
            if (array[smallest] > array[index]) {
                smallest = index;
            }
        }
        swap(&array[i], &array[smallest]);
    }
}

template <typename T>
void 
swap(T* ptr1, T* ptr2)
{
    T hold = *ptr2;
    *ptr2 = *ptr1;
    *ptr1 = hold;
}

template <typename T>
T*
inputArray(int& size)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input array size: ";
    }
    std::cin >> size;
    if (!(size> 0)) {
        std::cerr << "Error 1:Negative size! " << std::endl;
        exit(1);
    }
    T* array = new T[size];
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input array: ";
    }
    for (int i = 0; i < size; ++i) {
        std::cin >> array[i];
    }
    return array;
}

template <typename T>
void
print(const T* array, const int size) 
{
    for (int i = 0; i < size; ++i) {
        std::cout << std::setw(4) << array[i];
    }
    std::cout << std::endl;
}

