#include "headers/Array.hpp"
#include <iostream>
#include <cassert>

template <typename T>
Array<T>::Array()
    : size_(0)
{
    assert(size_ > 0);
}

template <typename T>
Array<T>::Array(const T* array, const int size)
    : array_(new T[size])
    , size_(size)
{
    assert(size_ > 0);
    for (int i = 0; i < size_; ++i) {
        array_[i] = array[i];
    }
}

template <typename T>
inline T*
Array<T>::getArray() const
{
    return array_;
}

template <typename T>
inline int
Array<T>::getSize() const
{
    return size_;
}

template <typename T>
inline void 
Array<T>::print() const
{
    for (int i = 0; i < size_; ++i) {
        std::cout << array_[i] << "  ";
    }
    std::cout << std::endl;
}

Array<float>::Array()
    : size_(0)
{
    assert(size_ > 0);
}

Array<float>::Array(const float* array, const int size)
    : array_(new float[size])
    , size_(size)
{
    assert(size_ > 0);
    for (int i = 0; i < size_; ++i) {
        array_[i] = array[i];
    }
}

inline float*
Array<float>::getArray() const
{
    return array_;
}

inline int
Array<float>::getSize() const
{
    return size_;
}

inline void 
Array<float>::print() const
{
    std::cout << "This is a float type array. " << std::endl;
    for (int i = 0; i < size_; ++i) {
        std::cout << array_[i] << "  ";
    }
    std::cout << std::endl;
}

