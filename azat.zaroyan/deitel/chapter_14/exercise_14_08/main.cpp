#include "headers/Array.hpp"
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <stdlib.h>

template <typename T>
T*
inputArray(int& size);

int main()
{
    int size1, size2;
    int* array1 = inputArray<int>(size1);
    float* array2 = inputArray<float>(size2);
    const Array<int> arr1(array1, size1);
    const Array<float> arr2(array2, size2);
    arr1.print();
    arr2.print();
    return 0;
}

template <typename T>
T*
inputArray(int& size)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input array size: ";
    }
    std::cin >> size;
    if (!(size> 0)) {
        std::cerr << "Error 1:Negative size! " << std::endl;
        exit(1);
    }
    T* array = new T[size];
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input array: ";
    }
    for (int i = 0; i < size; ++i) {
        std::cin >> array[i];
    }
    return array;
}

