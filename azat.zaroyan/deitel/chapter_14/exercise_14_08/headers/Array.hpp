#ifndef __ARRAY_HPP__
#define __ARRAY_HPP__

template <typename T>
class Array
{
public:
    Array();
    Array(const T* array, const int size);
    T* getArray() const;
    int getSize() const;
    void print() const;
private:
    T* array_;
    int size_;
};

template <>
class Array<float>
{
public:
    Array();
    Array(const float* array, const int size);
    float* getArray() const;
    int getSize() const;
    void print() const;
private:
    float* array_;
    int size_;
};

#include "sources/Array.cpp"

#endif ///__ARRAY_HPP__

