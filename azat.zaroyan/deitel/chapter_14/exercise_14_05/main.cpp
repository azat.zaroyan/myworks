#include <iostream>
#include <unistd.h>
#include <stdlib.h>
#include <cassert>
#include <string>

void printArray(const std::string* str, const size_t size);
std::string* inputArray(size_t& size); 

int
main()
{
    size_t size;
    std::string* str = inputArray(size);
    printArray(str, size);
    delete [] str;
    return 0;
}

std::string*
inputArray(size_t& size)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input size: ";
    }
    std::cin >> size;
    if (size <= 0) {
        std::cerr << "Error 1: Invalid size!" << std::endl;
        ::exit(1);
    }
    std::string* str = new std::string[size];
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input array: ";
    }
    for (size_t i = 0; i < size; ++i) {
        std::cin >> str[i];
    }
    return str;
}

void
printArray(const std::string* str, const size_t size)
{
    for (size_t i = 0; i < size; ++i){
        for (size_t j = 0; j < str[i].size(); ++j) {
            std::cout << str[i][j] << "  ";
        }
        std::cout << std::endl;
    }
}

