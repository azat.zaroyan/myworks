#include "headers/DoubleSubscriptedArray.hpp"
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cassert>
#include <string>
#include <vector>
#include <sstream>
#include <algorithm>

namespace {

void 
strToArray(const std::string& str, std::vector<std::string>& v)
{
    std::string temp;
    std::stringstream s(str);
    while (s >> temp) {
        v.push_back(temp);
    }
}

int
strToInt(const std::string& str)
{
    int temp;
    std::stringstream s(str);
    s >> temp;
    return temp;
}

}

DoubleSubscriptedArray::DoubleSubscriptedArray(const int rows, const int columns) 
    : rows_(rows)
    , columns_(columns)
    , matrix_(new int[rows_ * columns_])
{
    for (int i = 0; i < rows_ * columns_; ++i) {
        matrix_[i] = 0;
    }
}

DoubleSubscriptedArray::DoubleSubscriptedArray(const DoubleSubscriptedArray& rhv)
    : rows_(rhv.rows_)
    , columns_(rhv.columns_)
    , matrix_(new int[rows_ * columns_])
{
    for (int i = 0; i < rows_ * columns_; ++i) {
        matrix_[i] = rhv.matrix_[i];
    }
}

DoubleSubscriptedArray::~DoubleSubscriptedArray()
{
    if (matrix_ != NULL) {
        delete [] matrix_;
        matrix_ = NULL;
    }
}

int
DoubleSubscriptedArray::getRows() const
{
    return rows_;
}

int
DoubleSubscriptedArray::getColumns() const
{
    return columns_;
}

const DoubleSubscriptedArray& 
DoubleSubscriptedArray::operator=(const DoubleSubscriptedArray& rhv)
{
    if (rows_ * columns_ != rhv.rows_ * rhv.columns_) { 
        delete [] matrix_;
        matrix_ = new int [rhv.rows_ * rhv.columns_];
    }
    rows_ = rhv.rows_;
    columns_ = rhv.columns_;
    for (int i = 0; i < rows_ * columns_; ++i) {
        matrix_[i] = rhv.matrix_[i];
    }
    return *this;
}

bool
DoubleSubscriptedArray::operator==(const DoubleSubscriptedArray& rhv) const 
{
    if (rows_ != rhv.rows_ || columns_ != rhv.columns_) {
        return false;
    }
    for (int i = 0; i < rows_ * columns_; ++i) {
        if (matrix_[i] != rhv.matrix_[i]) {
            return false;
        }
    }
    return true;
}

bool
DoubleSubscriptedArray::operator!=(const DoubleSubscriptedArray& rhv) const
{
    return !(*this == rhv);
}

int 
DoubleSubscriptedArray::operator()(const int rows, const int columns) const
{
    assert (rows < rows_ || rows > 0 || columns < columns_ || columns > 0);
    return matrix_[rows * columns_ + columns];
}

int& 
DoubleSubscriptedArray::operator()(const int rows, const int columns)
{
    assert (rows < rows_ || rows > 0 || columns < columns_ || columns > 0);
    return matrix_[rows * columns_ + columns];
}

std::istream&
operator>>(std::istream& input, DoubleSubscriptedArray& matrix) 
{
    char s1;
    std::cin >> s1;
    if (s1 != '{') {
        std::cerr << "Error 1: This is not a matrix!";
        ::exit(1);
    }

    std::string buffer, line;
    std::getline(std::cin, buffer, '}');

    matrix.columns_ = -1;
    matrix.rows_ = 1 + std::count(buffer.begin(), buffer.end(), '\n');
    
    int rows = 0;
    std::stringstream s(buffer);
    while (std::getline(s, line) != NULL) {
        std::vector<std::string> tokens;
        strToArray(line, tokens);
        const int columns = static_cast<int>(tokens.size());
        if (-1 == matrix.columns_) {
            matrix.~DoubleSubscriptedArray();
            matrix.columns_ = columns;
            matrix.matrix_ = new int[matrix.rows_ * matrix.columns_];
        }
        if (columns != matrix.columns_) {
            std::cerr << "Error 2: Invalid Matrix!" << std::endl;
            ::exit(2);
        }
        for (int i = 0; i < columns; ++i) {
            matrix.matrix_[columns * rows + i] = strToInt(tokens[i]);
        }
        ++rows;
    }
    return input;
}

std::ostream& 
operator<<(std::ostream& output, const DoubleSubscriptedArray& matrix) 
{
    output << '{' << ' ';
    for (int i = 0; i < matrix.rows_; ++i) {
        for (int j = 0; j < matrix.columns_; ++j) {
            output << matrix(i, j) << ' ';      
        }
        if (matrix.rows_ - 1 == i) {
            output << '}';
        }
        output << '\n' << "  ";
    }
    return output;
}


