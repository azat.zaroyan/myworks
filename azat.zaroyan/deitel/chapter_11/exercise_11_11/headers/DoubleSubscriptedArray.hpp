#ifndef __DOUBLE_SUBSCRIPTED_ARRAY_HPP__
#define __DOUBLE_SUBSCRIPTED_ARRAY_HPP__
#include <iostream>

class DoubleSubscriptedArray
{
    friend std::istream& operator>>(std::istream& input, DoubleSubscriptedArray& matrix);
    friend std::ostream& operator<<(std::ostream& output, const DoubleSubscriptedArray& matrix);
public:
    DoubleSubscriptedArray(const int rows = 0, const int columns = 0);
    DoubleSubscriptedArray(const DoubleSubscriptedArray& rhv);
    ~DoubleSubscriptedArray();
    int getRows() const;
    int getColumns() const;
    const DoubleSubscriptedArray& operator=(const DoubleSubscriptedArray& rhv);
    bool operator==(const DoubleSubscriptedArray& rhv) const;
    bool operator!=(const DoubleSubscriptedArray& rhv) const;
    int& operator()(const int rows, const int columns);
    int operator()(const int rows, const int columns) const;
private:
    int rows_;
    int columns_;
    int* matrix_;

};

#endif //__DOUBLE_SUBTRACTED_ARRAY_HPP__

