#ifndef __COMPLEX_HPP__
#define __COMPLEX_HPP__

class Complex
{
public:
    Complex();
    Complex(const double realPart, const double imaginaryPart);
    Complex add(const Complex& rhv) const;
    Complex remove(const Complex& rhv) const;
    void print() const;

private:
    double realPart_;
    double imaginaryPart_;
    
};

#endif /// __COMPLEX_HPP__

