#include "Complex.hpp"

#include <iostream>

Complex::Complex()
     : realPart_(0)
     , imaginaryPart_(0)
{
}

Complex::Complex(const double realPart, const double imaginaryPart)
    : realPart_(realPart)
    , imaginaryPart_(imaginaryPart)
{
}

Complex
Complex::add(const Complex& rhv) const
{
    const double realPart = realPart_ + rhv.realPart_;
    const double imaginaryPart = imaginaryPart_ + rhv.imaginaryPart_;
    const Complex sum(realPart, imaginaryPart);
    return sum;
}
   
Complex
Complex::remove(const Complex& rhv) const
{        
    const double realPart =  realPart_ - rhv.realPart_;
    const double imaginaryPart = imaginaryPart_ - rhv.imaginaryPart_;
    const Complex dif(realPart, imaginaryPart);
    return dif;
}

void 
Complex::print() const
{  
    std::cout << "(" << realPart_;
    std::cout << (imaginaryPart_ > 0 ? " + " : " - ");
    std::cout << std::abs(imaginaryPart_) << "i)";
}

