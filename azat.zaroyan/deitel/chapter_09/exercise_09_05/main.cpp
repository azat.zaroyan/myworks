#include "Complex.hpp"

#include <iostream>
#include <unistd.h>

void
inputComplexNumber(double& real, double& imaginary)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input real and imaginary numbers: ";
    }
    std::cin >> real >> imaginary;
}

void
printOperation(const Complex& operand1, const Complex& operand2, const Complex& result, const char operation)
{
    operand1.print();
    std::cout << ' ' << operation << ' ';
    operand2.print();
    std::cout << " = ";
    result.print();
    std::cout << std::endl;
}

int 
main()
{
    double firstRealNumber, firstImaginaryNumber, secondRealNumber, secondImaginaryNumber;
    inputComplexNumber(firstRealNumber, firstImaginaryNumber);
    inputComplexNumber(secondRealNumber, secondImaginaryNumber);

    const Complex complex1(firstRealNumber, firstImaginaryNumber);
    const Complex complex2(secondRealNumber, secondImaginaryNumber);
    const Complex complex3 = complex1.add(complex2);
    const Complex complex4 = complex1.remove(complex2);

    printOperation(complex1, complex2, complex3, '+');
    printOperation(complex1, complex2, complex4, '-');
    return 0;
}

