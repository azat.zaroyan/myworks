#include "DateAndTime.hpp"

#include <iostream>
#include <iomanip>


DateAndTime::DateAndTime()
    : day_(1)
    , month_(1)
    , year_(2000)
    , hour_(0)
    , minute_(0)
    , second_(0)
{
}

DateAndTime::DateAndTime(const int day, const int month, const int year, const int hour, const int minute, const int second)
{
    setDateAndTime(day, month, year, hour, minute, second);
}

void
DateAndTime::setDateAndTime(const int day, const int month, const int year,const int hour, const int minute, const int second)
{
    setDay(day);
    setMonth(month);
    setYear(year);
    setHour(hour); 
    setMinute(minute);
    setSecond(second);
}

inline void
DateAndTime::setHour(const int hour)
{
    hour_ = (hour >= 0 && hour < 24) ? hour : 0;
}

inline void
DateAndTime::setMinute(const int minute)
{
    minute_ = (minute >= 0 && minute < 60) ? minute : 0;
}

inline void
DateAndTime::setSecond(const int second)
{
    second_ = (second >= 0 && second < 60) ? second : 0;
}

inline void
DateAndTime::setDay(const int day)
{
    day_ = (day > 0 && day <= 30) ? day : 0;
}

inline void 
DateAndTime::setMonth(const int month)
{
    month_ = (month > 0 && month <= 12) ? month : 0;
}

inline void 
DateAndTime::setYear(const int year)
{
    year_ = (year > 0) ? year : 0;
}

inline int 
DateAndTime::getDay() const
{
    return day_;
}

inline int 
DateAndTime::getMonth() const
{
    return month_;
}

inline int 
DateAndTime::getYear() const
{
    return year_;
}

inline int
DateAndTime::getHour() const
{
    return hour_;
}

inline int
DateAndTime::getMinute() const
{
    return minute_;
}

inline int
DateAndTime::getSecond() const
{
    return second_;
}

void
DateAndTime::tick()
{
    setSecond((getSecond() + 1) % 60);
    if (0 == getSecond()) {
        setMinute((getMinute() + 1) % 60);
        if (getMinute() == 0) {
            setHour((getHour() + 1) % 24);
            if (0 == getSecond()) {
                nextDay();
            }
        }
    }
}

void
DateAndTime::nextDay()
{
    day_ = (day_ % 30) + 1 ;
    if (1 == day_) {
        month_ = (month_ % 12) +1;
        if (1 == month_) {
            ++year_;
        }
    }
}

void
DateAndTime::printUniversal() const
{
    std::cout << std::setfill('0') << std::setw(2) << getDay() << "/" << std::setw(2) 
              << getMonth() << "/" << getYear() << std::endl 
              << std::setw( 2 ) << getHour() << ":"
              << std::setw(2) << getMinute() << ":" 
              << std::setw(2) << getSecond () << std::endl;

}

void DateAndTime::printStandard() const
{
    std::cout << std::setfill('0')  << std::setw(2) << getDay() << "/"
              << std::setw(2) << getMonth() << "/" << getYear() << std::endl
              << ((getHour()  == 0 || getHour() == 12) ? 12 : getHour() % 12)
              << ":" << std::setw( 2 ) << getMinute() << ":"
              << std::setw( 2 ) << getSecond() << ( getHour() < 12 ? " AM" : " PM" ) << std::endl;
}

