#include "DateAndTime.hpp"
#include <iostream>
#include <unistd.h>

void 
inputDateAndTime(int& day, int& month, int& year, int& hour, int& minute, int& second)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input day, month, year, hour, minute and second:";
    }
    std::cin >> day >> month >> year >> hour >> minute >> second;
}

int 
main()
{
    int day, month, year, hour, minute, second;
    inputDateAndTime(day, month, year, hour, minute, second);
    DateAndTime time1(day, month, year, hour, minute, second);
    time1.printUniversal();
    time1.printStandard();
    time1.tick();
    time1.printUniversal();
    time1.printStandard();

    return 0;
}

