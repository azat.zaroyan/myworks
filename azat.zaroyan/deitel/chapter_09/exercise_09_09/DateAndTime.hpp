#ifndef __DATEANDTIME_HPP__
#define __DATEANDTIME_HPP__


class DateAndTime
{
public: 
    DateAndTime();
    DateAndTime(const int day, const int month, const int year, const int hour, const int minute, const int second);
    
    void setDateAndTime(const int day, const int month, const int year, const int hour, const int minute, const int second); 
    void setDay(const int day);
    void setMonth(const int month);
    void setYear(const int year);
    void setHour(const int hour);
    void setMinute(const int minute);
    void setSecond(const int second);
    
    int getDay() const;
    int getMonth() const;
    int getYear() const;
    int getHour() const;
    int getMinute() const;
    int getSecond() const;
    
    void tick();
    void nextDay();

    void printUniversal() const;
    void printStandard() const; 

private:
    int day_;
    int month_;
    int year_;
    int hour_;
    int minute_;
    int second_;
   
};
#endif /// __DATEANDTIME_HPP__

