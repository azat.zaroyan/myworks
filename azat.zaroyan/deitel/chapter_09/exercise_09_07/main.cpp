#include "Time.hpp"
#include <unistd.h>
#include <iostream>

void 
inputTime(int& hour, int& minute, int& second)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input hour, minute and second:";
    }
    std::cin >> hour >> minute >> second;
}

int 
main()
{
    int hour, minute, second;
    inputTime(hour, minute, second);
    Time time1(hour, minute, second);
    time1.printUniversal();
    time1.printStandard();
    time1.tick();
    time1.printUniversal();
    time1.printStandard();

    return 0;
}

