#include "Time.hpp"
#include <iostream>
#include <iomanip>

Time::Time()
    : hour_(0)
    , minute_(0)
    , second_(0)
{
}

Time::Time(const int hour, const int minute, const int second)
{
    setTime(hour, minute, second);
}

inline void
Time::setTime(const int hour, const int minute, const int second)
{
    setHour(hour); 
    setMinute(minute);
    setSecond(second);
}

inline void
Time::setHour(const int hour)
{
    hour_ = (hour >= 0 || hour < 24) ? hour : 0;
}
inline void
Time::setMinute(const int minute)
{
    minute_ = (minute >= 0 || minute < 60) ? minute : 0;
}
inline void
Time::setSecond(const int second) const
{
    second_ = (second >= 0 || second < 60) ? second : 0;
}

inline int
Time::getHour() const
{
    return hour_;
}

inline int
Time::getMinute() const
{
    return minute_;
}

inline int
Time::getSecond() const
{
    return second_;
}

void
Time::tick()
{
    setSecond((getSecond() + 1) % 60);
    if (getSecond() == 0) {
        setMinute((getMinute() + 1) % 60);
    }
    if (getMinute() == 0) {
        setHour((getHour() + 1) % 24);
    }
}

void
Time::printUniversal() const
{
    std::cout << std::setfill( '0' ) << std::setw( 2 ) << getHour() << ":"
    << std::setw( 2 ) << getMinute () << ":" << std::setw( 2 ) << getSecond () << std::endl;
}

void 
Time::printStandard() const
{
    std::cout << ((getHour() == 0 || getHour() == 12) ? 12 : getHour()%12)
    << ":" << std::setfill( '0' ) << std::setw( 2 ) << getMinute() << ":"
    << std::setw( 2 ) << getSecond() << ( hour_ < 12 ? " AM" : " PM" ) << std::endl;
}

