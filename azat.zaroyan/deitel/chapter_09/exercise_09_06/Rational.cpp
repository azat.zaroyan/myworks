#include "Rational.hpp"
#include <iostream>
#include <assert.h>

Rational::Rational()
    : numinator_(0)
    , denominator_(1)
{
}

Rational::Rational(const int numinator, const int denominator)
    : numinator_(numinator)
    , denominator_(denominator)
{
    assert(denominator_ != 0);
    reduce();
}

Rational::Rational(const Rational& rhv)
    : numinator_(rhv.numinator_)
    , denominator_(rhv.denominator_)
{
}

Rational
Rational::add(const Rational& rhv) const
{
    const int numinator = numinator_ * rhv.denominator_ + rhv.numinator_ * denominator_;
    const int denominator = denominator_ * rhv.denominator_;
    const Rational sum(numinator, denominator);
    return sum;
}

Rational
Rational::remove(const Rational& rhv) const
{
    const int numinator = numinator_ * rhv.denominator_ - rhv.numinator_ * denominator_;
    const int denominator = denominator_ * rhv.denominator_;
    const Rational dif(numinator, denominator);
    return dif;
}

Rational
Rational::multiply(const Rational& rhv) const
{
    const int numinator = numinator_ * rhv.numinator_;
    const int denominator = denominator_ * rhv.denominator_;
    const Rational res(numinator, denominator);
    return res;
}

Rational
Rational::divide(const Rational& rhv) const
{
    const int numinator = numinator_ * rhv.denominator_;
    const int denominator = denominator_ * rhv.numinator_;
    const Rational quo(numinator, denominator);
    return quo;
}

void 
Rational::printRational() const
{ 
    if (numinator_ == denominator_) {
        std::cout << 1;
        return;
    }
   
    std::cout << '(' << numinator_ << " / " << denominator_ << ')';
}


void
Rational::print() const
{         
    std::cout << static_cast<double>(numinator_) / denominator_;
}


void
Rational::reduce()
{
    if (numinator_ != 0) { 
        int n = std::abs(numinator_);
        int d = std::abs(denominator_);
        while (n != d) {
            if (n > d) {
                n -= d;
            } else {
                d -= n;
            }
        }
        numinator_ /= n;
        denominator_ /= d;
    }
    if (denominator_ < 0 ) {
        numinator_ = -numinator_;
        denominator_ = -denominator_;
     }
}

