#include "Rational.hpp"

#include <iostream>
#include <unistd.h>

void
inputRational(double& numinator, double& denominator)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input real and imaginary numbers: ";
    }
    std::cin >> numinator >> denominator;
    if (static_cast<int>(denominator) == 0) {
        std::cerr << "Error 1: The denominator cannot be zero" << std::endl;
        ::exit(1);
    }
 }  

void
printOperation(const Rational& operand1, const Rational& operand2, const Rational& result, const char operation)
{
    operand1.print();
    std::cout << ' ' << operation << ' ';
    operand2.print();
    std::cout << " = ";
    result.print();
    std::cout << std::endl;
}

void
printRationalOperation(const Rational& operand1, const Rational& operand2, const Rational& result, const char operation)
{
    operand1.printRational();
    std::cout << ' ' << operation << ' ';
    operand2.printRational();
    std::cout << " = ";
    result.printRational();
    std::cout << std::endl;
}

int
main()
{

    double firstNuminator, firstDenominator, secondNuminator, secondDenominator;
    inputRational(firstNuminator, firstDenominator);
    inputRational(secondNuminator, secondDenominator);

    const Rational rational1(firstNuminator, firstDenominator);
    const Rational rational2(secondNuminator, secondDenominator);
    const Rational rational3 = rational1.add(rational2);
    
    printOperation(rational1, rational2, rational3, '+');
    printRationalOperation(rational1, rational2, rational3, '+');

    return 0;
}

