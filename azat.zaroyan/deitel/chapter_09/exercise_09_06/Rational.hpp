#ifndef __RATIONAL_HPP__
#define __RATIONAL_HPP__

class Rational 
{
public:
    Rational();
    Rational(const int numinator, const int denominator);
    Rational(const Rational& rhv);
    Rational add(const Rational& rhv) const;
    Rational remove(const Rational& rhv) const;
    Rational multiply(const Rational& rhv) const;
    Rational divide(const Rational& rhv) const;
    void printRational() const;
    void print() const;

private:
    void reduce();

private:
    int numinator_;
    int denominator_;

};

#endif /// __RATIONAL_HPP__

