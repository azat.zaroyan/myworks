#ifndef __RATIONAL_HPP__
#define __RATIONAL_HPP__

struct Vertex 
{
public:
    Vertex();
    Vertex(const int x, const int y);
    void setVertex(const int x, const int y);
    void setX(const int x);
    void setY(const int y);
    int getX() const;
    int getY() const;
private:
    int x_;
    int y_;

};

class Rectangle
{  
public:
    static double distance(const Vertex& a, const Vertex& b); 
public:
    Rectangle();
    Rectangle(const Vertex& a, const Vertex& b, const Vertex& c, const Vertex& d);
    double length() const;
    double width() const;
    double perimeter() const;
    double area() const;
    bool isSquare() const;
    void print() const;

private:
    Vertex a_;
    Vertex b_;
    Vertex c_;
    Vertex d_;
};


#endif ///__RATIONAL_HPP__

