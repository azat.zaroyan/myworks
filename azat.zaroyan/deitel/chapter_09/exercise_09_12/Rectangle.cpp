#include "Rectangle.hpp"
#include <iostream>
#include <cmath>

Vertex::Vertex()
    : x_(0)
    , y_(0)
{
}

Vertex::Vertex(const int x, const int y)
    : x_(x)
    , y_(y)
{
}

inline void
Vertex::setVertex(const int x, const int y)
{
    setX(x);
    setY(y);
}

inline void
Vertex::setX(const int x)
{
    x_ = x;
}

inline void 
Vertex::setY(const int y)
{
    y_ = y;
}

inline int 
Vertex::getX() const
{ 
    return x_;
}

inline int 
Vertex::getY() const
{
    return y_;
}

Rectangle::Rectangle()
    : a_(0, 0)
    , b_(0, 0)
    , c_(0, 0)
    , d_(0, 0)
{
}

Rectangle::Rectangle(const Vertex& a, const Vertex& b, const Vertex& c, const Vertex& d)
    : a_(a)
    , b_(b)
    , c_(c)
    , d_(d)
{
}


inline double 
Rectangle::length() const
{ 
    const double ab = Rectangle::distance(a_, b_);
    const double bc = Rectangle::distance(b_, c_);
    return (ab > bc) ? ab : bc;

}

inline double 
Rectangle::width() const
{
    const double ab = Rectangle::distance(a_, b_);
    const double bc = Rectangle::distance(b_, c_);
    return (ab < bc) ? ab : bc;
}

inline double
Rectangle::perimeter() const
{
    return 2 * (length() + width());
}

inline double
Rectangle::area() const
{
    return length() * width();
}

inline bool 
Rectangle::isSquare() const
{
     return length() == width();
}

void 
Rectangle::print() const
{
    std::cout << "length - " << length() << std::endl;
    std::cout << "width - " << width() << std::endl;
    std::cout << "perimeter - " << perimeter() << std::endl;
    std::cout << "area - " << area() << std::endl;
    if (isSquare()) {
        std::cout << "Yes, this is square." << std::endl;
    } else {
        std::cout << "No, this is not a square." << std::endl;
    }
}

double 
Rectangle::distance(const Vertex& a, const Vertex& b)
{  
    return std::sqrt(((a.getX() - b.getX()) * (a.getX() - b.getX())) + ((a.getY() - b.getY()) * (a.getY() - b.getY())));
}

