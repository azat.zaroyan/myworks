#ifndef __DATE_HPP__
#define __DATE_HPP__

class Date
{
public:
    Date();
    Date(int day, int month, int year);
    void print() const;
    void nextDay();

private:
    int day_;
    int month_;
    int year_;
};

#endif /// __DATE_HPP__

