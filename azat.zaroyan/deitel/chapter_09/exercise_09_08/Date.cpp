#include "Date.hpp"
#include <iostream>

Date::Date()
    : day_(1)
    , month_(1)
    , year_(2000)
{
}

Date::Date(int day, int month, int year)
    : day_(day)
    , month_(month)
    , year_(year)
{
}

void
Date::print() const
{
    std::cout << day_ << '/' << month_ << '/' << year_ << std::endl;
}

void
Date::nextDay()
{
    day_ = (day_ % 30) + 1 ;
    if (1 == day_) {
        month_ = (month_ % 12) + 1;
        if (1 == month_) {
            ++year_;
        }
    }
}

