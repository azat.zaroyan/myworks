#include "Date.hpp"
#include <iostream>
#include <unistd.h>

void 
inputDate(int& day, int& month, int& year)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input day, month and year:";
    }
    std::cin >> day >> month >> year;
}

int
main()
{
    int day, month, year;
    inputDate(day, month, year);
    Date date1(day, month, year);
    date1.print();
    date1.nextDay();
    date1.print();
    return 0;
}

