#include "Rectangle.hpp"
#include <iostream>
#include <assert.h>

Rectangle::Rectangle()
    : length_(1)
    , width_(1)
{
}

Rectangle::Rectangle(const double length, const double width)
{
    setRectangle(length, width);
}

inline void
Rectangle::setRectangle(const double length, const double width)
{
    setLength(length);
    setWidth(width);
}

inline void
Rectangle::setLength(const double length)
{   
    assert(length > 0 || length < 20.0);
    length_ = length;
}

inline void
Rectangle::setWidth(const double width)
{   
    assert(width > 0 || width < 20.0);
    width_ = width;
}

inline double 
Rectangle::getLength() const
{
    return length_;
}

inline double
Rectangle::getWidth() const
{
    return width_;
}

inline double
Rectangle::perimeter() const
{
    return 2 * (length_ + width_);
}

inline double
Rectangle::area() const
{
    return length_ * width_;
}

void 
Rectangle::print() const
{
    std::cout << "length - " << getLength() << std::endl;
    std::cout << "width - " << getWidth() << std::endl;
    std::cout << "perimetr - " << perimeter() << std::endl;
    std::cout << "area - " << area() << std::endl;
}

