#ifndef __RECTANGLE_HPP__
#define __RECTANGLE_HPP__

class Rectangle
{

public:
    Rectangle();
    Rectangle(const double length, const double width);
    void setRectangle(const double length, const double width);
    void setLength(const double length);
    void setWidth(const double width);

    double getLength() const;
    double getWidth() const;

    double perimeter() const;
    double area() const;
    void print() const;
private:
    double length_;
    double width_;
};
#endif /// __REGTANGLE_HPP__

