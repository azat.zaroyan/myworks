#include "Rectangle.hpp"
#include <iostream>
#include <unistd.h>

void
inputRectangle(double& length, double& width)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input length and width:";
    }
    std::cin >> length >> width;
    if (length < 0 || length > 20.0 || width < 0 ||  width > 20.0) {
        std::cerr << "Error 1:Rectangle parameters do not accept values in the range (0, 20)) " << std::endl;
        ::exit(1);
    }
}  

int 
main()
{
    double length, width;
    inputRectangle(length, width);
    const Rectangle rectangle1(length, width);
    rectangle1.print();

    return 0;
}

