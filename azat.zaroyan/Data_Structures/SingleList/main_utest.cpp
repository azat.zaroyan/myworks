#include <iostream>
#include "headers/SingleList.hpp"
#include <gtest/gtest.h>

struct A {
    int data_;
    A() : data_(0) {}
    A(const int data) : data_(data) {}
};


TEST(SingleListTest, Resize)
{
    typedef SingleList<A> L;
    L l;
    l.resize(4);
    EXPECT_EQ(l.size(), 4);

}

TEST(SingleListTest, PushFrontA)
{
    SingleList<A> l1;
    const A a;
    l1.push_front(a);
    EXPECT_EQ(l1.size(), 1);
}

TEST(SingleListTest, ConstIterator)
{
    typedef SingleList<A> L;
    typedef SingleList<A>::const_iterator LCI;
    A a1(6);
    A a2(3);
    L l;
    l.push_front(a1);
    l.push_front(a2);
    LCI it1 = l.begin();
    ++it1;
    EXPECT_EQ(it1->data_, 6);
}

TEST(SingleListTest, RangeConstructor)
{
    typedef SingleList<A> L;
    typedef SingleList<A>::iterator LI;
    A a1(4);
    A a2(3);
    A a3(2);
    A a4(7);
    A* a = new A[3];
    a[0] = a1;
    a[1] = a2;
    a[2] = a3;
    L l(a, a + 3);
    LI it = l.begin();
    EXPECT_EQ(it->data_, 4);
}

TEST(SingleListTest, InsertFunction)
{
    typedef SingleList<A> L;
    typedef SingleList<A>::iterator LI;
    A a1(4);
    A a2(3);
    A a3(2);
    A a4(7);
    A* a = new A[3];
    a[0] = a1;
    a[1] = a2;
    a[2] = a3;
    L l(a, a + 3);
    LI it = l.begin();
    it = l.insert(it, a4);
    EXPECT_EQ(it->data_, 7);
}

TEST(SingleListTest, InsertRangeFunction)
{
    typedef SingleList<A> L;
    typedef SingleList<A>::iterator LI;
    A a1(4);
    A a2(3);
    A a3(2);
    A a4(7);
    A* a = new A[3];
    a[0] = a1;
    a[1] = a2;
    a[2] = a3;
    L l(a, a + 3);
    LI it = l.begin();
    ++it;
    l.insert(it, a, a + 2);
    ++it;
    EXPECT_EQ(it->data_, 4);
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

