#ifndef __SINGLE_LIST_HPP__
#define __SINGLE_LIST_HPP__

template <typename T>
class SingleList
{
    template <typename TT>
    friend bool operator==(const SingleList<TT>& lhv, const SingleList<TT>& rhv);
    template <typename TT>
    friend bool operator!=(const SingleList<TT>& lhv, const SingleList<TT>& rhv);
    template <typename TT>
    friend bool operator<(const SingleList<TT>& lhv, const SingleList<TT>& rhv);
    template <typename TT>
    friend bool operator<=(const SingleList<TT>& lhv, const SingleList<TT>& rhv);
    template <typename TT>
    friend bool operator>(const SingleList<TT>& lhv, const SingleList<TT>& rhv);
    template <typename TT>
    friend bool operator>=(const SingleList<TT>& lhv, const SingleList<TT>& rhv);
private:
    struct Node {
        Node(const T& data = T(), Node* next = NULL);
        T data_;
        Node* next_;
    };

public:
    typedef T                 value_type;
    typedef value_type&       reference;
    typedef const value_type& const_reference;
    typedef value_type*       pointer;
    typedef std::ptrdiff_t    difference_type;
    typedef std::size_t       size_type;
    typedef const value_type* const_pointer;
public:
    class const_iterator {
    public:
        friend class SingleList<T>;
        const_iterator();
        const_iterator(const const_iterator& rhv);
        ~const_iterator();
        const const_iterator& operator=(const const_iterator& rhv);
        bool operator==(const const_iterator& rhv) const;
        bool operator!=(const const_iterator& rhv) const;
        const_reference operator*() const;
        const_pointer operator->() const;
        const const_iterator& operator++();
        const const_iterator operator++(int);
    protected:
        Node* getPtr() const;
        explicit const_iterator(Node* ptr);
    private:
        Node* ptr_;
    };

    class iterator : public const_iterator {
    public:
        friend class SingleList<T>;
        iterator();
        iterator(const iterator& rhv);
        reference operator*();
        pointer operator->();
    private:
        iterator(Node* ptr);
    };

public:
    friend const_iterator;
    friend iterator;
    SingleList();
    explicit SingleList(const size_type n);
    SingleList(const size_type n, const T& val);
    SingleList(const SingleList& rhv);
    template <typename InputIterator>
    SingleList(InputIterator f, InputIterator l);   
    ~SingleList();
    const SingleList& operator=(const SingleList& rhv);
    void swap(SingleList& rhv);
    size_type size() const;
    size_type max_size() const;
    bool empty() const;
    void clear();
    void resize(const size_type n, const T& val = T());
    reference front();
    const_reference front() const;
    void pop_front();
    void push_front (const value_type& val);
    iterator begin();
    iterator end();
    const_iterator begin() const;
    const_iterator end() const;
    iterator insert(iterator pos, const T& x);
    void insert(iterator pos, const size_t n, const T& x);
    template <typename InputIt>
    void insert(iterator pos, InputIt f, InputIt l);
    iterator insert_after(iterator pos, const T& x);
    void insert_after(iterator pos, size_t n, const T& x);
    template <typename InputIt>
    void insert_after(iterator pos, InputIt f, InputIt l);
    iterator erase(iterator pos);
    iterator erase(iterator f, iterator l);
    iterator erase_after(iterator pos);
private:
    Node* begin_;
    Node* end_;
    
};
template <typename TT>
bool operator==(const SingleList<TT>& lhv, const SingleList<TT>& rhv);

#include "sources/SingleList.cpp"

#endif ///_SINGLE_LIST_HPP__


