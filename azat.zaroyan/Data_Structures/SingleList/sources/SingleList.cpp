#include "headers/SingleList.hpp"
#include <iostream>
#include <cassert>

template <typename TT>
bool
operator==(const SingleList<TT>& lhv, const SingleList<TT>& rhv)
{
    typename SingleList<TT>::Node* headLhv = lhv.begin_;
    typename SingleList<TT>::Node* headRhv = rhv.begin_;
    while (headLhv != NULL || headRhv != NULL) {
        if (headLhv->data_ != headRhv->data_) {
            return false;
        }
        headLhv = headLhv->next_;
        headRhv = headRhv->next_;
    }
    return NULL == headLhv && NULL == headRhv;
}

template <typename TT>
bool
operator!=(const SingleList<TT>& lhv, const SingleList<TT>& rhv)
{
    return !(lhv == rhv);
}

template <typename TT>
bool
operator<(const SingleList<TT>& lhv, const SingleList<TT>& rhv)
{
    bool areEqual = true;
    typename SingleList<TT>::Node* headLhv = lhv.begin_;
    typename SingleList<TT>::Node* headRhv = rhv.begin_;
    while (headLhv != NULL || headRhv != NULL) {
        if (headLhv->data_ > headRhv->data_) {
            areEqual = false;
        }
        headLhv = headLhv->next_;
        headRhv = headRhv->next_;
    }
    if (NULL == headLhv && NULL == headRhv) {
        return areEqual;
    }
    return headLhv != NULL;
}

template <typename TT>
bool
operator>(const SingleList<TT>& lhv, const SingleList<TT>& rhv)
{
    return rhv < lhv;
}

template <typename TT>
bool
operator<=(const SingleList<TT>& lhv, const SingleList<TT>& rhv)
{
    return !(lhv > rhv);
}

template <typename TT>
bool
operator>=(const SingleList<TT>& lhv, const SingleList<TT>& rhv)
{
    return !(rhv > lhv);
}

template <typename T>
SingleList<T>::Node::Node(const T& data, Node* next)
    : data_(data)
    , next_(next)
{
}

template <typename T>
SingleList<T>::SingleList()
    : begin_(NULL)
    , end_(NULL)
{
}

template <typename T>
SingleList<T>::SingleList(const size_type n)
    : begin_(NULL)
    , end_(NULL)
{
    resize(n);
}

template <typename T>
SingleList<T>::SingleList(const size_type n, const T& val)
    : begin_(NULL)
    , end_(NULL)
{
    resize(n, val);
}
template <typename T>

template <typename InputIterator>
SingleList<T>::SingleList(InputIterator f, InputIterator l)
    : begin_(NULL)
    , end_(NULL)
{
    size_type size = 0;
    for (InputIterator it = f; it != l; ++it) {
        ++size;
    }
    resize(size);
    iterator nextIt = begin();
    for (InputIterator it = f; it != l; ++it) {
        *nextIt = *it;
        ++nextIt;
    }
}

template <typename T>
SingleList<T>::SingleList(const SingleList& rhv)
    : begin_(NULL)
    , end_(NULL)
{
    resize(rhv.size());
    end_ = begin_;
    Node* head = rhv.begin_;
    while (head != rhv.end_) {
        end_->data_ = head->data_;
        end_ = end_->next_;
        head = head->next_;
    }
}

template <typename T>
SingleList<T>::~SingleList()
{
    while (begin_ != NULL) {
        Node* temp = begin_;
        begin_ = begin_->next_;
        delete temp;
    }
}

template <typename T>
const SingleList<T>&
SingleList<T>::operator=(const SingleList& rhv) {
    resize(rhv.size());
    end_ = begin_;
    Node* head = rhv.begin_;
    while (head != NULL) {
        end_->data_ = head->data_;
        end_ = end_->next_;
        head = head->next_;
    }
}

template <typename T>
typename SingleList<T>::size_type
SingleList<T>::size() const
{
    size_type s = 0;
    Node* head = begin_;
    while (head != NULL) {
        ++s;
        head = head->next_;
    }
    return s;
}

template <typename T>
typename SingleList<T>::size_type
SingleList<T>::max_size() const
{
    return 2147000000 / sizeof(Node);
}

template <typename T>
void
SingleList<T>::swap(SingleList& rhv)
{
    Node* tempBegin = begin_;
    Node* tempEnd = end_;
    begin_ = rhv.begin_;
    end_ = rhv.end_;
    rhv.begin_ = tempBegin;
    rhv.end_ = tempEnd;
}

template <typename T>
bool
SingleList<T>::empty() const
{
    return begin_ == NULL;
}

template <typename T>
void
SingleList<T>::clear()
{
    resize(0);
}

template <typename T>
void
SingleList<T>::resize(const size_type n, const T& val)
{
    size_type s = size();
    if (n > s) {
        if (NULL == begin_) {
            begin_ = end_ = new Node(val, NULL);
            ++s;
        }
        for (size_type i = s; i < n; ++i) {
            end_->next_ = new Node(val, NULL);
            end_ = end_->next_;
        }
        return;
    }
    iterator it = begin();
    for (size_type i = 0; i < n; ++i) {
        ++it;
    }
    end_ = it.ptr_;
    ++it;
    while (it != end()) {
        Node* temp = it.ptr_;
        ++it;
        delete temp;
    }
    end_->next_ = NULL;
}

template <typename T>
typename SingleList<T>::const_reference
SingleList<T>::front() const
{
    return begin_->data_;
}

template <typename T>
typename SingleList<T>::reference
SingleList<T>::front()
{
    return begin_->data_;
}

template <typename T>
void
SingleList<T>::pop_front()
{
    Node* temp = begin_;
    begin_ = begin_->next_;
    delete temp;
}

template <typename T>
void
SingleList<T>::push_front(const value_type& val)
{
    begin_ = new Node(val, begin_);
}

template <typename T>
SingleList<T>::const_iterator::const_iterator()
    : ptr_(NULL)
{
}

template <typename T>
SingleList<T>::const_iterator::const_iterator(const const_iterator& rhv)
    : ptr_(rhv.ptr_)
{
}

template <typename T>
SingleList<T>::const_iterator::~const_iterator()
{
    ptr_ = NULL;
}

template <typename T>
SingleList<T>::const_iterator::const_iterator(Node* ptr)
    : ptr_(ptr)
{
}

template <typename T>
inline typename SingleList<T>::Node*
SingleList<T>::const_iterator::getPtr() const
{
    return ptr_;
}

template <typename T>
const typename SingleList<T>::const_iterator&
SingleList<T>::const_iterator::operator=(const SingleList<T>::const_iterator& rhv)
{
    ptr_ = rhv.ptr_;
    return *this;
}

template <typename T>
typename SingleList<T>::const_reference
SingleList<T>::const_iterator::operator*() const
{
    return getPtr()->data_;
}

template <typename T>
typename SingleList<T>::const_pointer
SingleList<T>::const_iterator::operator->() const
{
    return &getPtr()->data_;
}

template <typename T>
const typename SingleList<T>::const_iterator&
SingleList<T>::const_iterator::operator++()
{
    ptr_ = ptr_->next_;
    return *this;
}

template <typename T>
const typename SingleList<T>::const_iterator
SingleList<T>::const_iterator::operator++(int)
{
    const_iterator temp = *this;
    ptr_ = ptr_->next_;
    return temp;
}

template <typename T>
bool
SingleList<T>::const_iterator::operator==(const const_iterator& rhv) const
{
    return ptr_ == rhv.ptr_;
}

template <typename T>
bool
SingleList<T>::const_iterator::operator!=(const const_iterator& rhv) const
{
    return ptr_ == rhv.ptr_;
}

template <typename T>
SingleList<T>::iterator::iterator()
    : const_iterator()
{
}

template <typename T>
SingleList<T>::iterator::iterator(Node* ptr)
    : const_iterator(ptr)
{
}

template <typename T>
SingleList<T>::iterator::iterator(const iterator& rhv)
    : const_iterator(rhv)
{
}

template <typename T>
typename SingleList<T>::reference
SingleList<T>::iterator::operator*()
{
    return const_iterator::getPtr()->data_;
}

template <typename T>
typename SingleList<T>::pointer
SingleList<T>::iterator::operator->()
{
    return &const_iterator::getPtr()->data_;
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::begin()
{
    return iterator(begin_);
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::end()
{
    return iterator(NULL);
}

template <typename T>
typename SingleList<T>::const_iterator
SingleList<T>::begin() const
{
    return const_iterator(begin_);
}

template <typename T>
typename SingleList<T>::const_iterator
SingleList<T>::end() const
{
    return const_iterator(NULL);
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::insert(iterator pos, const T& x)
{
    pos.ptr_->next_ = new Node(pos.ptr_->data_, pos.ptr_->next_);
    pos.ptr_->data_ = x;
    return pos;
}

template <typename T>
void
SingleList<T>::insert(iterator pos, const size_type n, const T& x)
{
    for (size_type i = 0; i < n; ++i) { 
        pos = insert(pos, n);
    }
}

template <typename T>
template <typename InputIt>
void
SingleList<T>::insert(iterator pos, InputIt f, InputIt l)
{
    for (InputIt it = f; it != l; ++it) {
        pos = insert(pos, *it);
    }
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::insert_after(iterator pos, const T& x)
{
    assert(pos.ptr_ != NULL);
    pos.ptr_->next_ = new Node(x, pos.ptr_->next_);
    return pos;
}

template <typename T>
void
SingleList<T>::insert_after(iterator pos, const size_type n, const T& x)
{
    assert(pos.ptr_ != NULL);
    for (size_type i = 0; i < n; ++i) {
        pos = insert_after(pos,x);
    }
}

template <typename T>
template <typename InputIt>
void
SingleList<T>::insert_after(iterator pos, InputIt f, InputIt l)
{
    assert(pos.ptr_ != NULL);
    for (InputIt it = f; it != l; ++it) {
        pos = insert_after(pos, *it);
    }
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::erase(iterator pos) {
    assert(pos.ptr_ != NULL);
    if (begin() == pos) {
        begin_ = begin_->next_;
        delete pos.ptr_;
        pos = begin();
        return pos;
    }
    if (end_ == pos.ptr_) {
        end_ = begin_;
        while (end_ != pos.ptr_) {
            end_ = end_->next_;
        }
        delete pos.ptr_;
        end_->next_ = NULL;
        pos.ptr_ = end_;
        return pos;
    }
    pos.ptr_->data_ = pos.ptr_->next_->data_;
    Node* temp = pos.ptr_->next_;
    pos.ptr_->next_ = temp.next_;
    delete temp;
    return pos;
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::erase(iterator f, iterator l)
{
    if (begin() == f) {
        while (l != begin()) {
            begin_ = begin_->next_;
            delete f.ptr_;
            f = begin();
        }
        return f;
    }
    while (f != l) {
        assert(f.ptr_ != NULL);
        f.ptr_->data_ = f.ptr_->next_->data_;
        Node * temp = f.ptr_->next_;
        f.ptr_->next_ = temp.next_;
        delete temp;
    }
    return f;
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::erase_after(iterator pos)
{
    assert(pos.ptr_ < end_);
    Node* temp = pos.ptr_->next_;
    pos.ptr_->next_ = temp->next_;
    delete temp;
    return pos;
}

