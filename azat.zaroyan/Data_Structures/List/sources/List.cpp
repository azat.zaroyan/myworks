#include "headers/List.hpp"
#include <iostream>
#include <cassert>

template <typename TT>
bool
operator==(const List<TT>& lhv, const List<TT>& rhv)
{
    bool areEqual = true;
    typename List<TT>::Node* headLhv = lhv.begin_;
    typename List<TT>::Node* headRhv = rhv.begin_;
    while (headLhv != NULL || headRhv != NULL) {
        if (headLhv->data_ != headRhv->data_) {
            return false;
        }
        headLhv = headLhv->next_;
        headRhv = headRhv->next_;
    }
    if (NULL == headLhv && NULL == headRhv) {
        return true;
    }
    return false;
}

template <typename TT>
bool
operator!=(const List<TT>& lhv, const List<TT>& rhv)
{
    return !(lhv == rhv);
}

template <typename TT>
bool
operator<(const List<TT>& lhv, const List<TT>& rhv)
{
    bool areEqual = true;
    typename List<TT>::Node* headLhv = lhv.begin_;
    typename List<TT>::Node* headRhv = rhv.begin_;
    while (headLhv != NULL || headRhv != NULL) {
        if (headLhv->data_ > headRhv->data_) {
            areEqual = false;
        }
        headLhv = headLhv->next_;
        headRhv = headRhv->next_;
    }
    if (NULL == headLhv && NULL == headRhv) {
        return areEqual;
    }
    if (headLhv != NULL) {
        return true;
    }
    return false;
}

template <typename TT>
bool
operator>(const List<TT>& lhv, const List<TT>& rhv)
{
    return rhv < lhv;
}
template <typename TT>
bool
operator<=(const List<TT>& lhv, const List<TT>& rhv)
{
    return !(lhv > rhv);
}
template <typename TT>
bool
operator>=(const List<TT>& lhv, const List<TT>& rhv)
{
    return !(rhv > lhv);
}

template <typename T>
List<T>::Node::Node(const T& data, Node* next, Node* prev)
    : data_(data)
    , next_(next)
    , prev_(prev)
{
}

template <typename T>
List<T>::List()
    : begin_(NULL)
    , end_(NULL)
{
}

template <typename T>
List<T>::List(const size_type n)
    : begin_(NULL)
    , end_(NULL)
{
    resize(n);
}

template <typename T>
List<T>::List(const size_type n, const T& val)
    : begin_(NULL)
    , end_(NULL)
{
    resize(n, val);
}
template <typename T>
template <typename InputIterator>
List<T>::List(InputIterator f, InputIterator l)
    : begin_(NULL)
    , end_(NULL)
{
    size_type size = 0;
    for (InputIterator it = f; it != l; ++it) {
        ++size;
    }
    resize(size);
    iterator nextIt = begin();
    for (InputIterator it = f; it != l; ++it) {
        *nextIt = *it;
        ++nextIt;
    }
}

template <typename T>
List<T>::List(const List& rhv)
    : begin_(NULL)
    , end_(NULL)
{
    resize(rhv.size());
    end_ = begin_;
    Node* head = rhv.begin_;
    while (head != rhv.end_) {
        end_->data_ = head->data_;
        end_ = end_->next_;
        head = head->next_;
    }
}

template <typename T>
List<T>::~List()
{
    while (end_ != NULL) {
        Node* temp = end_;
        end_ = end_->prev_;
        delete temp;
    }
}

template <typename T>
const List<T>&
List<T>::operator=(const List& rhv) {
    resize(rhv.size());
    end_ = begin_;
    Node* head = rhv.begin_;
    while (head != rhv.end_) {
        end_->data_ = head->data_;
        end_ = end_->next_;
        head = head->next_;
    }
}

template <typename T>
typename List<T>::size_type
List<T>::size() const
{
    size_type s = 0;
    Node* head = begin_;
    while (head != NULL) {
        ++s;
        head = head->next_;
    }
    return s;
}

template <typename T>
typename List<T>::size_type
List<T>::max_size() const
{
    return 2147000000 / sizeof(Node);
}

template <typename T>
void
List<T>::swap(List& rhv)
{
    Node* tempBegin = begin_;
    Node* tempEnd = end_;
    begin_ = rhv.begin_;
    end_ = rhv.end_;
    rhv.begin_ = tempBegin;
    rhv.end_ = tempEnd;
}

template <typename T>
bool
List<T>::empty() const
{
    return begin_ == end_;
}

template <typename T>
void
List<T>::clear()
{
    resize(0);
}

template <typename T>
void
List<T>::resize(const size_type n, const T& val)
{
    size_type s = size();
    if (n > s) {
        if (NULL == begin_) {
            begin_ = end_ = new Node(val, NULL, begin_);
            ++s;
        }
        for (size_type i = s; i < n; ++i) {
            end_ = new Node(val, NULL, end_);
            end_->prev_->next_ = end_;
        }
        return;
    }
    for (size_type i = n; i < s; ++i) {
        end_ = end_->prev_;
        delete end_->next_;
        end_->next_ = NULL;
    }
}

template <typename T>
typename List<T>::const_reference
List<T>::back() const
{
    return end_->data_;
}

template <typename T>
typename List<T>::reference
List<T>::back()
{
    return end_->data_;
}

template <typename T>
typename List<T>::const_reference
List<T>::front() const
{
    return begin_->data_;
}

template <typename T>
typename List<T>::reference
List<T>::front()
{
    return begin_->data_;
}

template <typename T>
void
List<T>::pop_back()
{
    end_ = end_->prev_;
    delete end_->next_;
    end_->next_ = NULL;
}

template <typename T>
void
List<T>::pop_front()
{
    begin_ = begin_->next_;
    delete begin_->prev_;
    begin_->prev_ = NULL;
}

template <typename T>
void
List<T>::push_front(const value_type& val)
{
    begin_->prev_ = new Node(val, begin_, NULL);
    begin_ = begin_->prev_;
}

template <typename T>
void
List<T>::push_back(const value_type& val)
{
    resize(size() + 1, val);
}


template <typename T>
List<T>::const_iterator::const_iterator()
    : ptr_(NULL)
{
}

template <typename T>
List<T>::const_iterator::const_iterator(const const_iterator& rhv)
    : ptr_(rhv.ptr_)
{
}

template <typename T>
List<T>::const_iterator::~const_iterator()
{
    ptr_ = NULL;
}

template <typename T>
List<T>::const_iterator::const_iterator(Node* ptr)
    : ptr_(ptr)
{
}

template <typename T>
inline typename List<T>::Node*
List<T>::const_iterator::getPtr() const
{
    return ptr_;
}

template <typename T>
const typename List<T>::const_iterator&
List<T>::const_iterator::operator=(const List<T>::const_iterator& rhv)
{
    ptr_ = rhv.ptr_;
    return *this;
}

template <typename T>
typename List<T>::const_reference
List<T>::const_iterator::operator*() const
{
    return getPtr()->data_;
}

template <typename T>
typename List<T>::const_pointer
List<T>::const_iterator::operator->() const
{
    return &getPtr()->data_;
}

template <typename T>
const typename List<T>::const_iterator&
List<T>::const_iterator::operator++()
{
    ptr_ = ptr_->next_;
    return *this;
}

template <typename T>
const typename List<T>::const_iterator
List<T>::const_iterator::operator++(int)
{
    const_iterator temp = *this;
    ptr_ = ptr_->next_;
    return temp;
}

template <typename T>
const typename List<T>::const_iterator&
List<T>::const_iterator::operator--()
{
    ptr_ = ptr_->prev_;
    return *this;
}

template <typename T>
const typename List<T>::const_iterator
List<T>::const_iterator::operator--(int)
{
    const_iterator temp = *this;
    ptr_ = ptr_->prev_;
    return temp;
}

template <typename T>
List<T>::iterator::iterator()
    : const_iterator()
{
}

template <typename T>
List<T>::iterator::iterator(Node* ptr)
    : const_iterator(ptr)
{
}

template <typename T>
List<T>::iterator::iterator(const iterator& rhv)
    : const_iterator(rhv)
{
}

template <typename T>
typename List<T>::reference
List<T>::iterator::operator*()
{
    return const_iterator::getPtr()->data_;
}

template <typename T>
typename List<T>::pointer
List<T>::iterator::operator->()
{
    return &const_iterator::getPtr()->data_;
}

template <typename T>
List<T>::const_reverse_iterator::const_reverse_iterator()
    : ptr_(NULL)
{
}

template <typename T>
List<T>::const_reverse_iterator::const_reverse_iterator(Node* ptr)
    : ptr_(ptr)
{
}

template <typename T>
List<T>::const_reverse_iterator::const_reverse_iterator(const const_reverse_iterator& rhv)
    : ptr_(rhv.ptr_)
{
}

template <typename T>
List<T>::const_reverse_iterator::~const_reverse_iterator()
{
    ptr_ = NULL;
}

template <typename T>
typename List<T>::Node*
List<T>::const_reverse_iterator::getPtr() const
{
    return ptr_;
}

template <typename T>
const typename
List<T>::const_reverse_iterator&
List<T>::const_reverse_iterator::operator=(const List<T>::const_reverse_iterator& rhv)

{
    ptr_ = rhv.ptr_;
    return *this;
}

template <typename T>
typename List<T>::const_reference
List<T>::const_reverse_iterator::operator*() const
{
    return getPtr()->data_;
}

template <typename T>
typename List<T>::const_pointer
List<T>::const_reverse_iterator::operator->() const
{
    return &getPtr()->data_;
}

template <typename T>
const typename List<T>::const_reverse_iterator&
List<T>::const_reverse_iterator::operator--()
{
    ptr_ = ptr_->next_;
    return *this;
}

template <typename T>
const typename List<T>::const_reverse_iterator
List<T>::const_reverse_iterator::operator--(int)
{
    const const_reverse_iterator temp = *this;
    ptr_ = ptr_->next_;
    return temp;
}

template <typename T>
const typename List<T>::const_reverse_iterator&
List<T>::const_reverse_iterator::operator++()
{
    ptr_ = ptr_->prev_;
    return *this;
}

template <typename T>
const typename List<T>::const_reverse_iterator
List<T>::const_reverse_iterator::operator++(int)
{
    const const_reverse_iterator temp = *this;
    ptr_ = ptr_->prev_;
    return temp;
}

template <typename T>
List<T>::reverse_iterator::reverse_iterator()
    : const_reverse_iterator()
{
}

template <typename T>
List<T>::reverse_iterator::reverse_iterator(Node* ptr)
    : const_reverse_iterator(ptr)
{
}


template <typename T>
List<T>::reverse_iterator::reverse_iterator(const reverse_iterator& rhv)
    : const_reverse_iterator(rhv)
{
}

template <typename T>
typename List<T>::reference
List<T>::reverse_iterator::operator*()
{
    return const_reverse_iterator::getPtr()->data_;
}

template <typename T>
typename List<T>::pointer
List<T>::reverse_iterator::operator->()
{
    return *(const_reverse_iterator::getPtr()->data_);
}

template <typename T>
typename List<T>::iterator
List<T>::begin()
{
    return iterator(begin_);
}

template <typename T>
typename List<T>::iterator
List<T>::end()
{
    return iterator(NULL);
}

template <typename T>
typename List<T>::const_iterator
List<T>::begin() const
{
    return const_iterator(begin_);
}

template <typename T>
typename List<T>::const_iterator
List<T>::end() const
{
    return const_iterator(NULL);
}

template <typename T>
typename List<T>::reverse_iterator
List<T>::rbegin()
{
    return reverse_iterator(end_);
}

template <typename T>
typename List<T>::reverse_iterator
List<T>::rend()
{
    return reverse_iterator(NULL);
}

template <typename T>
typename List<T>::const_reverse_iterator
List<T>::rbegin() const
{
    return const_reverse_iterator(end_);
}

template <typename T>
typename List<T>::const_reverse_iterator
List<T>::rend() const
{
    return const_reverse_iterator(NULL);
}

template <typename T>
typename List<T>::iterator
List<T>::insert(iterator pos, const T& x)
{
    Node* temp = new Node(x, pos.ptr_, pos.ptr_->prev_);
    if (pos.ptr_ != begin_) {
        pos.ptr_->prev_->next_ = temp;
    }
    if (pos.ptr_ != end_) {
        pos.ptr_->prev_ = temp;
    }
    pos.ptr_ = temp;
    return pos;
}

template <typename T>
void
List<T>::insert(iterator pos, const size_type n, const T& x)
{
    for (size_type i = 0; i < n; ++i) { 
        Node* temp = new Node(x, pos.ptr_, pos.ptr_->prev_);
        if (pos.ptr_ != begin_) {
            pos.ptr_->prev_->next_ = temp;
        }
        if (pos.ptr_ != end_) {
            pos.ptr_->prev_ = temp;
        }
    }
}

template <typename T>
template <typename InputIt>
void
List<T>::insert(iterator pos, InputIt f, InputIt l)
{
    for (InputIt it = f; it != l; ++it) {
        Node* temp = new Node(*it, pos.ptr_, pos.ptr_->prev_);
        if (pos.ptr_ != begin_) {
            pos.ptr_->prev_->next_ = temp;
        }
        if (pos.ptr_ != end_) {
            pos.ptr_->prev_ = temp;
        }
    }
}

template <typename T>
typename List<T>::iterator
List<T>::insert_after(iterator pos, const T& x)
{
    Node* temp = new Node(x, pos.ptr_->next_, pos.ptr_);
    if (pos.ptr_ != end_) {
        pos.ptr_->next_->prev_ = temp;
    }
    if (pos.ptr_ != NULL) {
        pos.ptr_->next = temp;
    }
    pos.ptr_ = temp;
    return pos;
}

template <typename T>
void
List<T>::insert_after(iterator pos, const size_type n, const T& x)
{
    for (size_type i = 0; i < n; ++i) {
        Node* temp = new Node(x, pos.ptr_->next_, pos.ptr_);
        if (pos.ptr_ != end_) {
            pos.ptr_->next_->prev_ = temp;
        }
        if (pos.ptr_ != NULL) {
            pos.ptr_->next = temp;
        }
    }
}

template <typename T>
template <typename InputIt>
void
List<T>::insert_after(iterator pos, InputIt f, InputIt l)
{
    for (InputIt it = f; it != l; ++it) {
        Node* temp = new Node(*it, pos.ptr_->next_, pos.ptr_);
        if (pos.ptr_ != end_) {
            pos.ptr_->next_->prev_ = temp;
        }
        if (pos.ptr_ != NULL) {
            pos.ptr_->next = temp;
        }
    }
}

template <typename T>
typename List<T>::iterator
List<T>::erase(iterator pos) {
    Node* temp = pos.ptr_;
    pos.ptr_ = pos.ptr_->prev_;
    if (pos.ptr_ != NULL) {
        pos.ptr_->next_ = temp->next_;
    } else {
        begin_ = temp->next;
    }
    if (temp != end_) {
        temp->next_->prev_ = pos.ptr_;
    } else {
        end_ = temp.pev_;
    }
    delete temp;
    return pos;
}

template <typename T>
typename List<T>::iterator
List<T>::erase(iterator f, iterator l) {
    for (iterator it = f; it != f; ++it) {
        Node* temp = it.ptr_;
        it.ptr_ = it.ptr_->prev_;
        if (it.ptr_ != NULL) {
            it.ptr_->next_ = temp->next_;
        } else {
            begin_ = temp->next;
        }
        if (temp != end_) {
            temp->next_->prev_ = it.ptr_;
        } else {
            end_ = temp.pev_;
        }
        delete temp;
    }
    return f;
}

template <typename T>
typename List<T>::iterator
List<T>::erase_after(iterator pos) {
    Node* temp = pos.ptr_->next_;
    pos.ptr_->next_ = temp->next_;
    if (temp != end_) {
        temp->next_->prev_ = pos.ptr_;
    } else {
        end_ = pos.ptr_;
    }
    delete temp;
    return pos;
}

template <typename T>
typename List<T>::Node*
List<T>::binarySearch(const value_type& x) const
{
    size_type s = size();
    Node* start = begin_;
    Node* end = end_;
    Node* middle = NULL;
    while (start != end) {
        s = s / 2;
        middle = start;
        for (size_type i = 0; i < s; ++i) {
            middle = middle->next_;
        }
        if (x == middle->data_) {
            return middle;
        }
        if (middle->data_ < x) {
            start = middle->next_;
        } else {
            end = middle;
        }
    }
    return end_;
}

template <typename T>
typename List<T>::iterator
List<T>::binarySearchIt(const value_type& x) const
{
    size_type s = size();
    iterator start(begin_);
    iterator end(end_);
    iterator middle;
    while (start != end) {
        s = s / 2;
        middle = start;
        for (size_type i = 0; i < s; ++i) {
            ++middle;
        }
        if (x == *middle) {
            return middle;
        }
        if (*middle < x) {
            start = middle;
            ++start;
        } else {
            end = middle;
        }
    }
    return iterator(end_);
}

template <typename T>
typename List<T>::Node*
List<T>::linearSearch(const value_type& x) const
{
    for (Node* ptr = begin_; ptr != end_; ptr = ptr->next_) {
        if (x == ptr->data_) {
            return ptr;
        }
    }
    return end_;
}

template <typename T>
typename List<T>::iterator
List<T>::linearSearchIt(const value_type& x) const
{
    for (iterator it = begin(); it != end(); ++it) {
        if (x == *it) {
            return it;
        }
    }
    return end();
}

