#ifndef __LIST_HPP__
#define __LIST_HPP__

template <typename T>
class List
{
    template <typename TT>
    friend bool operator==(const List<TT>& lhv, const List<TT>& rhv);
    template <typename TT>
    friend bool operator!=(const List<TT>& lhv, const List<TT>& rhv);
    template <typename TT>
    friend bool operator<(const List<TT>& lhv, const List<TT>& rhv);
    template <typename TT>
    friend bool operator<=(const List<TT>& lhv, const List<TT>& rhv);
    template <typename TT>
    friend bool operator>(const List<TT>& lhv, const List<TT>& rhv);
    template <typename TT>
    friend bool operator>=(const List<TT>& lhv, const List<TT>& rhv);
private:
    struct Node {
        Node(const T& data = T(), Node* next = NULL, Node* prev = NULL);
        T data_;
        Node* next_;
        Node* prev_;
    };

public:
    typedef T                 value_type;
    typedef value_type&       reference;
    typedef const value_type& const_reference;
    typedef value_type*       pointer;
    typedef std::ptrdiff_t    difference_type;
    typedef std::size_t       size_type;
    typedef const value_type* const_pointer;
public:
    class const_iterator {
    public:
        friend class List<T>;
        const_iterator();
        const_iterator(const const_iterator& rhv);
        ~const_iterator();
        const const_iterator& operator=(const const_iterator& rhv);
        const_reference operator*() const;
        const_pointer operator->() const;
        const const_iterator& operator++();
        const const_iterator& operator--();
        const const_iterator operator++(int);
        const const_iterator operator--(int);
    protected:
        Node* getPtr() const;
        explicit const_iterator(Node* ptr);
    private:
        Node* ptr_;
    };

    class iterator : public const_iterator {
    public:
        friend class List<T>;
        iterator();
        iterator(const iterator& rhv);
        reference operator*();
        pointer operator->();
    private:
        iterator(Node* ptr);
    };

    class const_reverse_iterator {
    public:
        friend class List<T>;
        const_reverse_iterator();
        const_reverse_iterator(const const_reverse_iterator& rhv);
        ~const_reverse_iterator();
        const const_reverse_iterator& operator=(const const_reverse_iterator& rhv);
        const_reference operator*() const;
        const_pointer operator->() const;
        const const_reverse_iterator& operator++();
        const const_reverse_iterator& operator--();
        const const_reverse_iterator operator++(int);
        const const_reverse_iterator operator--(int);
    protected:
        Node* getPtr() const;
        explicit const_reverse_iterator(Node* ptr);
    private:
        Node* ptr_;
    };

    class reverse_iterator : public const_reverse_iterator {
    public:
        friend class List<T>;
        reverse_iterator();
        reverse_iterator(const reverse_iterator& rhv);
        reference operator*();
        pointer operator->();
    private:
        reverse_iterator(Node* ptr);
    };

public:
    friend const_iterator;
    friend iterator;
    friend const_reverse_iterator;
    friend reverse_iterator;
    List();
    explicit List(const size_type n);
    List(const size_type n, const T& val);
    List(const List& rhv);
    template <typename InputIterator>
    List(InputIterator f, InputIterator l);
    ~List();
    const List& operator=(const List& rhv);
    void swap(List& rhv);
    size_type size() const;
    size_type max_size() const;
    bool empty() const;
    void clear();
    void resize(const size_type n, const T& val = T());
    reference back();
    const_reference back() const;
    reference front();
    const_reference front() const;
    void pop_back();
    void pop_front();
    void push_front (const value_type& val);
    void push_back (const value_type& val);
    iterator begin();
    iterator end();
    const_iterator begin() const;
    const_iterator end() const;
    reverse_iterator rbegin();
    reverse_iterator rend();
    const_reverse_iterator rbegin() const;
    const_reverse_iterator rend() const;
    iterator insert(iterator pos, const T& x);
    void insert(iterator pos, const size_t n, const T& x);
    template <typename InputIt>
    void insert(iterator pos, InputIt f, InputIt l);
    iterator insert_after(iterator pos, const T& x);
    void insert_after(iterator pos, size_t n, const T& x);
    template <typename InputIt>
    void insert_after(iterator pos, InputIt f, InputIt l);
    iterator erase(iterator pos);
    iterator erase(iterator f, iterator l);
    iterator erase_after(iterator pos);
    Node* binarySearch(const value_type& x) const;
    iterator binarySearchIt(const value_type& x) const;
    Node* linearSearch(const value_type& x) const;
    iterator linearSearchIt(const value_type& x) const;
private:
    Node* begin_;
    Node* end_;
    
};
template <typename TT>
bool operator==(const List<TT>& lhv, const List<TT>& rhv);

#include "sources/List.cpp"

#endif ///__LIST_HPP__

