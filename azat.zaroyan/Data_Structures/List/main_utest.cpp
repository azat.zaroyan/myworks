#include <iostream>
#include "headers/List.hpp"
#include <gtest/gtest.h>

struct A {
    int data_;
    A() : data_(0) {}
    A(const int data) : data_(data) {}
};


TEST(ListTest, Resize)
{
    typedef List<A> L;
    L l;
    l.resize(4);
    EXPECT_EQ(l.size(), 4);

}

TEST(ListTest, PushBackA)
{
    List<A> l1;
    const A a;
    l1.push_back(a);
    EXPECT_EQ(l1.size(), 1);
}

TEST(ListTest, ConstIterator)
{
    typedef List<A> L;
    typedef List<A>::const_iterator LCI;
    A a1(6);
    A a2(3);
    L l;
    l.push_back(a1);
    l.push_back(a2);
    LCI it1 = l.begin();
    ++it1;
    EXPECT_EQ(it1->data_, 3);
}

TEST(ListTest, RangeConstructor)
{
    typedef List<A> L;
    typedef List<A>::iterator LI;
    A a1(4);
    A a2(3);
    A a3(2);
    A a4(7);
    A* a = new A[3];
    a[0] = a1;
    a[1] = a2;
    a[2] = a3;
    L l(a, a + 3);
    LI it = l.begin();
    EXPECT_EQ(it->data_, 4);
}

TEST(ListTest, InsertFunction)
{
    typedef List<A> L;
    typedef List<A>::iterator LI;
    A a1(4);
    A a2(3);
    A a3(2);
    A a4(7);
    A* a = new A[3];
    a[0] = a1;
    a[1] = a2;
    a[2] = a3;
    L l(a, a + 3);
    LI it = l.begin();
    it = l.insert(it, a4);
    EXPECT_EQ(it->data_, 7);
}

TEST(ListTest, InsertRangeFunction)
{
    typedef List<A> L;
    typedef List<A>::iterator LI;
    A a1(4);
    A a2(3);
    A a3(2);
    A a4(7);
    A* a = new A[3];
    a[0] = a1;
    a[1] = a2;
    a[2] = a3;
    L l(a, a + 3);
    LI it = l.begin();
    ++it;
    l.insert(it, a, a + 2);
    --it;
    EXPECT_EQ(it->data_, 3);
}

TEST(ListTest, LinearSearch)
{
    typedef List<int> L;
    int a[] = { 3, 2, 4, 5, 6, 8 };
    L l(a, a + 6);
    EXPECT_EQ(l.linearSearch(4)->next_->data_, 5);
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

