#include <iostream>
struct A {
    int data_;
    A() : data_(0) {}
    A(const int data) : data_(data) {}
};

#include "headers/Vector.hpp"

#include <gtest/gtest.h>
#include <list>

TEST(VectorTest, CapacityA)
{
    const cd02::Vector<A> v(7);
    EXPECT_EQ(v.capacity(), 7);
}

TEST(VectorTest, PushBackA)
{
    cd02::Vector<A> v1;
    const A a;
    v1.push_back(a);
    EXPECT_EQ(v1.size(), 1);

}

TEST(VectorTest, RservePushBack)
{
    cd02::Vector<A> v1(2);
    const A a;
    v1.push_back(a);
    v1.push_back(a);
    EXPECT_EQ(v1.capacity(), 4);
    v1.push_back(a);
    EXPECT_EQ(v1.capacity(), 8);
}

TEST(VectorTest, ReserveA)
{
    cd02::Vector<A> v;
    v.reserve(3);
    EXPECT_EQ(v.capacity(), 3);
}

TEST(VectorTest, ConstIterator)
{
    typedef cd02::Vector<A> V;
    typedef cd02::Vector<A>::const_iterator VCI;
    A a1(6);
    A a2(3);
    V v;
    v.push_back(a1);
    v.push_back(a2);
    VCI it1 = v.end();
    --it1;
    EXPECT_EQ(it1->data_, 3);
}

TEST(VectorTest, ConstIterato1r)
{
    typedef cd02::Vector<A> V;
    typedef cd02::Vector<A>::const_iterator VCI;
    A a1(4);
    A a2(3);
    A a3(2);
    V v;
    v.push_back(a1);
    v.push_back(a2);
    v.push_back(a3);
    VCI it1 = v.begin();
    it1++;
    EXPECT_EQ(it1->data_, 3);
    VCI it2 = v.end();
    --it2;
    EXPECT_EQ(it2->data_, 2);
}

TEST(VectorTest, ReverseIterator)
{
    typedef cd02::Vector<A> V;
    typedef cd02::Vector<A>::reverse_iterator VRI;
    A a1(2);
    A a2(7);
    V v;
    v.push_back(a1);
    v.push_back(a2);
    VRI it1 = v.rbegin();
    EXPECT_EQ(it1->data_, 7);
    VRI it2 = v.rend();
    --it2;
    EXPECT_EQ(it2->data_, 2);
}

TEST(VectorTest, RangeConstructor)
{
    typedef cd02::Vector<A> V;
    typedef cd02::Vector<A>::iterator VI;
    A a1(4);
    A a2(3);
    A a3(2);
    A a4(7);
    A* a = new A[3];
    a[0] = a1;
    a[1] = a2;
    a[2] = a3;
    V v(a, a + 3);
    VI it = v.begin();
    EXPECT_EQ(it->data_, 4);
    it = v.insert(it, a4);
    EXPECT_EQ(it->data_, 7);
    ++it;
    EXPECT_EQ(it->data_, 4);
}

TEST(VectorTest, ConstructorSize)
{
    typedef cd02::Vector<int> V;
    V v(5, 8);
    std::stringstream stream;
    stream << v;
    EXPECT_STREQ(stream.str().c_str(), "{ 8, 8, 8, 8, 8 }");
}

TEST(VectorTest, RangeConstructorItList)
{
    typedef cd02::Vector<int> V;
    typedef cd02::Vector<int>::iterator VI;
    typedef std::list<int> L;
    typedef std::list<int>::iterator LI;
    int a[] = {1, 2, 3, 4, 5 };
    L l(a, a + 5);
    LI it1 = l.begin();
    LI it2 = l.end();
    V v(it1, it2);
    VI itv = v.begin();
    ++itv;
    EXPECT_EQ(*itv, 2);
}

TEST(VectorTest, ConstReverseIterator)
{
    typedef cd02::Vector<int> V;
    typedef V::const_reverse_iterator VCRI;
    int arr[] = { 1, 2, 3 };
    V v(arr, arr + 3);
    int i = 3;
    for (VCRI it = v.rbegin(); it < v.rend(); ++it)
    {
        EXPECT_EQ(*it, i);
        --i;
    }
}

TEST(VectorTest, BinarySearch)
{
    typedef cd02::Vector<int> V;
    typedef V::iterator VI;
    int a[] = { 1, 2, 3, 4, 7, 9, 11, 24 };
    V v(a, a + 8);
    int* ptr = v.binarySearch(4);
    EXPECT_EQ(*(++ptr), 7);
    VI it = v.binarySearchIt(9);
    ++it;
    EXPECT_EQ(*it, 11);
}

TEST(VectorTest, LinearSearch)
{
    typedef cd02::Vector<int> V;
    typedef V::iterator VI;
    int arr[] = { 1, 2, 3, 4 };
    V v(arr, arr + 4);
    VI it = v.linearSearchIt(2);
    ++it;
    EXPECT_EQ(*it, 3);
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

