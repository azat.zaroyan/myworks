#ifndef __VECTOR_T_HPP__
#define __VECTOR_T_HPP__

const std::size_t RESERVE_COEF = 2;

#include <cstddef>
#include <iostream>

namespace cd02 {

template <typename T>
class Vector
{
public:
    typedef T                 value_type;
    typedef value_type&       reference;
    typedef const value_type& const_reference;
    typedef value_type*       pointer;
    typedef const value_type* const_pointer;
    typedef std::ptrdiff_t    difference_type;
    typedef std::size_t       size_type;

    class const_iterator {
    public:
        friend class Vector<T>;
        const_iterator(); /// trivial iterator (default constructible)
        const_iterator(const const_iterator& rhv); /// trivial iterator (assignable)
        ~const_iterator(); /// destructor
        const const_iterator& operator=(const const_iterator& rhv); /// trivial (assignable)
        const_reference operator*() const; /// trivial iterator (dereferencable)
        const_pointer operator->() const; /// trivial iterator (dereferencable)
        const const_iterator& operator++();
        const const_iterator& operator--();
        const const_iterator operator++(int);
        const const_iterator operator--(int);
        const const_iterator operator+(const size_type index);
        const const_iterator operator-(const size_type index);
        size_type operator-(const const_iterator& rhv);
        const const_iterator& operator+=(const size_type index);
        const const_iterator& operator-=(const size_type index);
        bool operator==(const const_iterator& rhv) const;
        bool operator!=(const const_iterator& rhv) const;
        bool operator<=(const const_iterator& rhv) const;
        bool operator>=(const const_iterator& rhv) const;
        bool operator<(const const_iterator& rhv) const;
        bool operator>(const const_iterator& rhv) const;
        const_reference operator[](const size_type size) const; /// random access
    protected:
        pointer getPtr() const;
        const_iterator(T* ptr);
    private:
        pointer ptr_; /// pointer to the current element
    };

    class iterator : public const_iterator {
    public:
        friend class Vector<T>;
        iterator();
        iterator(const iterator& rhv);
        reference operator*();
        pointer operator->();
        reference operator[](const size_type size);
    private:
        iterator(T* ptr);
    };
    
    class const_reverse_iterator {
    public:
        friend class Vector<T>;
        const_reverse_iterator();
        const_reverse_iterator(const const_reverse_iterator& rhv);
        ~const_reverse_iterator();
        const const_reverse_iterator& operator=(const const_reverse_iterator& rhv);
        const_reference operator*() const;
        const_pointer operator->() const;
        const const_reverse_iterator& operator++();
        const const_reverse_iterator& operator--();
        const const_reverse_iterator operator++(int);
        const const_reverse_iterator operator--(int);
        const const_reverse_iterator operator+(const size_type index);
        const const_reverse_iterator operator-(const size_type index);
        const const_reverse_iterator& operator+=(const size_type index);
        const const_reverse_iterator& operator-=(const size_type index);
        size_type operator-(const const_reverse_iterator& rhv);
        bool operator==(const const_reverse_iterator& rhv) const;
        bool operator!=(const const_reverse_iterator& rhv) const;
        bool operator<=(const const_reverse_iterator& rhv) const;
        bool operator>=(const const_reverse_iterator& rhv) const;
        bool operator<(const const_reverse_iterator& rhv) const;
        bool operator>(const const_reverse_iterator& rhv) const;
        const_reference operator[](const size_type size) const;
    protected:
        pointer getPtr() const;
        const_reverse_iterator(T* ptr);
    private:
        pointer ptr_;
    };

    class reverse_iterator : public const_reverse_iterator {
    public:
        friend class Vector<T>;
        reverse_iterator();
        reverse_iterator(const reverse_iterator& rhv);
        reference operator*();
        pointer operator->();
        reference operator[](const size_type size);
    private:
        reverse_iterator(T* ptr);
    };


public:
    explicit Vector(const size_t size = 0);
    Vector(const Vector<T>& rhv);                  /// Container, Assignable
    Vector(const int size, const T& val);
    const Vector& operator=(const Vector<T>& rhv);/// Container, Assignable
    template <typename InputIterator>
    Vector(InputIterator f, InputIterator l);
    ~Vector();

public:
    const_reference operator[](const size_t index) const; /// Random Access Container, O(1)
    reference operator[](const size_t index);             /// Random Access Container, O(1)
    bool operator==(const Vector<T>& rhv) const; /// Forward Container, Equality Comparable
    bool operator!=(const Vector<T>& rhv) const; /// Forward Container, Equality Comparable
    bool operator<(const Vector<T>& rhv) const;  /// Forward Container, Less Than Comparable
    bool operator>(const Vector<T>& rhv) const;  /// Forward Container, Less Than Comparable
    bool operator<=(const Vector<T>& rhv) const; /// Forward Container, Less Than Comparable
    bool operator>=(const Vector<T>& rhv) const; /// Forward Container, Less Than Comparable
    void resize(const size_type size);
    void resize(const size_type size, const T& val);
    size_type size() const;
    size_type max_size() const;
    size_type capacity() const;
    void reserve(const size_type size);
    const_reference at(const size_t index) const;
    reference at(const size_t index);
    void push_back(const value_type val);
    void pop_back();
    bool empty() const;
    
    const_iterator begin() const;
    iterator begin();
    const_iterator end() const;
    iterator end();
    const_reverse_iterator rbegin() const;
    reverse_iterator rbegin();
    const_reverse_iterator rend() const;
    reverse_iterator rend();
    
    iterator insert(iterator pos, const T& x);                   /// Sequence
    void insert(iterator pos, const size_type n, const T& x);    /// Sequence
    template <typename InputIterator>
    void insert(iterator pos, InputIterator f, InputIterator l); /// Sequence
    iterator erase(iterator pos);                                /// Sequence
    iterator erase(iterator f, iterator l);
    pointer binarySearch(const value_type& x) const;
    iterator binarySearchIt(const value_type& x) const;
    pointer linearSearch(const value_type& x);
    iterator linearSearchIt(const value_type& x);
private:
    T* begin_;
    T* end_;
    T* bufferEnd_;
}; /// end of class Vector

} /// end of namespace cd02


///template <typename T>
///std::ostream& operator<<(std::ostream& out, const cd02::Vector<T>& rhv);
#include "sources/Vector.cpp"

#endif /// __VECTOR_T_HPP__

