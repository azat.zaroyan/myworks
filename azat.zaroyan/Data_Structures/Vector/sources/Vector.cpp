#include "headers/Vector.hpp"

#include <cassert>

template <typename T>
std::ostream&
operator<<(std::ostream& out, const cd02::Vector<T>& rhv)
{
    out << "{ ";
    const size_t beforeEnd = rhv.size() - 1;
    size_t i = 0;
    for (; i < beforeEnd; ++i) {
        out << rhv[i] << ", ";
    }
    out << rhv[i];
    out << " }";
    return out;
}

namespace cd02 {

template <typename T>
Vector<T>::Vector(const size_t size)
    : begin_(NULL)
    , end_(NULL)
    , bufferEnd_(NULL)
{
    resize(size);
}

template <typename T>
Vector<T>::Vector(const Vector<T>& rhv)
    : begin_(NULL)
    , end_(NULL)
    , bufferEnd_(NULL)
{
    resize(rhv.size());
    for (size_t i = 0; i < size(); ++i) {
        at(i) = rhv.at(i);
    }
}

template <typename T>
const Vector<T>&
Vector<T>::operator=(const Vector<T>& rhv)
{
    if (&rhv == this) {
        return *this;
    }

    if (size() != rhv.size()) {
        ::operator delete [](reinterpret_cast<void*>(begin_));
        begin_ =  reinterpret_cast<T*>(::operator new[](rhv.size() * sizeof(T)));
        end_ = begin_ + rhv.size();
        bufferEnd_ = begin_ + rhv.size();
    }

    for (size_t i = 0; i < size(); ++i) {
        at(i) = rhv.at(i);
    }

    return *this;
}

template <typename T>
Vector<T>::Vector(const int size, const T& val)
    : begin_(NULL)
    , end_(NULL)
    , bufferEnd_(NULL)
{
    assert(size >= 0);
    resize(static_cast<size_type>(size), val);
}

template <typename T>
template <typename InputIterator>
Vector<T>::Vector(InputIterator f, InputIterator l)
    : begin_(NULL)
    , end_(NULL)
    , bufferEnd_(NULL)
{
    size_type size = 0;
    for(InputIterator it = f; it != l; ++it) {
        ++size;
    }
    reserve(size);
    for (InputIterator it = f; it != l; ++it) {
        *end_ = *it;
        ++end_;
    }
}

template <typename T>
Vector<T>::~Vector()
{
    if (begin_ != NULL) {
        ::operator delete[](reinterpret_cast<void*>(begin_));
        begin_ = NULL;
    }
}

template <typename T>
void
Vector<T>::resize(const size_type n)
{
    const size_type s = size();
    if (n > capacity()) {
        reserve(n);
        for (size_type i = s; i < n; ++i) {
            begin_[i] = T();
        }
        end_ = begin_ + n;
        return;
    }
    if (n > s) {
        for (size_type i = s; i < n; ++i) {
            begin_[i] = T();
        }
        end_ = begin_ + n;
        return;
    }
    if (n < s) {
        for (size_type i = n + 1; i < s; ++i) {
            begin_[i].~T();
        }
        end_ = begin_ + n;
    }
}

template <typename T>
void
Vector<T>::resize(const size_type n, const T& val)
{
    const size_type s = size();
    if (n > capacity()) {
        reserve(n);   
    }
    for (size_type i = s; i < n; ++i) {
        begin_[i] = val;
    }
    for (size_type i = n; i < s; ++i) {
        begin_[i].~T();
    }
    end_ = begin_ + n;
}

template <typename T>
void
Vector<T>::reserve(const size_type c)
{
    if (c <= capacity()) {
        return;
    }
    
    T* temp = reinterpret_cast<T*>(::operator new[](c * sizeof(T)));
    const size_type s = size();
    for (size_type i = 0; i < s; ++i) {
        temp[i] = begin_[i];
    }

    if (begin_ != NULL) {
        ::operator delete[](reinterpret_cast<void*>(begin_));
    }

    begin_ = temp;
    end_ = begin_ + s;
    bufferEnd_ = begin_ + c;
}

template <typename T>
bool
Vector<T>::empty() const
{
    return end_ == begin_;
}

template <typename T>
typename Vector<T>::size_type
Vector<T>::size() const
{
    return end_ - begin_;
}

template <typename T>
typename Vector<T>::size_type
Vector<T>::max_size() const
{
    return 2147000000 / sizeof(T);
}

template <typename T>
typename Vector<T>::size_type
Vector<T>::capacity() const
{
    return bufferEnd_ - begin_;
}

template <typename T>
void
Vector<T>::push_back(const value_type val)
{
    if (0 == capacity()) {
        reserve(1);
    }
    if (capacity() == size()) {
        reserve(capacity() * RESERVE_COEF);
    }
    *end_ = val;
    ++end_;
}

template <typename T>
void
Vector<T>::pop_back()
{
    if (!empty()) {
        --end_;
        *end_.~T();
    }
}

template <typename T>
bool
Vector<T>::operator==(const Vector<T>& rhv) const
{
    if (size() != rhv.size()) {
        return false;
    }

    for (size_t i = 0; i < size(); ++i) {
        if (at(i) != rhv.at(i)) {
            return false;
        }
    }

    return true;
}

template <typename T>
bool
Vector<T>::operator!=(const Vector<T>& rhv) const
{
    return !(*this == rhv);
}

template <typename T>
typename Vector<T>::const_reference
Vector<T>::operator[](const size_t index) const
{
    assert(begin_ + index < end_);
    return *(begin_ + index);
}

template <typename T>
typename Vector<T>::reference
Vector<T>::operator[](const size_t index)
{
    assert(begin_ + index < end_);
    return *(begin_ + index);
}

template <typename T>
typename Vector<T>::const_reference
Vector<T>::at(const size_t index) const
{
    assert(begin_ + index < end_);
    return *(begin_ + index);
}

template <typename T>
typename Vector<T>::reference
Vector<T>::at(const size_t index)
{
    assert(begin_ + index < end_);
    return *(begin_ + index);
}

template <typename T>
Vector<T>::const_iterator::const_iterator()
    : ptr_(NULL)
{
}

template <typename T>
Vector<T>::const_iterator::const_iterator(const const_iterator& rhv)
    : ptr_(rhv.ptr_)
{
}

template <typename T>
Vector<T>::const_iterator::~const_iterator()
{
    ptr_ = NULL;
}

template <typename T>
Vector<T>::const_iterator::const_iterator(T* ptr)
    : ptr_(ptr)
{
}

template <typename T>
inline typename Vector<T>::pointer
Vector<T>::const_iterator::getPtr() const
{
    return ptr_;
}

template <typename T>
const typename Vector<T>::const_iterator&
Vector<T>::const_iterator::operator=(const Vector<T>::const_iterator& rhv)
{
    ptr_ = rhv.ptr_;
    return *this;
}

template <typename T>
typename Vector<T>::const_reference
Vector<T>::const_iterator::operator*() const
{
    return *getPtr();
}

template <typename T>
typename Vector<T>::const_pointer
Vector<T>::const_iterator::operator->() const
{
    return getPtr();
}

template <typename T>
typename Vector<T>::size_type
Vector<T>::const_iterator::operator-(const const_iterator& rhv)
{
    return ptr_ - rhv.ptr_;
}

template <typename T>
const typename Vector<T>::const_iterator&
Vector<T>::const_iterator::operator++()
{
    ++ptr_;
    return *this;
}

template <typename T>
const typename Vector<T>::const_iterator
Vector<T>::const_iterator::operator++(int)
{
    const_iterator temp = *this;
    ++ptr_;
    return temp;
}

template <typename T>
const typename Vector<T>::const_iterator&
Vector<T>::const_iterator::operator--()
{
    --ptr_;
    return *this;
}

template <typename T>
const typename Vector<T>::const_iterator
Vector<T>::const_iterator::operator--(int)
{
    const_iterator temp = *this;
    --ptr_;
    return temp;
}

template <typename T>
const typename Vector<T>::const_iterator
Vector<T>::const_iterator::operator+(const size_type index)
{
    const const_iterator temp(ptr_ + index);
    return temp;
}

template <typename T>
const typename Vector<T>::const_iterator
Vector<T>::const_iterator::operator-(const size_type index)
{
    const const_iterator temp(ptr_ - index);
    return temp;
}

template <typename T>
const typename Vector<T>::const_iterator&
Vector<T>::const_iterator::operator+=(const size_type index)
{
    ptr_ += index;
    return *this;
}

template <typename T>
const typename Vector<T>::const_iterator&
Vector<T>::const_iterator::operator-=(const size_type index)
{
    *ptr_ -= index;
    return *this;
}

template <typename T>
bool
Vector<T>::const_iterator::operator==(const const_iterator& rhv) const
{
    return ptr_ == rhv.ptr_;
}

template <typename T>
bool
Vector<T>::const_iterator::operator!=(const const_iterator& rhv) const
{
    return !(ptr_ == rhv.ptr_);
}

template <typename T>
bool
Vector<T>::const_iterator::operator>(const const_iterator& rhv) const
{
    return ptr_ - rhv.ptr_ > 0;
}

template <typename T>
bool
Vector<T>::const_iterator::operator<(const const_iterator& rhv) const
{
    return rhv > *this; 
}

template <typename T>
bool
Vector<T>::const_iterator::operator>=(const const_iterator& rhv) const
{
    return !(*this < rhv);
}

template <typename T>
bool
Vector<T>::const_iterator::operator<=(const const_iterator& rhv) const
{
    return !(*this > rhv);
}

template <typename T>
typename Vector<T>::const_reference
Vector<T>::const_iterator::operator[](const size_type index) const
{
    assert(!(begin_ + index > end_));
    return *(begin_ + index);
}

template <typename T>
Vector<T>::iterator::iterator()
    : const_iterator()
{
}

template <typename T>
Vector<T>::iterator::iterator(T* ptr)
    : const_iterator(ptr)
{
}

template <typename T>
Vector<T>::iterator::iterator(const iterator& rhv)
    : const_iterator(rhv)
{
}

template <typename T>
typename Vector<T>::reference
Vector<T>::iterator::operator*()
{
    return *const_iterator::getPtr();
}

template <typename T>
typename Vector<T>::pointer
Vector<T>::iterator::operator->()
{
    return const_iterator::getPtr();
}

template <typename T>
typename Vector<T>::reference
Vector<T>::iterator::operator[](const size_type index)
{
    return *(begin_ + index);
}

template <typename T>
Vector<T>::const_reverse_iterator::const_reverse_iterator()
    : ptr_(NULL)
{
}

template <typename T>
Vector<T>::const_reverse_iterator::const_reverse_iterator(T* ptr)
    : ptr_(ptr)
{
}

template <typename T>
Vector<T>::const_reverse_iterator::const_reverse_iterator(const const_reverse_iterator& rhv)
    : ptr_(rhv.ptr_)
{
}

template <typename T>
Vector<T>::const_reverse_iterator::~const_reverse_iterator()
{
    ptr_ = NULL;
}

template <typename T>
typename Vector<T>::pointer
Vector<T>::const_reverse_iterator::getPtr() const
{
    return ptr_;
}

template <typename T>
const typename
Vector<T>::const_reverse_iterator&
Vector<T>::const_reverse_iterator::operator=(const Vector<T>::const_reverse_iterator& rhv)

{
    ptr_ = rhv.ptr_;
    return *this;
}

template <typename T>
typename Vector<T>::const_reference
Vector<T>::const_reverse_iterator::operator*() const
{
    return *(getPtr());
}

template <typename T>
typename Vector<T>::const_pointer
Vector<T>::const_reverse_iterator::operator->() const
{
    return getPtr();
}

template <typename T>
typename Vector<T>::size_type
Vector<T>::const_reverse_iterator::operator-(const const_reverse_iterator& rhv)
{
    assert(*this < rhv);
    return ptr_ - rhv.ptr_;
}

template <typename T>
const typename Vector<T>::const_reverse_iterator&
Vector<T>::const_reverse_iterator::operator--()
{
    ++ptr_;
    return *this;
}

template <typename T>
const typename Vector<T>::const_reverse_iterator
Vector<T>::const_reverse_iterator::operator--(int)
{
    const const_reverse_iterator temp = *this;
    ++ptr_;
    return temp;
}

template <typename T>
const typename Vector<T>::const_reverse_iterator&
Vector<T>::const_reverse_iterator::operator++()
{
    --ptr_;
    return *this;
}

template <typename T>
const typename Vector<T>::const_reverse_iterator
Vector<T>::const_reverse_iterator::operator++(int)
{
    const const_reverse_iterator temp = *this;
    --ptr_;
    return temp;
}

template <typename T>
const typename Vector<T>::const_reverse_iterator
Vector<T>::const_reverse_iterator::operator-(const size_type index)
{
    const const_reverse_iterator temp(ptr_ + index);
    return temp;
}

template <typename T>
const typename Vector<T>::const_reverse_iterator
Vector<T>::const_reverse_iterator::operator+(const size_type index)
{
    const const_reverse_iterator temp(ptr_ - index);
    return temp;
}

template <typename T>
const typename Vector<T>::const_reverse_iterator&
Vector<T>::const_reverse_iterator::operator-=(const size_type index)
{
    ptr_ += index;
    return *this;
}

template <typename T>
const typename Vector<T>::const_reverse_iterator&
Vector<T>::const_reverse_iterator::operator+=(const size_type index)
{
    ptr_ -= index;
    return *this;
}

template <typename T>
bool
Vector<T>::const_reverse_iterator::operator==(const const_reverse_iterator& rhv) const
{
    return ptr_ == rhv.ptr_;
}

template <typename T>
bool
Vector<T>::const_reverse_iterator::operator!=(const const_reverse_iterator& rhv) const
{
    return !(ptr_ == rhv.ptr_);
}

template <typename T>
bool
Vector<T>::const_reverse_iterator::operator<(const const_reverse_iterator& rhv) const
{
    return ptr_ - rhv.ptr_ > 0;
}

template <typename T>
bool
Vector<T>::const_reverse_iterator::operator>(const const_reverse_iterator& rhv) const
{
    return rhv > *this; 
}

template <typename T>
bool
Vector<T>::const_reverse_iterator::operator<=(const const_reverse_iterator& rhv) const
{
    return !(*this < rhv);
}

template <typename T>
bool
Vector<T>::const_reverse_iterator::operator>=(const const_reverse_iterator& rhv) const
{
    return !(*this > rhv);
}

template <typename T>
typename Vector<T>::const_reference
Vector<T>::const_reverse_iterator::operator[](const size_type index) const
{
    return *(end_ - index);
}

template <typename T>
Vector<T>::reverse_iterator::reverse_iterator()
    : const_reverse_iterator()
{
}

template <typename T>
Vector<T>::reverse_iterator::reverse_iterator(T* ptr)
    : const_reverse_iterator(ptr)
{
}


template <typename T>
Vector<T>::reverse_iterator::reverse_iterator(const reverse_iterator& rhv)
    : const_reverse_iterator(rhv)
{
}

template <typename T>
typename Vector<T>::reference
Vector<T>::reverse_iterator::operator*()
{
    return *(const_reverse_iterator::getPtr());
}

template <typename T>
typename Vector<T>::pointer
Vector<T>::reverse_iterator::operator->()
{
    return const_reverse_iterator::getPtr();
}

template <typename T>
typename Vector<T>::reference
Vector<T>::reverse_iterator::operator[](const size_type index)
{
    return *(begin_ + index);
}

template <typename T>
typename Vector<T>::const_iterator
Vector<T>::begin() const
{
    const const_iterator temp(begin_);
    return temp;
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::begin()
{
    const iterator temp(begin_);
    return temp;
}

template <typename T>
typename Vector<T>::const_iterator
Vector<T>::end() const
{
    const const_iterator temp(end_);
    return temp;
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::end()
{
    const iterator temp(end_);
    return temp;
}

template <typename T>
typename Vector<T>::const_reverse_iterator
Vector<T>::rbegin() const
{
    const const_reverse_iterator temp(end_ - 1);
    return temp;
}

template <typename T>
typename Vector<T>::reverse_iterator
Vector<T>::rbegin()
{
    const reverse_iterator temp(end_ - 1);
    return temp;
}

template <typename T>
typename Vector<T>::const_reverse_iterator
Vector<T>::rend() const
{
    const const_reverse_iterator temp(begin_ - 1);
    return temp;
}

template <typename T>
typename Vector<T>::reverse_iterator
Vector<T>::rend()
{
    const reverse_iterator temp(begin_ - 1);
    return temp;
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::insert(iterator pos, const T& x)
{
    assert(pos.ptr_ >= begin_ && pos.ptr_ <= end_);
    if (capacity() == 0) {
        reserve(1);
    }
    if (capacity() == size()) {
        const size_type posdiff = pos.ptr_ - begin_;
        reserve(capacity() * RESERVE_COEF);
        pos.ptr_ = begin_ + posdiff;
    }
    ++end_;
    for (iterator it = end(); it > pos; --it) {
        *it = *(it - 1);
    }
    *pos = x;
    return pos;
}

template <typename T>
void
Vector<T>::insert(iterator pos, const size_type n, const T& x)
{
    assert(pos.ptr_ >= begin_ && pos.ptr_ <= end_);
    const size_type c = capacity();
    const size_type s = size();
    if (c == 0) {
        reserve(n);
    }
    if (s + n > c) {
        size_type newCapacity = c;
        while (newCapacity < s + n) {
            newCapacity *= RESERVE_COEF;
        }
        const size_type posdiff = pos.ptr_ - begin_;
        reserve(newCapacity);
        pos.ptr_ = begin_ + posdiff;
    }
    end_ += n;
    for (iterator it = end(); it > pos + n - 1; --it) {
        *it = *(it - n);
    }
    for (iterator it = pos; it < it + n; ++it) {
        *it = x;
    }
}

template <typename T>
template <typename InputIterator>
void
Vector<T>::insert(iterator pos, InputIterator f, InputIterator l)
{
    assert(pos.ptr_ >= begin_ && pos.ptr_ <= end_);
    const size_type c = capacity();
    const size_type s = size();
    const size_type n = f - l;
    if (c == 0) {
        reserve(n);
    }
    if (s + n > c) {
        size_type newCapacity = c;
        while (newCapacity < s + n) {
            newCapacity *= RESERVE_COEF;
        }
        const size_type posdiff = pos.ptr_ - begin_;
        reserve(newCapacity);
        pos.ptr_ = begin_ + posdiff;
    }
    iterator e = end();
    end_ += n;
    for (iterator it = end(); it > pos + n - 1; --it) {
        *it = *(it - n);
    }
    for (iterator it = pos, nextIt = f; nextIt != l; ++it, ++nextIt) {
        *it = *nextIt;
    }
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::erase(iterator pos)
{
    assert(pos.ptr_ >= begin_ && pos.ptr_ <= end_);
    pos->~T();
    for (iterator it = pos; it < end(); ++it) {
        *it = *(it + 1);
    }
    --end_;
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::erase(iterator f, iterator l)
{
    assert(l > f && f >= begin() && l <= end());
    for(iterator it = f; it < l; ++it) {
        it->~T();
    }
    const size_type n = l - f;
    for (iterator it = f, nextIt = l; nextIt < end(); ++it, ++nextIt) {
        *it = *nextIt;
    }
    end_ -= n;
}

template <typename T>
typename Vector<T>::pointer
Vector<T>::binarySearch(const value_type& x) const
{
    size_type start = 0;
    size_type end = size();
    while (start != end) {
        size_type middle = (start + end) / 2;
        if (x == begin_[middle]) {
            return begin_ + middle;
        }
        if (x < begin_[middle]) {
            end = middle;
        } else {
            start = middle + 1;
        }
    }
    return end_;
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::binarySearchIt(const value_type& x) const
{
    iterator start(begin_);
    iterator end(end_);
    iterator middle;
    while (start != end) {
        middle.ptr_ = start.ptr_ + (end - start) / 2;
        if (x == *middle) {
            return middle;
        }
        if (x < *middle) {
            end = middle;
        } else {
            start = middle;
            ++start;
        }
    }
    return iterator(end_);
}

template <typename T>
typename Vector<T>::pointer
Vector<T>::linearSearch(const value_type& x)
{
    for (size_type i = 0; i < size(); ++i) {
        if (x == begin_[i]) {
            return begin_ + i;
        }
    }
    return end_;
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::linearSearchIt(const value_type& x)
{
    for (iterator it = begin(); it != end(); ++it) {
        if (x == *it) {
            return it;
        }
    }
    return end();
}

} /// end of namespace cd02

