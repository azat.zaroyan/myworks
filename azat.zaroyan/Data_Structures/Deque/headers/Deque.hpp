#ifndef __DEQUE_HPP__
#define __DEQUE_HPP__
#include <vector>

template <typename T>
class Deque 
{
public:
    typedef T                 value_type;
    typedef std::vector<T>    container_type;
    typedef value_type&       reference;
    typedef const value_type& const_reference;
    typedef value_type*       pointer;
    typedef const value_type* const_pointer;
    typedef std::size_t       size_type;
public:
    class const_iterator {
    public:
        friend class Deque<T>;
        const_iterator();
        const_iterator(const const_iterator& rhv);
        const const_iterator& operator=(const const_iterator& rhv);
        const_reference operator*() const;
        const_pointer operator->() const;
        const const_iterator& operator++();
        const const_iterator& operator--();
        const const_iterator operator++(int);
        const const_iterator operator--(int);
        const const_iterator operator+(const size_type index);
        const const_iterator operator-(const size_type index);
        size_type operator-(const const_iterator& rhv);
        const const_iterator& operator+=(const size_type index);
        const const_iterator& operator-=(const size_type index);
        bool operator==(const const_iterator& rhv) const;
        bool operator!=(const const_iterator& rhv) const;
        bool operator<=(const const_iterator& rhv) const;
        bool operator>=(const const_iterator& rhv) const;
        bool operator<(const const_iterator& rhv) const;
        bool operator>(const const_iterator& rhv) const;
        const_reference operator[](const size_type size) const;
    protected:
        typename container_type::iterator getIt() const;
        const_iterator(typename container_type::iterator it, Deque<T>* deque);
    private:
        typename container_type::iterator it_;
        Deque<T>* deque_;
    };

    class iterator : public const_iterator {
    public:
        friend class Deque<T>;
        iterator();
        iterator(const iterator& rhv);
        reference operator*();
        pointer operator->();
        reference operator[](const size_type size);
    private:
        iterator(typename container_type::iterator it, Deque<T>* deque);
    };

    class const_reverse_iterator {
    public:
        friend class Deque<T>;
        const_reverse_iterator();
        const_reverse_iterator(const const_reverse_iterator& rhv);
        const const_reverse_iterator& operator=(const const_reverse_iterator& rhv);
        const_reference operator*() const;
        const_pointer operator->() const;
        const const_reverse_iterator& operator++();
        const const_reverse_iterator& operator--();
        const const_reverse_iterator operator++(int);
        const const_reverse_iterator operator--(int);
        const const_reverse_iterator operator+(const size_type index);
        const const_reverse_iterator operator-(const size_type index);
        const const_reverse_iterator& operator+=(const size_type index);
        const const_reverse_iterator& operator-=(const size_type index);
        size_type operator-(const const_reverse_iterator& rhv);
        bool operator==(const const_reverse_iterator& rhv) const;
        bool operator!=(const const_reverse_iterator& rhv) const;
        bool operator<=(const const_reverse_iterator& rhv) const;
        bool operator>=(const const_reverse_iterator& rhv) const;
        bool operator<(const const_reverse_iterator& rhv) const;
        bool operator>(const const_reverse_iterator& rhv) const;
        const_reference operator[](const size_type size) const;
    protected:
        typename container_type::iterator getIt() const;
        const_reverse_iterator(typename container_type::iterator it, Deque<T>* deque);
    private:
        typename container_type::iterator it_;
        Deque<T>* deque_;
    };

    class reverse_iterator : public const_reverse_iterator {
    public:
        friend class Deque<T>;
        reverse_iterator();
        reverse_iterator(const reverse_iterator& rhv);
        reference operator*();
        pointer operator->();
        reference operator[](const size_type size);
    private:
            reverse_iterator(typename container_type::iterator it, Deque<T>* deque);
    };

public:
    friend class const_iterator;
    friend class iterator;
    Deque();
    explicit Deque(size_type n);
    Deque(size_type n, const T& val);
    Deque(const Deque& rhv);
    template <class InputIterator>
    Deque(InputIterator f, InputIterator l);
    Deque& operator=(const Deque<T>& rhv);
    void resize(const size_type n, const T&  val = T());
    size_type size() const;
    void clear();
    reference front();
    const_reference front() const;
    reference back();
    const_reference back() const;
    void push_front(const T& val);
    void push_back(const T& val);
    void pop_front();
    void pop_back();
    const_iterator begin() const;
    iterator begin();
    const_iterator end() const;
    iterator end();
    const_reverse_iterator rbegin() const;
    reverse_iterator rbegin();
    const_reverse_iterator rend() const;
    reverse_iterator rend();
    iterator insert(iterator pos, const T& x);
    template <class InputIterator>
    void insert(iterator pos, InputIterator f, InputIterator l);
    void insert(iterator pos, size_type n, const T& x);
    iterator erase(iterator pos);
    iterator erase(iterator first, iterator last);

private:
    container_type bufferFront_;
    container_type bufferBack_;
};

#include "sources/Deque.cpp"
#endif ///__DEQUE_HPP__

