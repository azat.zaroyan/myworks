#ifndef __DEQUE_CPP__
#define __DEQUE_CPP__
#include <iostream>
#include <cassert>
#include "headers/Deque.hpp"

template <typename T>
Deque<T>::Deque()
    : bufferFront_()
    , bufferBack_()
{
}

template <typename T>
Deque<T>::Deque(size_type n)
    : bufferFront_(n / 2)
    , bufferBack_(n - n / 2)
{
}

template <typename T>
Deque<T>::Deque(size_type n, const T& val)
    : bufferFront_(n / 2, val)
    , bufferBack_(n - n / 2, val)
{
}

template <typename T>
Deque<T>::Deque(const Deque& rhv)
    : bufferFront_(rhv.bufferFront_)
    , bufferBack_(rhv.bufferBack_)
{
}

template <typename T>
template <class InputIterator>
Deque<T>::Deque(InputIterator f, InputIterator l)
    : bufferFront_()
    , bufferBack_(f, l)
{
    size_type s = 0;
    for (InputIterator it = f; it != l; ++it) {
        ++s;
    }
    
}

template <typename T>
Deque<T>&
Deque<T>::operator=(const Deque& rhv)
{
    bufferFront_ = rhv.BufferFront_;
    bufferBack_ = rhv.bufferBack_;
    return *this;
}

template <typename T>
void
Deque<T>::resize(const size_type n, const T& val)
{
    const size_type s = size();
    if (n >= s) {
        bufferBack_.resize(n - bufferFront_.size(), val);
        return;
    }
    if (n <= bufferBack_.size()) {
        bufferBack_.resize(n - bufferFront_.size());
        return;
    }
    bufferBack_.resize(0);
    typename container_type::iterator it = bufferFront_.begin();
    bufferFront_.erase(it, it + (bufferFront_.size() - n));
}

template <typename T>
typename Deque<T>::size_type
Deque<T>::size() const
{   
    return bufferFront_.size() + bufferBack_.size();
}

template <typename T>
void
Deque<T>::clear()
{
    bufferFront_.resize(0);
    bufferBack_.resize(0);
}

template <typename T>
typename Deque<T>::reference
Deque<T>::front()
{
    if (bufferFront_.empty()) {
        return bufferBack_[0];
    }
    return bufferFront_.back();
}

template <typename T>
typename Deque<T>::const_reference
Deque<T>::front() const
{
    if (bufferFront_.empty()) {
        return bufferBack_[0];
    }
    return bufferFront_.back();
}

template <typename T>
typename Deque<T>::reference
Deque<T>::back()
{
    if (bufferBack_.empty()) {
        return bufferFront_[0];
    }
    return bufferBack_.back();
}

template <typename T>
typename Deque<T>::const_reference
Deque<T>::back() const
{
    if (bufferBack_.empty()) {
        return bufferFront_[0];
    }
    return bufferBack_.back();
}

template <typename T>
void
Deque<T>::push_front(const T& val)
{
    bufferFront_.push_back(val);
}

template <typename T>
void
Deque<T>::push_back(const T& val)
{
    bufferBack_.push_back(val);
}

template <typename T>
void
Deque<T>::pop_front()
{
    if (bufferFront_.empty()) {
        const size_type s = bufferBack_.size() / 2;
        bufferFront_.insert(bufferFront_.begin(), bufferBack_.rbegin() + s, bufferBack_.rend());
        bufferBack_.erase(bufferBack_.begin(), bufferBack_.end() - s);
    }
    bufferFront_.pop_back();
}

template <typename T>
void
Deque<T>::pop_back()
{
    if (bufferBack_.empty()) {
        const size_type s = bufferFront_.size() / 2;
        bufferBack_.insert(bufferBack_.begin(), bufferFront_.rbegin() + s, bufferFront_.rend());
        bufferFront_.erase(bufferFront_.begin(), bufferFront_.end() - s);
    }
    bufferBack_.pop_back();
}

template <typename T>
Deque<T>::const_iterator::const_iterator()
    : it_()
    , deque_(NULL)
{
}

template <typename T>
Deque<T>::const_iterator::const_iterator(const const_iterator& rhv)
    : it_(rhv.it_)
    , deque_(rhv.deque_)
{
}

template <typename T>
Deque<T>::const_iterator::const_iterator(typename container_type::iterator it, Deque<T>* deque)
    : it_(it)
    , deque_(deque)
{
}

template <typename T>
const typename Deque<T>::const_iterator&
Deque<T>::const_iterator::operator=(const Deque<T>::const_iterator& rhv)
{
    it_ = rhv.it_;
    deque_ = rhv.deque_;
    return *this;
}

template <typename T>
typename Deque<T>::container_type::iterator
Deque<T>::const_iterator::getIt() const
{
    return it_;
}

template <typename T>
typename Deque<T>::const_reference
Deque<T>::const_iterator::operator*() const
{
    return *it_;
}

template <typename T>
typename Deque<T>::const_pointer
Deque<T>::const_iterator::operator->() const
{
    return &*it_;
}

template <typename T>
typename Deque<T>::size_type
Deque<T>::const_iterator::operator-(const const_iterator& rhv)
{
    if (it_ > deque_->bufferBack_.begin() && it_ < deque_->bufferBack_.end() &&
        rhv.it_>= deque_->bufferFront_.begin() && rhv.it_ < deque_->bufferFront_.end()) {
        return it_ - deque_->bufferBack_.begin() + rhv.it_ - rhv.deque_->bufferFront_.begin();
    }
    return it_ - rhv.it_;
}

template <typename T>
const typename Deque<T>::const_iterator&
Deque<T>::const_iterator::operator++()
{
    if (it_>= deque_->bufferFront_.begin() && it_ < deque_->bufferFront_.end()) {
        --it_;
        return *this;
    }
    if (it_ >= deque_->bufferBack_.begin() && it_ < deque_->bufferBack_.end()) {
        ++it_;
        return *this;
    }
    if (it_ == deque_->bufferFront_.begin()) {
        it_ = deque_->bufferBack_.begin();
    }
    return *this;
}

template <typename T>
const typename Deque<T>::const_iterator
Deque<T>::const_iterator::operator++(int)
{
    const_iterator temp = *this;
    if (it_ >= deque_->bufferFront_.begin() && it_ < deque_->bufferFront_.end()) {
        --it_;
        return temp;
    }
    if (it_ >= deque_->bufferBack_.begin() && it_ < deque_->bufferBack_.end()) {
        ++it_;
        return temp;
    }
    if (it_ == deque_->bufferFront_.begin()) {
        it_ = deque_->bufferBack_.begin();
    }
    return temp;
}

template <typename T>
const typename Deque<T>::const_iterator&
Deque<T>::const_iterator::operator--()
{
    if (it_ > deque_->bufferFront_.begin() && it_ < deque_->bufferFront_.end()) {
        ++it_;
        return *this;
    }
    if (it_ > deque_->bufferBack_.begin() && it_ <= deque_->bufferBack_.end()) {
        --it_;
        return *this;
    } 
    if (it_ == deque_->bufferBack_.begin()) {
        it_ = deque_->bufferFront_.begin();
    }
    return *this;
}

template <typename T>
const typename Deque<T>::const_iterator
Deque<T>::const_iterator::operator+(const size_type index)
{
    if (it_ > deque_->bufferFront_.begin() && it_ < deque_->bufferFront_.end() && it_ - deque_->bufferFront_.begin() < index) {
            const const_iterator temp(it_);
            temp.it_ = deque_->bufferBack_.begin() + (index - (it_ - deque_->bufferFront_.begin()));
            return temp;
        }
    const const_iterator temp(it_ + index);
    return temp;
}

template <typename T>
const typename Deque<T>::const_iterator
Deque<T>::const_iterator::operator-(const size_type index)
{
    if (it_ > deque_->bufferBack_.begin() && it_ < deque_->bufferBack_.end() && it_ - deque_->bufferBack_.begin() < index) {
            const const_iterator temp(it_);
            temp.it_ = deque_->bufferFront_.begin() + (index - (it_ - deque_->bufferBack_.begin()));
            return temp;
        }
    const const_iterator temp(it_ - index);
    return temp;

}

template <typename T>
const typename Deque<T>::const_iterator&
Deque<T>::const_iterator::operator+=(const size_type index)
{
    return const_iterator(it_ += index);
}

template <typename T>
const typename Deque<T>::const_iterator&
Deque<T>::const_iterator::operator-=(const size_type index)
{
    return const_iterator(it_ += index);
}

template <typename T>
const typename Deque<T>::const_iterator
Deque<T>::const_iterator::operator--(int)
{
    const_iterator temp = *this;
    if (it_ > deque_->bufferFront_.begin() && it_ < deque_->bufferFront_.end()) {
        ++it_;
        return temp;
    }
    if (it_ > deque_->bufferBack_.begin() && it_ < deque_->bufferBack_.end()) {
        --it_;
        return temp;
    }
    if (it_ == deque_->bufferBack_.begin()) {
        it_ = deque_->bufferFront_.begin();
    }
    return temp;
}

template <typename T>
bool
Deque<T>::const_iterator::operator==(const const_iterator& rhv) const
{
    return it_ == rhv.it_;
}

template <typename T>
bool
Deque<T>::const_iterator::operator!=(const const_iterator& rhv) const
{
    return !(it_ == rhv.it_);
}

template <typename T>
bool
Deque<T>::const_iterator::operator<(const const_iterator& rhv) const
{
    if (it_ >= deque_->bufferFront_.begin() && it_ <= deque_->bufferFront_.end() && rhv.it_ >= deque_->bufferBack_.begin() && rhv.it_ <= deque_->bufferBack_.end()) {
        return true;
    }
    if (rhv.it_ >= deque_->bufferFront_.begin() && rhv.it_ <= deque_->bufferFront_.end() && it_ >= deque_->bufferBack_.begin() && it_ <= deque_->bufferBack_.end()) {
        return false;
    }
    return it_ < rhv.it_;
}

template <typename T>
bool
Deque<T>::const_iterator::operator>(const const_iterator& rhv) const
{
    return rhv < *this; 
}

template <typename T>
bool
Deque<T>::const_iterator::operator>=(const const_iterator& rhv) const
{
    return !(*this < rhv);
}

template <typename T>
bool
Deque<T>::const_iterator::operator<=(const const_iterator& rhv) const
{
    return !(*this > rhv);
}

template <typename T>
typename Deque<T>::const_reference
Deque<T>::const_iterator::operator[](const size_type index) const
{
    return *(this + index);
}


template <typename T>
Deque<T>::iterator::iterator()
    : const_iterator()
{
}

template <typename T>
Deque<T>::iterator::iterator(typename container_type::iterator it, Deque<T>* deque)
    : const_iterator(it, deque)
{
}

template <typename T>
Deque<T>::iterator::iterator(const iterator& rhv)
    : const_iterator(rhv)
{
}

template <typename T>
typename Deque<T>::reference
Deque<T>::iterator::operator*()
{
    return *const_iterator::getIt();
}

template <typename T>
typename Deque<T>::pointer
Deque<T>::iterator::operator->()
{
    return &*const_iterator::getIt();
}

template <typename T>
typename Deque<T>::reference
Deque<T>::iterator::operator[](const size_type index)
{
    return *(this + index);
}

template <typename T>
Deque<T>::const_reverse_iterator::const_reverse_iterator()
    : it_()
    , deque_(NULL)
{
}

template <typename T>
Deque<T>::const_reverse_iterator::const_reverse_iterator(typename container_type::iterator it, Deque<T>* deque)
    : it_(it)
    , deque_(deque)
{
}

template <typename T>
Deque<T>::const_reverse_iterator::const_reverse_iterator(const const_reverse_iterator& rhv)
    : it_(rhv.it_)
    , deque_(rhv.deque_)
{
}

template <typename T>
typename Deque<T>::container_type::iterator
Deque<T>::const_reverse_iterator::getIt() const
{
    return it_;
}

template <typename T>
const typename
Deque<T>::const_reverse_iterator&
Deque<T>::const_reverse_iterator::operator=(const Deque<T>::const_reverse_iterator& rhv)

{
    it_ = rhv.it_;
    return *this;
}

template <typename T>
typename Deque<T>::const_reference
Deque<T>::const_reverse_iterator::operator*() const
{
    return *it_;
}

template <typename T>
typename Deque<T>::const_pointer
Deque<T>::const_reverse_iterator::operator->() const
{
    return &*it_;
}

template <typename T>
typename Deque<T>::size_type
Deque<T>::const_reverse_iterator::operator-(const const_reverse_iterator& rhv)
{
    if (rhv.it_ > deque_->bufferBack_.begin() && rhv.it_ < deque_->bufferBack_.end() &&
        it_>= deque_->bufferFront_.begin() && it_ < deque_->bufferFront_.end()) {
        return it_ - deque_->bufferFront_.begin() + rhv.it_ - rhv.deque_->bufferBack_.begin();
    }
    return it_ - rhv.it_;
}

template <typename T>
const typename Deque<T>::const_reverse_iterator&
Deque<T>::const_reverse_iterator::operator--()
{
    if (it_ > deque_->bufferFront_.begin() && it_ <= deque_->bufferFront_.end()) {
        --it_;
        return *this;
    }
    if (it_ > deque_->bufferBack_.begin() && it_ < deque_->bufferBack_.end()) {
        ++it_;
        return *this;
    }
    if (it_ == deque_->bufferBack_.begin()) {
        it_ = deque_->bufferFront_.begin();
    }
    return *this;
}

template <typename T>
const typename Deque<T>::const_reverse_iterator
Deque<T>::const_reverse_iterator::operator--(int)
{
     const_iterator temp = *this;
    if (it_ > deque_->bufferFront_.begin() && it_ <= deque_->bufferFront_.end()) {
        ++it_;
        return temp;
    }
    if (it_ > deque_->bufferBack_.begin() && it_ < deque_->bufferBack_.end()) {
        --it_;
        return temp;
    }
    if (it_ == deque_->bufferFront_.begin()) {
        it_ = deque_->bufferBack_.begin();
    }
    return temp;
   
}

template <typename T>
const typename Deque<T>::const_reverse_iterator&
Deque<T>::const_reverse_iterator::operator++()
{
    if (it_ > deque_->bufferFront_.begin() && it_ < deque_->bufferFront_.end()) {
        --it_;
        return *this;
    }
    if (it_ > deque_->bufferBack_.begin() && it_ < deque_->bufferBack_.end()) {
        ++it_;
        return *this;
    }
    if (it_ == deque_->bufferBack_.begin()) {
        it_ = deque_->bufferFront_.begin();
    }
    return *this;
}

template <typename T>
const typename Deque<T>::const_reverse_iterator
Deque<T>::const_reverse_iterator::operator++(int)
{
    const_reverse_iterator temp = *this;
    if (it_ > deque_->bufferFront_.begin() && it_ < deque_->bufferFront_.end()) {
        --it_;
        return temp;
    }
    if (it_ > deque_->bufferBack_.begin() && it_ < deque_->bufferBack_.end()) {
        ++it_;
        return temp;
    }
    if (it_ == deque_->bufferBack_.begin()) {
        it_ = deque_->bufferFront_.begin();
    }
    return temp;
}

template <typename T>
const typename Deque<T>::const_reverse_iterator
Deque<T>::const_reverse_iterator::operator-(const size_type index)
{
  if (it_ > deque_->bufferFront_.begin() && it_ < deque_->bufferFront_.end() && it_ - deque_->bufferFront_.begin() < index) {
            const const_reverse_iterator temp(it_);
            temp.it_ = deque_->bufferBack_.begin() - (index - (it_ - deque_->bufferFront_.begin()));
            return temp;
        }
    const const_reverse_iterator temp(it_ - index);
    return temp; 
}

template <typename T>
const typename Deque<T>::const_reverse_iterator
Deque<T>::const_reverse_iterator::operator+(const size_type index)
{
    if (it_ > deque_->bufferBack_.begin() && it_ < deque_->bufferBack_.end() && it_ - deque_->bufferBack_.begin() < index) {
            const const_reverse_iterator temp(it_);
            temp.it_ = deque_->bufferFront_.begin() + (index - (it_ - deque_->bufferBack_.begin()));
            return temp;
        }
    const const_reverse_iterator temp(it_ + index);
    return temp;
}

template <typename T>
const typename Deque<T>::const_reverse_iterator&
Deque<T>::const_reverse_iterator::operator-=(const size_type index)
{
    return const_reverse_iterator(it_ += index);
}

template <typename T>
const typename Deque<T>::const_reverse_iterator&
Deque<T>::const_reverse_iterator::operator+=(const size_type index)
{
    return const_reverse_iterator(it_ -= index);
}

template <typename T>
bool
Deque<T>::const_reverse_iterator::operator==(const const_reverse_iterator& rhv) const
{
    return it_ == rhv.it_;
}

template <typename T>
bool
Deque<T>::const_reverse_iterator::operator!=(const const_reverse_iterator& rhv) const
{
    return !(it_ == rhv.it_);
}

template <typename T>
bool
Deque<T>::const_reverse_iterator::operator<(const const_reverse_iterator& rhv) const
{
    return rhv - *this  > 0;
}

template <typename T>
bool
Deque<T>::const_reverse_iterator::operator>(const const_reverse_iterator& rhv) const
{
    return rhv < *this; 
}

template <typename T>
bool
Deque<T>::const_reverse_iterator::operator<=(const const_reverse_iterator& rhv) const
{
    return !(*this < rhv);
}

template <typename T>
bool
Deque<T>::const_reverse_iterator::operator>=(const const_reverse_iterator& rhv) const
{
    return !(*this > rhv);
}

template <typename T>
typename Deque<T>::const_reference
Deque<T>::const_reverse_iterator::operator[](const size_type index) const
{
    return *(this + index);
}

template <typename T>
Deque<T>::reverse_iterator::reverse_iterator()
    : const_reverse_iterator()
{
}

template <typename T>
Deque<T>::reverse_iterator::reverse_iterator(typename container_type::iterator it, Deque<T>* deque)
    : const_reverse_iterator(it, deque)
{
}


template <typename T>
Deque<T>::reverse_iterator::reverse_iterator(const reverse_iterator& rhv)
    : const_reverse_iterator(rhv)
{
}

template <typename T>
typename Deque<T>::reference
Deque<T>::reverse_iterator::operator*()
{
    return *(const_reverse_iterator::getIt());
}

template <typename T>
typename Deque<T>::pointer
Deque<T>::reverse_iterator::operator->()
{
    return &*const_reverse_iterator::getIt();
}

template <typename T>
typename Deque<T>::reference
Deque<T>::reverse_iterator::operator[](const size_type index)
{
    return *(this + index);
}

template <typename T>
typename Deque<T>::iterator
Deque<T>::insert(iterator pos, const T& x)
{
    assert(pos.it_ >= bufferFront_.begin() && pos.it_ <= bufferFront_.end() || pos.it_ >= bufferBack_.begin() && pos.it_ <= bufferBack_.end());
    if (pos.it_ >= bufferFront_.begin() && pos.it_ <= bufferFront_.end()) {
        bufferFront_.insert(++pos.it_, x);
    }
    if (pos.it_ >= bufferBack_.begin() && pos.it_ <= bufferBack_.end()) {
        bufferBack_.insert(pos.it_, x);
    }
    return pos;
}

template <typename T>
void
Deque<T>::insert(iterator pos, const size_type n, const T& x)
{
    assert(pos.it_ >= bufferFront_.begin() && pos.it_ <= bufferFront_.end() || pos.it_ >= bufferBack_.begin() && pos.it_ <= bufferBack_.end());
    if (pos.it_ >= bufferFront_.begin() && pos.it_ <= bufferFront_.end()) {
        bufferFront_.insert(++pos.it_, n, x);
    }
    if (pos.it_ >= bufferBack_.begin() && pos.it_ <= bufferBack_.end()) {
        bufferBack_.insert(pos.it_, n, x);
    }

}

template <typename T>
template <typename InputIterator>
void
Deque<T>::insert(iterator pos, InputIterator f, InputIterator l)
{
    assert(pos.it_ >= bufferFront_.begin() && pos.it_ <= bufferFront_.end() || pos.it_ >= bufferBack_.begin() && pos.it_ <= bufferBack_.end());
    if (pos.it_ >= bufferFront_.begin() && pos.it_ <= bufferFront_.end()) {
        bufferFront_.insert(pos.it_, ++f, ++l);
    }
    if (pos.it_ >= bufferBack_.begin() && pos.it_ <= bufferBack_.end()) {
        bufferBack_.insert(pos.it_, f, l);
    }

}

template <typename T>
typename Deque<T>::iterator
Deque<T>::erase(iterator pos)
{
    assert(pos.it_ >= bufferFront_.begin() && pos.it_ <= bufferFront_.end() || pos.it_ >= bufferBack_.begin() && pos.it_ <= bufferBack_.end());
    if (pos.it_ >= bufferFront_.begin() && pos.it_ <= bufferFront_.end()) {
        bufferFront_.erase(pos.it_);
    }
    if (pos.it_ >= bufferBack_.begin() && pos.it_ <= bufferBack_.end()) {
        bufferBack_.erase(pos.it_);
    }

}

template <typename T>
typename Deque<T>::iterator
Deque<T>::erase(iterator f, iterator l)
{
    assert(l > f && f.it_ >= bufferFront_.begin() && l.it_ <= bufferFront_.end() || l.it_ >= bufferBack_.begin() && f.it_ <= bufferBack_.end());
    if (f.it_ >= bufferFront_.begin() && l.it_ <= bufferFront_.end()) {
        bufferFront_.erase(f, l);
    }
    if (l.it_ >= bufferBack_.begin() && f.it_ <= bufferBack_.end()) {
        bufferBack_.erase(f, l);
    }
}

template <typename T>
typename Deque<T>::const_iterator
Deque<T>::begin() const
{
    if (bufferFront_.empty()) {
        return const_iterator(bufferBack_.begin(), this);
    }
    return const_iterator(bufferFront_.end() - 1, this);
}

template <typename T>
typename Deque<T>::iterator
Deque<T>::begin()
{
     if (bufferFront_.empty()) {
        return iterator(bufferBack_.begin(), this);
    }
    return iterator(bufferFront_.end() - 1, this);
}

template <typename T>
typename Deque<T>::const_iterator
Deque<T>::end() const
{
    if (bufferBack_.empty()) {
        return const_iterator(bufferFront_.begin() + 1, this);
    }
    return const_iterator(bufferBack_.end(), this);
}

template <typename T>
typename Deque<T>::iterator
Deque<T>::end()
{
    if (bufferBack_.empty()) {
        return iterator(bufferFront_.begin() + 1, this);
    }
    return iterator(bufferBack_.end(), this);
}

template <typename T>
typename Deque<T>::const_reverse_iterator
Deque<T>::rbegin() const
{
    if (bufferBack_.empty()) {
        return const_reverse_iterator(bufferFront_.begin(), this);
    }
    return const_reverse_iterator(bufferBack_.end() - 1, this);
}

template <typename T>
typename Deque<T>::reverse_iterator
Deque<T>::rbegin()
{
    if (bufferBack_.empty()) {
        return reverse_iterator(bufferFront_.begin(), this);
    }
    return reverse_iterator(bufferBack_.end() - 1, this);
}

template <typename T>
typename Deque<T>::const_reverse_iterator
Deque<T>::rend() const
{
    if (bufferFront_.empty()) {
        return const_reverse_iterator(bufferBack_.begin() - 1, this);
    }
    return const_reverse_iterator(bufferFront_.end(), this);
}

template <typename T>
typename Deque<T>::reverse_iterator
Deque<T>::rend()
{
    if (bufferFront_.empty()) {
        return reverse_iterator(bufferBack_.begin() - 1, this);
    }
    return reverse_iterator(bufferFront_.end(), this);
}

#endif ///__DEQUE_CPP__
