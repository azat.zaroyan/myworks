#include <iostream>
struct A {
    int data_;
    A() : data_(0) {}
    A(const int data) : data_(data) {}
};

#include "headers/Deque.hpp"

#include <gtest/gtest.h>
#include <list>

TEST(DequeTest, PushBack)
{
    Deque<A> d1;
    const A a;
    d1.push_back(a);
    EXPECT_EQ(d1.size(), 1);
}

TEST(DequeTest, PushFront)
{
    Deque<A> d1(2);
    const A a;
    d1.push_back(a);
    d1.push_back(a);
    EXPECT_EQ(d1.size(), 4);
    d1.push_front(a);
    EXPECT_EQ(d1.size(), 5);
}

TEST(DequeTest, PopFront)
{
    Deque<A> d1;
    Deque<A>::const_iterator it;
    A a1(1);
    A a2(2);
    A a3(3);
    d1.push_front(a2);
    d1.push_front(a1);
    d1.push_front(a3);
    d1.push_back(a3);
    it = d1.begin();
    EXPECT_EQ(it->data_, 3);
    ++it;
    EXPECT_EQ(it->data_, 1);
    d1.pop_front();
    it = d1.begin();
    EXPECT_EQ(it->data_, 1);
}

TEST(DequeTest, PopBack)
{
    Deque<A> d1;
    Deque<A>::iterator it1;
    A a1(6);
    A a2(3);
    d1.push_back(a1);
    d1.push_back(a2);
    d1.push_front(a2);
    it1 = d1.end();
    --it1;
    EXPECT_EQ(it1->data_, 3);
    d1.pop_back();
    it1 = d1.end();
    --it1;
    EXPECT_EQ(it1->data_, 6);
}

TEST(DequeTest, ConstIterato1r)
{
    Deque<A> d1;
    A a1(4);
    A a2(3);
    A a3(2);
    d1.push_back(a1);
    d1.push_back(a2);
    d1.push_back(a3);
    Deque<A>::const_iterator it1 = d1.begin();
    ++it1;
    EXPECT_EQ(it1->data_, 3);
    Deque<A>::const_iterator it2 = d1.end();
    --it2;
    EXPECT_EQ(it2->data_, 2);
}

TEST(DequeTest, ReverseIterator)
{
    Deque<A> d1;
    A a1(2);
    A a2(7);
    A a3(9);
    d1.push_back(a1);
    d1.push_back(a2);
    d1.push_front(a3);
    Deque<A>::reverse_iterator it1 = d1.rbegin();
    EXPECT_EQ(it1->data_, 7);
    Deque<A>::reverse_iterator it2 = d1.rend();
    --it2;
    EXPECT_EQ(it2->data_, 9);
}

TEST(DequeTest, RangeConstructorItList)
{
    typedef std::list<int> L;
    typedef std::list<int>::iterator LI;
    int a[] = {1, 2, 3, 4, 5 };
    L l(a, a + 5);
    LI it1 = l.begin();
    LI it2 = l.end();
    Deque<int> d1(it1, it2);
    Deque<int>::iterator itd = d1.begin();
    ++itd;
    EXPECT_EQ(*itd, 2);
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

