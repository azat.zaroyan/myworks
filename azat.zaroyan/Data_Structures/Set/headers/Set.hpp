#ifndef __SET_HPP__
#define __SET_HPP__

template <typename T1, typename T2>
struct Pair
{
public:
    Pair();
    Pair(const T1& t1, const T2& t2);
    Pair(const Pair& rhv);
    T1 first_;
    T2 second_;
};

template <typename T1, typename T2>
Pair<T1, T2> make_pair(const T1& t1, const T2& t2);

template <typename Data>
class Set
{
private:
    struct Node {
        Node(const Data& data = Data(), Node* parent = NULL, Node* left = NULL, Node* right = NULL);
        Data data_;
        Node* parent_;
        Node* left_;
        Node* right_;
    };
public:
    template <typename T>
    friend void deleteNode(typename Set<T>::Node* n);
    typedef Data value_type;
    typedef Data key_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef value_type* pointer;
    typedef const value_type* const_pointer;
    typedef std::ptrdiff_t difference_type;
    typedef std::size_t size_type;

    class const_iterator {
        friend class Set<Data>;
    public:
        const_iterator();
        const_iterator(const const_iterator& rhv);
        ~const_iterator();
        const const_iterator& operator=(const const_iterator& rhv);
        const const_iterator& operator++(); 
        const const_iterator& operator++(int); 
        const const_iterator& operator--(); 
        const const_iterator& operator--(int); 
        const_reference operator*() const;
        const_pointer operator->() const;
        bool operator==(const const_iterator& rhv) const;
        bool operator!=(const const_iterator& rhv) const;
        operator bool() const;
    protected:
        bool isLeftParent() const;
        bool isRightParent() const;
        void nextInOrder();
        void prevInOrder();
        Node* getPtr() const;
        explicit const_iterator(Node* ptr);
        int height();
        int balance();
        bool isRoot() const;
        const_iterator parent() const;
        const_iterator left() const;
        const_iterator right() const;
        void goLeft();
        void goRight();
        void goParent();
        void createLeft(const value_type& x);
        void createRight(const value_type& x);
        void setLeft(const const_iterator& it);
        void setRight(const const_iterator& it);
        void setParent(const const_iterator& it);
        const const_iterator& firstLeftParent();
        const const_iterator& firstRightParent();
    private:
        Node* ptr_;
    };

    class iterator : public const_iterator {
        friend class Set<Data>;
    public:
        iterator();
        operator bool() const;
        iterator(const iterator& rhv);
        reference operator*();
        pointer operator->();
    private:
        iterator parent();
        iterator left();
        iterator right();
        iterator& firstLeftParent();
        iterator& firstRightParent();
     private:
        explicit iterator(Node* ptr);
    };
    
    class const_reverse_iterator {
        friend class Set<Data>;
    public:
        const_reverse_iterator();
        const_reverse_iterator(const const_reverse_iterator& rhv);
        ~const_reverse_iterator();
        const const_reverse_iterator& operator=(const const_reverse_iterator& rhv);
        const const_reverse_iterator& operator++(); 
        const const_reverse_iterator& operator++(int); 
        const const_reverse_iterator& operator--(); 
        const const_reverse_iterator& operator--(int); 
        const_reference operator*() const;
        const_pointer operator->() const;
        bool operator==(const const_reverse_iterator& rhv) const;
        bool operator!=(const const_reverse_iterator& rhv) const;
        bool isLeftParent() const;
        bool isRightParent() const;
        void nextInOrder();
        void prevInOrder();
        const_reverse_iterator parent() const;
        const_reverse_iterator left() const;
        const_reverse_iterator right() const;
        void goLeft();
        void goRight();
        void goParent();
    protected:
        Node* getPtr() const;
    private:
    explicit const_reverse_iterator(Node* ptr);
        Node* ptr_;
    };

    class reverse_iterator : public const_reverse_iterator {
        friend class Set<Data>;
    public:
        reverse_iterator();
        reverse_iterator(const reverse_iterator& rhv);
        reference operator*();
        pointer operator->();
    private:
        explicit reverse_iterator(Node* ptr);
    };

public:
    friend class const_iterator;
    friend class iterator;
    friend class const_reverse_iterator;
    friend class reverse_iterator;
    Set();
    Set(const Set& rhv);
    template <typename InputIterator>
    Set(InputIterator f, InputIterator l);
    ~Set();
    const Set& operator=(const Set& rhv);
    void swap(Set& rhv);
    size_type size() const;
    size_type max_size() const;
    bool empty() const;
    void clear();
    iterator begin();
    iterator end();
    const_iterator begin() const;
    const_iterator end() const;
    reverse_iterator rbegin();
    reverse_iterator rend();
    const_reverse_iterator rbegin() const;
    const_reverse_iterator rend() const;
    Pair<iterator, bool> insert(const value_type& x);
    Pair<iterator, bool> insert(iterator it, const value_type& x);
    template <typename InputIt>
    void insert(InputIt f, InputIt l);
    void erase(iterator pos);
    void erase(iterator first, iterator last);
    iterator find(const key_type& k) const;
    size_type count(const key_type& k) const;
    iterator lower_bound(const  key_type& k) const;
    iterator upper_bound(const  key_type& k) const;
    void print();
    bool operator==(const Set& rhv) const;
    bool operator!=(const Set& rhv) const;
    bool operator<(const Set& rhv) const;
    bool operator<=(const Set& rhv) const;
    bool operator>(const Set& rhv) const;
    bool operator>=(const Set& rhv) const;

private:
    void helperPrint(Node* root, int distance);
    Node* leftMost() const;
    Node* rightMost() const;
    iterator eraseUnBalanced(iterator& pos);
    void goUp(iterator& it, const value_type& x);
    bool goDown(iterator& it, const value_type& x);
    void balance(iterator& it);
    void rotateRight(iterator& it);
    void rotateLeft(iterator& it);
    void inOrder(Node* n, void (*visit)(Node*));
    void postOrder(Node* n, void (*visit)(Node*));
    void preOrder(Node* n, void (*visit)(Node*));
    void levelOrder(Node* n, void (*visit)(Node*));
public:
    Node* root_;
};

template <typename T>
void deleteNode(typename Set<T>::Node* n);

#include "sources/Set.cpp"

#endif ///__SET_HPP__
