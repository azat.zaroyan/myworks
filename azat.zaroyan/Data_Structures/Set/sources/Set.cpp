#ifndef __SET_CPP__
#define __SET_CPP__
#include "headers/Set.hpp"
#include <queue>

template <typename T1, typename T2>
Pair<T1, T2>::Pair()
{
}
 
template <typename T1, typename T2>
Pair<T1, T2>::Pair(const Pair& rhv)
    : first_(rhv.first_)
    , second_(rhv.second_)
{
}

template <typename T1, typename T2>
Pair<T1, T2>::Pair(const T1& t1, const T2& t2)
    : first_(t1)
    , second_(t2)
{
}

template <typename T1, typename T2>
Pair<T1, T2>
make_pair(const T1& t1, const T2& t2)
{
    return Pair<T1, T2>(t1, t2);
}

template <typename Data>
Set<Data>::Node::Node(const Data& data, Node* parent, Node* left, Node* right)
    : data_(data)
    , parent_(parent)
    , left_(left)
    , right_(right)
{
}

template <typename Data>
Set<Data>::Set()
    : root_(NULL)
{
}

template <typename Data>
Set<Data>::Set(const Set& rhv)
    : root_(NULL)
{
    if (rhv.root_ != NULL) {
        std::queue<Node*> queue;
        queue.push(rhv.root_);
        while (queue) {
            if(queue.front().left()) {
                queue.push(queue.front()->left_);
            }
            if (queue.front().right_) {
                queue.push(queue.front()->right_);
            }
            goDawn(iterator(root_), queue.front()->data_);
            queue.pop();
        }
    }
}

template <typename Data>
template <typename InputIterator>                                                                                                                                                                  
Set<Data>::Set(InputIterator f, InputIterator l)
    : root_(NULL)
{
    insert(f, l);
}

template <typename Data>
Set<Data>::~Set()
{
    if (root_ != NULL) {
        clear();
    }
}

template <typename Data>
const Set<Data>&
Set<Data>::operator=(const Set& rhv)
{
    clear();
    if (rhv.root_ != NULL) {
        std::queue<Node*> queue;
        queue.push(rhv.root_);
        while (queue) {
            if(queue.front().left()) {
                queue.push(queue.front()->left_);
            }
            if (queue.front().right_) {
                queue.push(queue.front()->right_);
            }
            goDawn(iterator(root_), queue.front()->data_);
            queue.pop();
        }
    }
}

template <typename Data>
void
Set<Data>::swap(Set<Data>& rhv)
{
    Node* temp = rhv.root_;
    rhv.root_ = root_;
    root_ = temp;
}

template <typename Data>
typename Set<Data>::size_type 
Set<Data>::size() const
{
    size_type size = 0;
    for (const_iterator it = begin(); it != end(); ++it) {
        ++size;
    }
    return size;
}

template <typename Data>
typename Set<Data>::size_type
Set<Data>::max_size() const
{
    return 2147000 / sizeof(Node);
}

template <typename Data>
bool
Set<Data>::empty() const
{
    return NULL == root_;
}

template <typename Data>
void
Set<Data>::clear()
{
    if (root_ != NULL) {
        postOrder(root_, &deleteNode<Data>);
        root_ = NULL;
    }
}

template <typename T>
void
deleteNode(typename Set<T>::Node* n)
{
    delete n;
    n = NULL;
}
    
template <typename Data>
void
Set<Data>::goUp(iterator& it, const value_type& x)
{
    if (!it || !it.parent() || x == *it) { 
        return;
    }
    iterator temp = (x < *it) ? it.firstLeftParent() : it.firstRightParent();
    if (temp.isRoot()) {
        return;
    }
    const bool isRightPlace = (x < *it) ? (*temp < x) : (*temp > x);
    if (isRightPlace) {
        return;
    }
    goUp(it = temp, x);
}

template <typename Data>
bool
Set<Data>::goDown(iterator& it, const value_type& x)
{
    if (NULL == root_) {
        it.ptr_ = root_ = new Node(x);
        return true;
    }
    if (x < *it) {
        if (!it.left()) { 
            it.createLeft(x);
            it.goLeft();
            return true;
        }
        return goDown(it = it.left(), x);
    }
    if (x > *it) {
        if (!it.right()) {
            it.createRight(x);
            it.goRight();
            return true;
        }
        return goDown(it = it.right(), x);
    }
    return false;
}

template <typename Data>
typename Set<Data>::Node*
Set<Data>::leftMost() const
{
    if (NULL == root_) {
        return NULL;
    }
    Node* temp = root_;
    while (temp->left_ != NULL) {
        temp = temp->left_;
    }
    return temp;
}

template <typename Data>
typename Set<Data>::Node*
Set<Data>::rightMost() const
{
    if (NULL == root_) {
        return NULL;
    }
    Node* temp = root_;
    while (temp->right_ != NULL) {
        temp = temp->right_;
    }
    return temp;
}

template <typename Data>
Set<Data>::const_iterator::const_iterator()
    : ptr_(NULL)
{
}

template <typename Data>
Set<Data>::const_iterator::const_iterator(const const_iterator& rhv)
    : ptr_(rhv.ptr_)
{
}

template <typename Data>
Set<Data>::const_iterator::const_iterator(Node* ptr)
    : ptr_(ptr)
{
}


template <typename Data>
Set<Data>::const_iterator::~const_iterator()
{
    ptr_ = NULL;
}

template <typename Data>
typename Set<Data>::Node*
Set<Data>::const_iterator::getPtr() const
{
    return ptr_;
}

template <typename Data>
const typename Set<Data>::const_iterator&
Set<Data>::const_iterator::operator=(const const_iterator& rhv)
{
    ptr_ = rhv.ptr_;
    return *this;
}

template <typename Data>
bool
Set<Data>::const_iterator::isLeftParent() const
{
    if (NULL == ptr_ || NULL == ptr_->parent_) {
        return false;
    }
    return ptr_->parent_->right_ == ptr_;
}

template <typename Data>
bool
Set<Data>::const_iterator::isRightParent() const
{
    if (NULL == ptr_ || NULL == ptr_->parent_) {
        return false;
    }
    return ptr_->parent_->left_ == ptr_;
}

template <typename Data>
void
Set<Data>::const_iterator::nextInOrder()
{
    if (right()) {
        goRight();
        while (left()) {
            goLeft();
        }
        return;
    }
    if(!isRoot()) {
        while (isLeftParent()) {
            goParent();
            if (!parent()) {
                ptr_ = NULL;
                return;
            }
        }
        if (isRightParent()) {
            goParent();
            return;
        }
    } else {
        ptr_ = NULL;
    }
}

template <typename Data>
void
Set<Data>::const_iterator::prevInOrder()
{
    if (left()) {
        goLeft();
        while (right()) {
            goRight();
        }
        return;
    }
    if (isRoot()) {
        while (isRightParent()) {
            goParent();
            if (!parent()) {
                ptr_ = NULL;
                return;
            }
        }
        if (isLeftParent()) {
            goParent();
            return;
        }
    } else {
        ptr_ = NULL;
    }
}

template <typename Data>
const typename Set<Data>::const_iterator&
Set<Data>::const_iterator::operator++()
{
    nextInOrder();
    return *this;
}

template <typename Data>
const typename Set<Data>::const_iterator&
Set<Data>::const_iterator::operator++(int)
{
    const_iterator temp = *this;
    nextInOrder();
    return temp;
}

template <typename Data>
const typename Set<Data>::const_iterator&
Set<Data>::const_iterator::operator--()
{
    prevInOrder();
    return *this;
}

template <typename Data>
const typename Set<Data>::const_iterator&
Set<Data>::const_iterator::operator--(int)
{
    const_iterator temp;
    prevInOrder();
    return temp;
}

template <typename Data>
typename Set<Data>::const_reference
Set<Data>::const_iterator::operator*() const
{
    return ptr_->data_;
}

template <typename Data>
typename Set<Data>::const_pointer
Set<Data>::const_iterator::operator->() const
{
    return ptr_;
}

template <typename Data>
bool
Set<Data>::const_iterator::operator==(const const_iterator& rhv) const
{
    return ptr_ == rhv.ptr_;
}

template <typename Data>
bool
Set<Data>::const_iterator::operator!=(const const_iterator& rhv) const
{
    return ptr_ != rhv.ptr_;
}

template <typename Data>
int
Set<Data>::const_iterator::height()
{
    if (NULL == ptr_) {
        return -1;
    }
    const int lh = left().height(), rh = right().height();
    return ((lh > rh) ? lh : rh) + 1;
}

template <typename Data>
int
Set<Data>::const_iterator::balance()
{
    if (NULL == ptr_) {
        return -1;
    }
    return static_cast<int>(left().height() - right().height());
}
  
template <typename Data>
typename Set<Data>::const_iterator
Set<Data>::const_iterator::parent() const
{
    return const_iterator(ptr_->parent_);
}

template <typename Data>
typename Set<Data>::iterator
Set<Data>::iterator::parent()
{
    return iterator(const_iterator::getPtr()->parent_);
}

template <typename Data>
const typename Set<Data>::const_iterator&
Set<Data>::const_iterator::firstLeftParent()
{
    while (!isLeftParent()) {
        goParent();
    }
    goParent();
    return *this;
}

template <typename Data>
const typename Set<Data>::const_iterator&
Set<Data>::const_iterator::firstRightParent()
{
    while (!isRightParent()) {
        ptr_ = ptr_->parent_;
    }
    ptr_ = ptr_->parent_;
    return *this;
}

template <typename Data>
typename Set<Data>::iterator&
Set<Data>::iterator::firstLeftParent()
{
    while (!const_iterator::isLeftParent()) {
        const_iterator::goParent();
    }
    const_iterator::goParent();
    return *this;
}

template <typename Data>
typename Set<Data>::iterator&
Set<Data>::iterator::firstRightParent()
{
    while (!const_iterator::isRightParent()) {
        const_iterator::goParent();
    }
    const_iterator::goParent();
    return *this;
}

template <typename Data>
inline bool
Set<Data>::const_iterator::isRoot() const
{
    return NULL == ptr_->parent_;
}

template <typename Data>
typename Set<Data>::const_iterator
Set<Data>::const_iterator::left() const
{
    return const_iterator(ptr_->left_);
}

template <typename Data>
typename Set<Data>::const_iterator
Set<Data>::const_iterator::right() const
{
    return const_iterator(ptr_->right_);
}

template <typename Data>
typename Set<Data>::iterator
Set<Data>::iterator::left()
{
    return iterator(const_iterator::getPtr()->left_);
}

template <typename Data>
typename Set<Data>::iterator
Set<Data>::iterator::right()
{
    return iterator(const_iterator::getPtr()->right_);
}


template <typename Data>
void
Set<Data>::const_iterator::goLeft()
{
    ptr_ = ptr_->left_;
}

template <typename Data>
void
Set<Data>::const_iterator::goRight()
{
    ptr_ = ptr_->right_;
}

template <typename Data>
void
Set<Data>::const_iterator::goParent()
{
    ptr_ = ptr_->parent_;
}

template <typename Data>
void
Set<Data>::const_iterator::createLeft(const value_type& x)
{
    ptr_->left_ = new Node(x, ptr_);
}

template <typename Data>
void
Set<Data>::const_iterator::createRight(const value_type& x)
{
    ptr_->right_ = new Node(x, ptr_);
}

template <typename Data>
void
Set<Data>::const_iterator::setParent(const const_iterator& it)
{
    ptr_->parent_ = it.ptr_;   
}

template <typename Data>
void
Set<Data>::const_iterator::setLeft(const const_iterator& it)
{ 
    ptr_->left_ = it.ptr_;   
}

template <typename Data>
void
Set<Data>::const_iterator::setRight(const const_iterator& it)
{
    ptr_->right_ = it.ptr_;     
}

template <typename Data>
Set<Data>::iterator::iterator()
    : const_iterator()
{
}

template <typename Data>
Set<Data>::iterator::iterator(const iterator& rhv)
    : const_iterator(rhv)
{
}

template <typename Data>
Set<Data>::iterator::iterator(Node* ptr)
    : const_iterator(ptr)
{
}

template <typename Data>
typename Set<Data>::reference
Set<Data>::iterator::operator*()
{
    return const_iterator::getPtr()->data_;
}

template <typename Data>
typename Set<Data>::pointer
Set<Data>::iterator::operator->()
{
    return const_iterator::getPtr();
}

template <typename Data>
Set<Data>::const_reverse_iterator::const_reverse_iterator()
    : ptr_(NULL)
{
}

template <typename Data>
Set<Data>::const_reverse_iterator::const_reverse_iterator(const const_reverse_iterator& rhv)
    : ptr_(rhv.ptr_)
{
}

template <typename Data>
Set<Data>::const_reverse_iterator::const_reverse_iterator(Node* ptr)
    : ptr_(ptr)
{
}


template <typename Data>
Set<Data>::const_reverse_iterator::~const_reverse_iterator()
{
    ptr_ = NULL;
}

template <typename Data>
typename Set<Data>::Node*
Set<Data>::const_reverse_iterator::getPtr() const
{
    return ptr_;
}

template <typename Data>
const typename Set<Data>::const_reverse_iterator&
Set<Data>::const_reverse_iterator::operator=(const const_reverse_iterator& rhv)
{
    *ptr_ = rhv.ptr_;
    return *this;
}

template <typename Data>
bool
Set<Data>::const_reverse_iterator::isLeftParent() const
{
    if (ptr_->parent_ != NULL) {
        return ptr_->parent_->right_ == ptr_;
    }
    return false;
}

template <typename Data>
bool
Set<Data>::const_reverse_iterator::isRightParent() const
{
    if (ptr_->parent_ != NULL) {
        return ptr_->parent_->left_ == ptr_;
    }
    return false;
}

template <typename Data>
void
Set<Data>::const_reverse_iterator::prevInOrder()
{
    if (right()) {
        goRight();
        while (left()) {
            goLeft();
        }
        return;
    }
    while (isLeftParent()) {
            goParent();
            if (!parent()) {
                ptr_ = NULL;
                return;
            }
    }
    if (isRightParent()) {
        goParent();
        return;
    }
}

template <typename Data>
void
Set<Data>::const_reverse_iterator::nextInOrder()
{
    if (left()) {
        goLeft();
        while (right()) {
            goRight();
        }
        return;
    }
    while (isRightParent()) {
            goParent();
            if (!parent()) {
                ptr_ = NULL;
                return;
            }
    }
    if (isLeftParent()) {
        goParent();
        return;
    }
}

template <typename Data>
const typename Set<Data>::const_reverse_iterator&
Set<Data>::const_reverse_iterator::operator++()
{
    nextInOrder();
    return *this;
}

template <typename Data>
const typename Set<Data>::const_reverse_iterator&
Set<Data>::const_reverse_iterator::operator++(int)
{
    const_reverse_iterator temp = *this;
    nextInOrder();
    return temp;
}

template <typename Data>
const typename Set<Data>::const_reverse_iterator&
Set<Data>::const_reverse_iterator::operator--()
{
    prevInOrder();
    return *this;
}

template <typename Data>
const typename Set<Data>::const_reverse_iterator&
Set<Data>::const_reverse_iterator::operator--(int)
{
    const_reverse_iterator temp;
    prevInOrder();
    return temp;
}

template <typename Data>
typename Set<Data>::const_reference
Set<Data>::const_reverse_iterator::operator*() const
{
    return ptr_->data;
}

template <typename Data>
typename Set<Data>::const_pointer
Set<Data>::const_reverse_iterator::operator->() const
{
    return ptr_;
}

template <typename Data>
void
Set<Data>::const_reverse_iterator::goLeft()
{
    ptr_ = ptr_->left_;
}

template <typename Data>
void
Set<Data>::const_reverse_iterator::goRight()
{
    ptr_ = ptr_->right_;
}

template <typename Data>
void
Set<Data>::const_reverse_iterator::goParent()
{
    ptr_ = ptr_->parent_;
}

template <typename Data>
typename Set<Data>::const_reverse_iterator
Set<Data>::const_reverse_iterator::left() const
{
    return const_reverse_iterator(ptr_->left_);
}

template <typename Data>
typename Set<Data>::const_reverse_iterator
Set<Data>::const_reverse_iterator::right() const
{
    return const_reverse_iterator(ptr_->right_);
}

template <typename Data>
typename Set<Data>::const_reverse_iterator
Set<Data>::const_reverse_iterator::parent() const
{
    return const_reverse_iterator(ptr_->parent_);
}

template <typename Data>
bool
Set<Data>::const_reverse_iterator::operator==(const const_reverse_iterator& rhv) const
{
    return ptr_ == rhv.ptr_;
}

template <typename Data>
bool
Set<Data>::const_reverse_iterator::operator!=(const const_reverse_iterator& rhv) const
{
    return ptr_ != rhv.ptr_;
}


template <typename Data>
Set<Data>::reverse_iterator::reverse_iterator()
    : const_reverse_iterator()
{
}

template <typename Data>
Set<Data>::reverse_iterator::reverse_iterator(const reverse_iterator& rhv)
    : const_reverse_iterator(rhv)
{
}

template <typename Data>
Set<Data>::reverse_iterator::reverse_iterator(Node* ptr)
    : const_reverse_iterator(ptr)
{
}

template <typename Data>
Set<Data>::const_iterator::operator bool() const
{
    return NULL != ptr_;
}

template <typename Data>
Set<Data>::iterator::operator bool() const
{
    return const_iterator::getPtr() != NULL;
}


template <typename Data>
typename Set<Data>::reference
Set<Data>::reverse_iterator::operator*()
{
    return const_reverse_iterator::getPtr()->data;
}

template <typename Data>
typename Set<Data>::pointer
Set<Data>::reverse_iterator::operator->()
{
    return const_reverse_iterator::getPtr();
}

template <typename Data>
typename Set<Data>::const_iterator
Set<Data>::begin() const
{
    return const_iterator(leftMost());
}

template <typename Data>
typename Set<Data>::const_iterator
Set<Data>::end() const
{
    return const_iterator(NULL);
}

template <typename Data>
typename Set<Data>::iterator
Set<Data>::begin()
{
    return iterator(leftMost());
}

template <typename Data>
typename Set<Data>::iterator
Set<Data>::end()
{
    return iterator(NULL);
}

template <typename Data>
typename Set<Data>::const_reverse_iterator
Set<Data>::rbegin() const
{
    return const_reverse_iterator(rightMost());
}

template <typename Data>
typename Set<Data>::const_reverse_iterator
Set<Data>::rend() const
{
    return const_reverse_iterator(NULL);
}

template <typename Data>
typename Set<Data>::reverse_iterator
Set<Data>::rbegin()
{
    return reverse_iterator(rightMost());
}

template <typename Data>
typename Set<Data>::reverse_iterator
Set<Data>::rend()
{
    return reverse_iterator(NULL);
}

template <typename Data>
void
Set<Data>::balance(iterator& it)
{
    if (!it) {
        return;
    }
    const int factor = it.balance();
    if (factor > 1) {
        if (it.left().balance() < 0) {
            iterator temp = it.left();
            rotateRight(temp);
        }
        rotateLeft(it);
    } else if (factor < -1) {
        if (it.right().balance() > 0) {
            iterator temp = it.right();
            rotateLeft(temp);
        }
        rotateRight(it);
    } else {
        balance(it = it.parent());
    }
}

template <typename Data>
void 
Set<Data>::rotateRight(iterator& it)
{
    iterator itParent = it.parent(), itRight = it.right();
    const bool isRightParent = it.isRightParent();
    it.setRight(itRight.left());
    if (itRight.left()) { 
        itRight.left().setParent(it);
    }
    itRight.setLeft(it);
    it.setParent(itRight);
    itRight.setParent(itParent);
    if (itParent) {
        if (isRightParent) { 
            itParent.setLeft(itRight);
        } else {
            itParent.setRight(itRight);
        }
    } else {
        root_ = itRight.ptr_;
    }
    it = itRight;
}

template <typename Data>
void
Set<Data>::rotateLeft(iterator& it) {
    iterator itParent = it.parent(), itLeft = it.left();
    const bool isLeftParent = it.isLeftParent();
    it.setLeft(itLeft.right());
    if (itLeft.right()) {
        itLeft.right().setParent(it);
    }
    itLeft.setRight(it);
    it.setParent(itLeft);
    itLeft.setParent(itParent);
    if (itParent) {
        if (isLeftParent) {
            itParent.setRight(itLeft);
        } else {
            itParent.setLeft(itLeft);
        }
    } else {
        root_ = itLeft.ptr_;
    }
    it = itLeft;
}


template <typename Data>
Pair<typename Set<Data>::iterator, bool>
Set<Data>::insert(const value_type& x)
{
    return insert(iterator(root_), x);
}

template <typename Data>
Pair<typename Set<Data>::iterator, bool>
Set<Data>::insert(iterator it, const value_type& x)
{
    goUp(it, x);
    const bool isAdded = goDown(it, x);
    if (isAdded) {
        balance(it);
    }
    return Pair<iterator, bool>(it, isAdded);
}

template <typename Data>
template <typename InputIt>
void
Set<Data>::insert(InputIt f, InputIt l)
{
    for (InputIt it = f; it != l; ++it) {
        insert(iterator(root_), *it);
    }

}

template <typename Data>
typename Set<Data>::iterator
Set<Data>::eraseUnBalanced(iterator& pos)
{
    iterator leftIt = pos.left();
    iterator rightIt = pos.right();
    if (pos.balance() > 0) {
        if (pos.parent()) {
            if (pos.isLeftParent()) {
                pos.parent().setRight(leftIt);
            }
            if (pos.isRightParent()) {
                pos.parent().setLeft(leftIt);
            }
            if (leftIt) {
                leftIt.setParent(pos.parent());
            }
        } else {
            root_ = leftIt.ptr_;
        }
        if (rightIt) {
            iterator leftRightIt = leftIt;
            while (leftRightIt.right()) {
                leftRightIt.goRight();
            }
            leftRightIt.setRight(rightIt);
            rightIt.setParent(leftRightIt);
        }
        delete pos.ptr_;
        return leftIt;
    } else {
        if (pos.parent()) {
            if (pos.isLeftParent()) {
                pos.parent().setRight(rightIt);
            }
            if (pos.isRightParent()) {
                pos.parent().setLeft(rightIt);
            }
            if (rightIt) {
                rightIt.setParent(pos.parent());
            }
        } else {
            root_ = rightIt.ptr_;
        }
        if (leftIt) {
            iterator rightLeftIt = rightIt;
            while (rightLeftIt.left()) {
                rightLeftIt.goLeft();
            }
            rightLeftIt.setLeft(leftIt);
            leftIt.setParent(rightLeftIt);
        }
        delete pos.ptr_;
        return rightIt;
    }
}

template <typename Data>
void
Set<Data>::erase(iterator pos)
{
    pos = eraseUnBalanced(pos);
    balance(pos);
}

template <typename Data>
void
Set<Data>::erase(iterator first, iterator last)
{
    iterator it = first;
    for (; it < last; ++it) {
        it = eraseUnBalanced(it);
    }
}

template <typename Data>
typename Set<Data>::iterator
Set<Data>::find(const key_type& k) const
{
    iterator it(root_);
    while (k != *it || it.ptr_ == NULL) {
        if (k > *it) {
            it.goRight();
        } else {
            it.goLeft();
        }
    }
    return it;
}

template <typename Data>
typename Set<Data>::size_type
Set<Data>::count(const key_type& k) const
{
    iterator it = find(k);
    if (!it) {
        return 0;
    }
    return 1;
}

template <typename Data>
typename Set<Data>::iterator
Set<Data>::lower_bound(const key_type& k) const
{
    iterator it = iterator(root_);
    while (k != *it) {
        if (k > *it) {
            it.goRight();
            if (k < *it) {
                while (*it > k) {
                    it.goLeft()
                }
                return ++it;
            }
        } else {
            it.goLeft();
            if (k > *it) {
                while(*it < k) {
                    it.goRight();
                }
                return it;
            }
        }
    }
    return it;
}

template <typename Data>
typename Set<Data>::iterator
Set<Data>::upper_bound(const key_type& k) const
{
    iterator it = iterator(root_);
    while (k != *it) {
        if (k > *it) {
            it.goLefti();
            if (k < *it) {
                return ++it;
            }
        } else {
            it.goRight();
            if (k > *it) {
                return it;
            }
        }
    }
    return it;
}

template <typename Data>
bool
Set<Data>::operator==(const Set& rhv) const
{
    iterator it = begin();
    iterator itRhv = rhv.begin();
    while (it || itRhv) {
        if (*it != *itRhv) {
            return false;
        }
        ++it;
        ++itRhv;
    }
    return NULL == it && NULL == itRhv;
}

template <typename Data>
bool
Set<Data>::operator!=(const Set& rhv) const
{
    return !(*this == rhv);
}

template <typename Data>
bool
Set<Data>::operator<(const Set& rhv) const
{
    bool inLess = true;
    iterator it = begin();
    iterator itRhv = rhv.begin();
     while (it != NULL || itRhv != NULL) {
        if (*it < *itRhv) {
            inLess = false;
        }
        ++it;
        ++itRhv;
    }
    if (NULL == it && NULL == itRhv) {
        return inLess;
    }
    return NULL == it;
}

template <typename Data>
bool
Set<Data>::operator>(const Set& rhv) const
{
    return rhv < *this;
}

template <typename Data>
bool
Set<Data>::operator<=(const Set& rhv) const
{
    return !(rhv > *this);
}

template <typename Data>
bool
Set<Data>::operator>=(const Set& rhv) const
{
    return !(rhv < *this);
}

template <typename Data>
void
Set<Data>::helperPrint(Node* root, int distance)
{
    static const int INDENT = 5;
    if (root) {
        helperPrint(root->right_, distance + 1);
        for (int i = 0; i < distance * INDENT; ++i) {
            std::cout << ' ';
        }
        std::cout << root->data_ << std::endl;
        helperPrint(root->left_, distance + 1);
    }
}

template <typename Data>
void
Set<Data>::print()
{
    helperPrint(root_, 0);
}

template <typename Data>
void
Set<Data>::inOrder(Node* n, void (*visit)(Node*))
{
    if (n != NULL) {
        inOrder(n->left_, visit);
        visit(n);
        inOrder(n->right_, visit);
    }
}

template <typename Data>
void
Set<Data>::preOrder(Node* n, void (*visit)(Node*))
{
    if (n != NULL) {
        visit(n);
        preOrder(n->left_, visit);
        preOrder(n->right_, visit);
    }
}

template <typename Data>
void
Set<Data>::postOrder(Node* n, void (*visit)(Node*))
{
    if (n != NULL) {
        postOrder(n->left_, visit);
        postOrder(n->right_, visit);
        visit(n);
    }
}

template <typename Data>
void
Set<Data>::levelOrder(Node* n, void (*visit)(Node*))
{
    if (NULL == n) {
        return;
    }
    std::queue<Node*> queue;
    queue.push(n);
    while (queue) {
        if(queue.front().left()) {
            queue.push(queue.front()->left_);
        }
        if (queue.front().right_) {
            queue.push(queue.front()->right_);
        }
        visit(queue.front());
        queue.pop();
    }
}

 #endif ///_SET_CPP__

