#include <gtest/gtest.h>
#include "headers/Set.hpp"

TEST(SetTest, Constructor)
{  
    Set<int> s;
    EXPECT_EQ(s.size(), 0);
}

TEST(SetTest, Insert)
{
    Set<int> s;
    Set<int>::iterator it;
    s.insert(7);
    it = s.begin();
    EXPECT_EQ(*it, 7);
    EXPECT_EQ(s.size(), 1);
    s.insert(4);
    --it;
    EXPECT_EQ(*it, 4);
    EXPECT_EQ(s.size(), 2);
}

TEST(SetTest, RangeConstructor)
{
    typedef Set<int> S;
    typedef Set<int>::iterator SI;
    int a[] = { 1, 3, 5, 6,};
    S s(a, a + 4);
    SI it = s.begin();
    EXPECT_EQ(s.size(), 4);
    EXPECT_EQ(*it, 1);
    ++it;
    EXPECT_EQ(*it, 3);
    ++it;
    EXPECT_EQ(*it, 5);
    it = s.end();
}

TEST(SetTest, InsertFunctionRange)
{
    typedef Set<int> S;
    typedef Set<int>::iterator SI;
    int a[] = { 1, 3, 5, 8 };
    S s(a, a + 4);
    int b[] = { 2, 4, 2, 7, 9 };
    s.insert(b, b + 5);
    SI it = s.begin();
    ++it;
    EXPECT_EQ(*it, 2);
    ++it;
    ++it;
    EXPECT_EQ(s.size(),8);
    EXPECT_EQ(*it, 4);
}

TEST(SetTest, EraseFunction)
{
    typedef Set<int> S;
    typedef Set<int>::iterator SI;
    int a[] = { 1, 3, 5, 4, 6};
    S s(a, a + 5);
    SI it = s.begin();
    ++it;
    EXPECT_EQ(*it,3);
    ++it;
    s.erase(it);
    it = s.begin();
    ++it;
    EXPECT_EQ(*it,3);
}

TEST(SetTest, Swop)
{
    typedef Set<int> S;
    int a[] = { 1, 3, 5, 8 };
    S s1(a, a + 4);
    int b[] = { 2, 4, 2, 7, 9 };
    S s2(b, b + 5);
    s1.swap(s2);
    s1.print();
    s2.print();
}
  
  
int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

