#include <iostream>
#include <gtest/gtest.h>
#include "headers/Stack.hpp"

struct A {
    int data_;
    A() : data_(0) {}
    A(const int data) : data_(data) {}
};

TEST(StackTest, PushStack)
{
    Stack<A> s;
    A a1(8);
    s.push(a1);
    EXPECT_EQ(s.top().data_, 8);
}

TEST(StackTest, SizeStack)
{
    Stack<A> s;
    A a1;
    A a2;
    s.push(a1);
    s.push(a2);
    EXPECT_EQ(s.size(), 2);
}

TEST(StackTest, TopStack)
{
    Stack<A> s;
    A a1(8);
    A a2(5);
    s.push(a1);
    s.push(a2);
    EXPECT_EQ(s.top().data_, 5);
}

TEST(StackTest, PopStack)
{
    Stack<A> s;
    A a1(8);
    A a2(5);
    A a3(44);
    s.push(a1);
    s.push(a2);
    s.push(a3);
    EXPECT_EQ(s.top().data_, 44);
    s.pop();
    EXPECT_EQ(s.top().data_, 5);
    EXPECT_EQ(s.size(), 2);

}
  
int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

