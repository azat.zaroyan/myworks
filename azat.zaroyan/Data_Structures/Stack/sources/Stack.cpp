#ifndef __STACK_CPP__
#define __STACK_CPP__

#include "headers/Stack.hpp"
#include <iostream>
#include <vector>

template <typename TT>
bool
operator==(const Stack<TT>& lhv, const Stack<TT>& rhv)
{
    return lhv.buffer_ == rhv.buffer_;
}

template <typename TT>
bool
operator!=(const Stack<TT>& lhv, const Stack<TT>& rhv)
{
    return !(lhv.buffer_ == rhv.buffer_);
}

template <typename TT>
bool
operator>(const Stack<TT>& lhv, const Stack<TT>& rhv)
{
    return lhv.buffer_ > rhv.buffer_;
}

template <typename TT>
bool
operator<(const Stack<TT>& lhv, const Stack<TT>& rhv)
{
    return rhv > lhv;
}

template <typename TT>
bool
operator>=(const Stack<TT>& lhv, const Stack<TT>& rhv)
{
    return !(lhv < rhv);
}

template <typename TT>
bool
operator<=(const Stack<TT>& lhv, const Stack<TT>& rhv)
{
    return !(lhv > rhv);
}

template <typename TT>
void
swap(const Stack<TT>& lhv, const Stack<TT>& rhv)
{
    swap(lhv.buffer_, rhv.buffer_);
}

template <typename T>
Stack<T>::Stack()
    : buffer_(container_type())
{
}

template <typename T>
Stack<T>::Stack(const Stack<T>& rhv)
    : buffer_(container_type(rhv.buffer_))
{
}

template <typename T>
Stack<T>&
Stack<T>::operator= (const Stack& rhv)
{
        buffer_ = rhv.buffer_;
        return *this;
}

template <typename T>
bool
Stack<T>::empty() const
{
    return buffer_.empty();
}

template <typename T>
typename Stack<T>::size_type
Stack<T>::size() const
{
    return buffer_.size();
}

template <typename T>
typename Stack<T>::reference
Stack<T>::top()
{
    return buffer_.back();
}

template <typename T>
typename Stack<T>::const_reference
Stack<T>::top() const
{
    return buffer_.back();
}

template <typename T>
void
Stack<T>::push(const value_type& val)
{
    buffer_.push_back(val);
}

template <typename T>
void
Stack<T>::pop()
{
    buffer_.pop_back();
}

#endif ///__STACK_CPP__
