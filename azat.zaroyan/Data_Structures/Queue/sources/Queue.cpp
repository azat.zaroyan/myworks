#ifndef __QUEUE_CPP__
#define __QUEUE_CPP__

#include "headers/Queue.hpp"
#include <iostream>
#include <deque>

template <typename TT>
bool
operator==(const Queue<TT>& lhv, const Queue<TT>& rhv)
{
    return false;//lhv.buffer_ == rhv.buffer_;
}

template <typename TT>
bool
operator!=(const Queue<TT>& lhv, const Queue<TT>& rhv)
{
    return !(lhv.buffer_ == rhv.buffer_);
}

template <typename TT>
bool
operator>(const Queue<TT>& lhv, const Queue<TT>& rhv)
{
    return lhv.buffer_ > rhv.buffer_;
}

template <typename TT>
bool
operator<(const Queue<TT>& lhv, const Queue<TT>& rhv)
{
    return rhv > lhv;
}

template <typename TT>
bool
operator>=(const Queue<TT>& lhv, const Queue<TT>& rhv)
{
    return !(lhv < rhv);
}

template <typename TT>
bool
operator<=(const Queue<TT>& lhv, const Queue<TT>& rhv)
{
    return !(lhv > rhv);
}

template <typename TT>
void
swap(const Queue<TT>& lhv, const Queue<TT>& rhv)
{
    swap(lhv.buffer_, rhv.buffer_);
}

template <typename T>
Queue<T>::Queue()
    : buffer_()
{
}

template <typename T>
Queue<T>::Queue(const Queue<T>& rhv)
    : buffer_(rhv.buffer_)
{
}

template <typename T>
Queue<T>&
Queue<T>::operator= (const Queue<T>& rhv)
{
        buffer_ = rhv.buffer_;
        return *this;
}

template <typename T>
bool
Queue<T>::empty() const
{
    return buffer_.empty();
}

template <typename T>
typename Queue<T>::size_type
Queue<T>::size() const
{
    return buffer_.size();
}

template <typename T>
typename Queue<T>::reference
Queue<T>::back()
{
    return buffer_.back();
}

template <typename T>
typename Queue<T>::const_reference
Queue<T>::back() const
{
    return buffer_.back();
}

template <typename T>
typename Queue<T>::reference
Queue<T>::front()
{
    return buffer_.front();
}

template <typename T>
typename Queue<T>::const_reference
Queue<T>::front() const
{
    return buffer_.front();
}

template <typename T>
void
Queue<T>::push(const value_type& val)
{
    buffer_.push_back(val);
}

template <typename T>
void
Queue<T>::pop()
{
    buffer_.pop_front();
}

#endif ///__QUEUE_CPP__

