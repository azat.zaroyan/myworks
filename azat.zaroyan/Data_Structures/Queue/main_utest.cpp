#include <iostream>
#include <gtest/gtest.h>
#include "headers/Queue.hpp"

struct A {
    int data_;
    A() : data_(0) {}
    A(const int data) : data_(data) {}
};

TEST(QueueTest, PushQueue)
{
    Queue<A> s;
    A a1(4);
    s.push(a1);
    EXPECT_EQ(s.back().data_, 4);
}

TEST(QueueTest, SizeQueue)
{
    Queue<A> s;
    A a1;
    A a2;
    s.push(a1);
    s.push(a2);
    EXPECT_EQ(s.size(), 2);
}

TEST(QueueTest, BackQueue)
{
    Queue<A> s;
    A a1(8);
    A a2(5);
    s.push(a1);
    s.push(a2);
    EXPECT_EQ(s.back().data_, 5);
}

TEST(QueueTest, FrontQueue)
{
    Queue<A> s;
    A a1(8);
    A a2(5);
    s.push(a1);
    s.push(a2);
    EXPECT_EQ(s.front().data_, 8);
}

TEST(QueueTest, PopQueue)
{
    Queue<A> s;
    A a1(8);
    A a2(5);
    A a3(44);
    s.push(a1);
    s.push(a2);
    s.push(a3);
    EXPECT_EQ(s.back().data_, 44);
    s.pop();
    EXPECT_EQ(s.back().data_, 44);
    EXPECT_EQ(s.size(), 2);
    EXPECT_EQ(s.front().data_, 5);
}
  
int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

