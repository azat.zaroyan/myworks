#ifndef __QUEUE_HPP__
#define __QUEUE_HPP__
#include <deque>

template <typename T>
class Queue
{
    template <typename TT>
    friend bool operator==(const Queue<TT>& lhv, const Queue<TT>& rhv);
    template <typename TT>
    friend bool operator!=(const Queue<TT>& lhv, const Queue<TT>& rhv);
    template <typename TT>
    friend bool operator>(const Queue<TT>& lhv, const Queue<TT>& rhv);
    template <typename TT>
    friend bool operator<(const Queue<TT>& lhv, const Queue<TT>& rhv);
    template <typename TT>
    friend bool operator>=(const Queue<TT>& lhv, const Queue<TT>& rhv);
    template <typename TT>
    friend bool operator<=(const Queue<TT>& lhv, const Queue<TT>& rhv);
    template <typename TT>
    friend void swap(const Queue<TT>& lhv, const Queue<TT>& rhv);

public:
    typedef T                       value_type;
    typedef std::deque<value_type>  container_type;
    typedef size_t                  size_type;
    typedef value_type&             reference;
    typedef const value_type&       const_reference;

    Queue();
    Queue(const Queue<T>& rhv);
    Queue& operator=(const Queue& rhv);
    bool empty() const;
    size_type size() const;
    reference back();
    const_reference back() const;
    reference front();
    const_reference front() const;
    void push(const value_type& val);
    void pop();
private:
    container_type buffer_;
};

#include "sources/Queue.cpp"

#endif ///__QUEUE_HPP__

