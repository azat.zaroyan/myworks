#ifndef __ALGORITHMS_HPP__
#define __ALGORITHMS_HPP__

namespace cd02
{
template <typename ForwardIterator, typename T>
void fill(ForwardIterator first, ForwardIterator last, const T& value);

template <typename OutputIterator, typename Size, typename T>
OutputIterator fill_n(OutputIterator first, Size n, const T& value);

template <typename InputIterator, typename EqualityComparable, typename Size>
void count(InputIterator first, InputIterator last,  const EqualityComparable& value, Size& n);

template <typename InputIterator, typename OutputIterator>
OutputIterator copy(InputIterator first, InputIterator last, OutputIterator result);

template <typename InputIterator, typename Size, typename OutputIterator>
OutputIterator copy_n(InputIterator first, Size n, OutputIterator result);

template <typename ForwardIterator, typename T>
void iota(ForwardIterator first, ForwardIterator last, T value);

template <typename ForwardIterator, typename Generator>
void generate(ForwardIterator first, ForwardIterator last, Generator gen);

template <typename OutputIterator, typename Size, typename Generator>
OutputIterator generate_n(OutputIterator first, Size n, Generator gen);

template <typename InputIterator1, typename InputIterator2>
bool includes(InputIterator1 f1, InputIterator1 l1, InputIterator2 f2, InputIterator2 l2);

template <typename InputIterator1, typename InputIterator2, typename OutputIterator>
OutputIterator set_union(InputIterator1 f1, InputIterator1 l1,
                        InputIterator2 f2, InputIterator2 l2, OutputIterator result);

template <typename InputIterator1, typename InputIterator2, typename OutputIterator>
OutputIterator set_intersection(InputIterator1 f1, InputIterator1 l1,
                                InputIterator2 f2, InputIterator2 l2, OutputIterator result);

template <typename InputIterator1, typename InputIterator2, typename OutputIterator>
OutputIterator set_difference(InputIterator1 f1, InputIterator1 l1,
                              InputIterator2 f2, InputIterator2 l2, OutputIterator result);

template <typename InputIterator1, typename InputIterator2, typename OutputIterator>
OutputIterator set_symmetric_difference(InputIterator1 f1, InputIterator1 l1,
                              InputIterator2 f2, InputIterator2 l2, OutputIterator result);

}

#include "sources/Algorithms.cpp"
#endif ///__ALGORITHMS_HPP__

