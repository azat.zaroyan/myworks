#include <gtest/gtest.h>
#include "headers/Algorithms.hpp"
#include <list>
#include <vector>
#include <set>

int num() { return 4;}

TEST(CopyTest, ListIterator)
{
    typedef std::list<int> L;
    typedef std::list<int>::iterator LI;
    int a [] = { 1, 2, 3, 4};
    L l1(a, a + 4);
    L l2(4);
    LI it = cd02::copy(l1.begin(), l1.end(), l2.begin());
    it = l2.begin();
    EXPECT_EQ(*it,1);
    ++it;
    EXPECT_EQ(*it, 2);
}

TEST(CopyNTest, ListIterator)
{
    typedef std::list<int> L;
    typedef std::list<int>::iterator LI;
    int a [] = { 1, 2, 3, 4};
    L l1(a, a + 4);
    L l2(4);
    LI it = cd02::copy_n(l1.begin(), 4, l2.begin());
    it = l2.begin();
    EXPECT_EQ(*it,1);
    ++it;
    EXPECT_EQ(*it, 2);
}

TEST(CountTest, ListIterator)
{
    typedef std::list<int> L;
    int a [] = { 1, 2, 1, 4};
    L l1(a, a + 4);
    int n = 0;
    cd02::count(l1.begin(), l1.end(), 1, n);
    EXPECT_EQ(n, 2);
}

TEST(IotaTest, ListIterator)
{
    typedef std::list<int> L;
    typedef std::list<int>::iterator LI;
    L l1(4);
    int n = 0;
    cd02::iota(l1.begin(), l1.end(), n);
    LI it = l1.begin();
    ++it;
    EXPECT_EQ(*it, 1);
}

TEST(FillTest, ListIterator)
{
    typedef std::list<int> L;
    typedef std::list<int>::iterator LI;
    L l1(4);
    cd02::fill(l1.begin(), l1.end(), 5);
    LI it = l1.begin();
    EXPECT_EQ(*it, 5);
    ++it;
    EXPECT_EQ(*it, 5);
}

TEST(FillNTest, ListIterator)
{
    typedef std::list<int> L;
    typedef std::list<int>::iterator LI;
    L l1(5);
    LI it = cd02::fill_n(l1.begin(), 5, 4);
    --it;
    EXPECT_EQ(*it, 4);
    --it;
    EXPECT_EQ(*it, 4);
}

TEST(GenerateTest, ListIterator)
{
    typedef std::list<int> L;
    typedef std::list<int>::iterator LI;
    L l1(7);
    cd02::generate(l1.begin(), l1.end(), num);
    LI it = l1.begin();
    EXPECT_EQ(*it, 4);
    ++it;
    EXPECT_EQ(*it, 4);
}

TEST(GenerateNTest, ListIterator)
{
    typedef std::list<int> L;
    typedef std::list<int>::iterator LI;
    L l(7);
    LI it = l.begin();
    it = cd02::generate_n(it, 7, num);
    it = l.begin();
    EXPECT_EQ(*it, 4);
    ++it;
    EXPECT_EQ(*it, 4);
}

TEST(Includes, SetIterator)
{
    typedef std::set<int> S;
    int a[] = { 1, 2, 4, 5, 7, 8 };
    int b[] = { 2, 3, 6, 7, 9, 12, 56 };
    S s1(a, a + 6);
    S s2(b, b + 7);
    EXPECT_EQ(cd02::includes(s1.begin(), s1.end(), s2.begin(), s2.end()), false);
    int a1[] = { 1, 2, 3, 4, 5};
    int b1[] = { 2, 3, 5 };
    S s3(a1, a1 + 5);
    S s4(b1, b1 + 3);
    EXPECT_EQ(cd02::includes(s3.begin(), s3.end(), s4.begin(), s4.end()), true);
}

TEST(SetUnion, SetIterator)
{
    typedef std::set<int> S;
    typedef std::vector<int> V;
    typedef std::vector<int>::iterator VI;
    int a[] = { 1, 4, 5, 7, 8, 12 };
    int b[] = { 2, 3, 4, 6, 7, 9, 12, 56 };
    S s1(a, a + 6);
    S s2(b, b + 8);
    V v(11);
    VI it = v.begin();
    it = set_union(s1.begin(), s1.end(), s2.begin(), s2.end(), it);
    it = v.begin();
    EXPECT_EQ(*it, 1);
    ++it;
    EXPECT_EQ(*it, 2);
}

TEST(SetIntersection, SetIterator)
{
    typedef std::set<int> S;
    typedef std::vector<int> V;
    typedef std::vector<int>::iterator VI;
    int a[] = { 1, 4, 5, 7, 8, 12 };
    int b[] = { 2, 3, 4, 6, 7, 9, 12, 56 };
    S s1(a, a + 6);
    S s2(b, b + 8);
    V v(3);
    VI it = v.begin();
    it = set_intersection(s1.begin(), s1.end(), s2.begin(), s2.end(), it);
    it = v.begin();
    EXPECT_EQ(*it, 4);
    ++it;
    EXPECT_EQ(*it, 7);
}

TEST(SetDifference, SetIterator)
{
    typedef std::set<int> S;
    typedef std::vector<int> V;
    typedef std::vector<int>::iterator VI;
    int a[] = { 1, 4, 5, 7, 8, 12 };
    int b[] = { 2, 3, 4, 6, 7, 9, 12, 56 };
    S s1(a, a + 6);
    S s2(b, b + 8);
    V v(3);
    VI it = v.begin();
    it = set_difference(s1.begin(), s1.end(), s2.begin(), s2.end(), it);
    it = v.begin();
    EXPECT_EQ(*it, 1);
    ++it;
    EXPECT_EQ(*it, 5);
    ++it;
    EXPECT_EQ(*it, 8);
}

TEST(SetDifferenceiSymmetric, SetIterator)
{
    typedef std::set<int> S;
    typedef std::vector<int> V;
    typedef std::vector<int>::iterator VI;
    int a[] = { 1, 4, 5, 7, 8, 12 };
    int b[] = { 2, 3, 4, 6, 7, 9, 12, 56 };
    S s1(a, a + 6);
    S s2(b, b + 8);
    V v(8);
    VI it = v.begin();
    it = set_symmetric_difference(s1.begin(), s1.end(), s2.begin(), s2.end(), it);
    it = v.begin();
    EXPECT_EQ(*it, 1);
    ++it;
    EXPECT_EQ(*it, 2);
    ++it;
    EXPECT_EQ(*it, 3);
    it = v.end();
    --it;
    EXPECT_EQ(*it, 56);
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

