#ifndef __ALGORITHMS_CPP__
#define __ALGORITHMS_CPP__
#include "headers/Algorithms.hpp"

namespace cd02 {
template <typename InputIterator, typename OutputIterator>
OutputIterator
copy(InputIterator first, InputIterator last, OutputIterator result)
{
    while (first != last) {
        *result = *first;
        ++first;
        ++result;
    }
    return result;
}

template <typename InputIterator, typename Size, typename OutputIterator>
OutputIterator
copy_n(InputIterator first, Size n, OutputIterator result)
{
    while (n > 0) {
        *result = *first;
        ++first;
        ++result;
        --n;
    }
    return result;
}

template <typename InputIterator, typename EqualityComparable, typename Size>
void
count(InputIterator first, InputIterator last,  const EqualityComparable& value, Size& n)
{
    while (first != last) {
        if (value == *first) {
            ++n;
        }
        ++first;
    }
}

template <typename ForwardIterator, typename T>
void
iota(ForwardIterator first, ForwardIterator last, T value)
{
    while (first != last) {
        *first = value;
        ++first;
        ++value;
    }
}

template <typename ForwardIterator, typename T>
void
fill(ForwardIterator first, ForwardIterator last, const T& value)
{
    while (first != last) {
        *first = value;
        ++first;
    }
}

template <typename OutputIterator, typename Size, typename T>
OutputIterator
fill_n(OutputIterator first, Size n, const T& value)
{
    while (n > 0) {
        *first = value;
        ++first;
        --n;
    }
    return first;
}

template <typename ForwardIterator, typename Generator>
void
generate(ForwardIterator first, ForwardIterator last, Generator gen)
{
    while (first != last) {
        *first = gen();
        ++first;
    }
}

template <typename OutputIterator, typename Size, typename Generator>
OutputIterator
generate_n(OutputIterator first, Size n, Generator gen)
{
    while (n > 0) {
        *first = gen();
        ++first;
        --n;
    }
    return first;
 }

template <typename InputIterator1, typename InputIterator2>
bool includes(InputIterator1 f1, InputIterator1 l1, InputIterator2 f2, InputIterator2 l2)
{
    while (f1 != l1 && f2 != l2) {
        if (*f1 == *f2) {
            ++f2;
        }
        ++f1;
    }
    return f2 == l2;
}

template <typename InputIterator1, typename InputIterator2, typename OutputIterator>
OutputIterator set_union(InputIterator1 f1, InputIterator1 l1,
                        InputIterator2 f2, InputIterator2 l2, OutputIterator result)
{
    while (f1 != l1 && f2 != l2) {
        while (f1 != l1 && f2 != l2 && *f1 <= *f2 ) {
            *result = *f1++;
            if (*result == *f2) { 
                ++f2;
            } 
            ++result;
        }
        while (f1 != l1 && f2 != l2 && *f2 <= *f1 ) {
            *result = *f2++;
            if (*result == *f1) {
                ++f1;
            }
            ++result;
        }
    }
    while (f1 != l1) {
        *result = *f1++;
        ++result;
    }
    while (f2 != l2) {
        *result = *f2++;
        ++result;
    }
    return result;
}

template <typename InputIterator1, typename InputIterator2, typename OutputIterator>
OutputIterator set_intersection(InputIterator1 f1, InputIterator1 l1,
                                InputIterator2 f2, InputIterator2 l2, OutputIterator result) 
{
    while (f1 != l1 && f2 != l2) {
        while (f1 != l1 && f2 != l2 && *f1 <= *f2) {
            if(*f1 == *f2) {
                *result = *f2++;
                ++result;
            }
            ++f1;
        }
        while (f1 != l1 && f2 != l2 && *f1 >= *f2) {
            if(*f1 == *f2) {
                *result = *f1++;
                ++result;
            }
            ++f2;
        }
    }
    return result;
}

template <typename InputIterator1, typename InputIterator2, typename OutputIterator>
OutputIterator set_difference(InputIterator1 f1, InputIterator1 l1,
                              InputIterator2 f2, InputIterator2 l2, OutputIterator result)
{
     while (f1 != l1 && f2 != l2) {
        while (f1 != l1 && f2 != l2 && *f1 <= *f2) {
            if (*f1 != *f2) {
                *result = *f1;
                ++result;
            } else {
                ++f2;
            }
            ++f1;
        }
        while (f1 != l1 && f2 != l2 && *f1 >= *f2) {
            if (*f1 == *f2) {
                ++f1;
            }
            ++f2;
        }
     }
     while (f1 != l1) {
         *result = *f1++;
         ++result;
     }
    return result;
}

template <typename InputIterator1, typename InputIterator2, typename OutputIterator>
OutputIterator set_symmetric_difference(InputIterator1 f1, InputIterator1 l1,
                              InputIterator2 f2, InputIterator2 l2, OutputIterator result)
{
    while (f1 != l1 && f2 != l2) {
        while (f1 != l1 && f2 != l2 && *f1 <= *f2) {
            if (*f1 != *f2) {
                *result = *f1;
                ++result;
            } else {
                ++f2;
            }
            ++f1;
        }
        while (f1 != l1 && f2 != l2 && *f1 >= *f2) {
            if (*f1 != *f2) {
                *result = f2;
                ++result;
            } else {
                ++f1;
            }
            ++f2;
        }
     }
     while (f1 != l1) {
         *result = *f1++;
         ++result;
     }
     while (f2 != l2) {
         *result = *f2++;
         ++result;
     }
     return result;
}

} //end namespace cd02

#endif ///__ALGORITHMS_CPP__

